#language:pt

@regressao
Funcionalidade: Executar teste de regressao nos cenarios

Contexto:
  Dado que estou logado e autenticado e com "PACOTES" selecionado no sistema

  @MOTOR_HOME @ok
  #done
  Esquema do Cenario: Fazer busca com origem São paulo com destino a uma cidade nacional
    Quando preencho o campo origem com <origem> e destino <destino>
    E seleciono alguma data
    E clico no botao "buscar"
    Entao valido a busca com <origem> e <destino>

  Exemplos:
    | origem      |  destino |
    | "São paulo" | "Rio de Janeiro - Rio de Janeiro - Brasil" |
    | "São paulo" | "Caldas Novas - Goiás - Brasil" |

  @MOTOR_HOME @ok
  #done
    Esquema do Cenario: Fazer busca com origem diferente de São paulo com destino a uma cidade nacional
     Quando preencho o campo origem com <origem> e destino <destino>
     E seleciono alguma data
     E clico no botao "buscar"
     Entao valido a busca com <origem> e <destino>

    Exemplos:
      |        origem      |  destino        |
      | "Maceio"           | "Porto de Galinhas - Pernambuco - Brasil" |
      | "Porto alegre"     | "Natal - Rio Grande do Norte - Brasil"  |



  @MOTOR_HOME @ok
  #ajustado 17/07
    Esquema do Cenario: Fazer busca com origem São paulo com destino a uma cidade internacional #ok
      Quando preencho o campo origem com <origem> e destino <destino> "internacionais"
      E seleciono alguma data
      E clico no botao "buscar"
      Entao valido a busca com <origem> e <destino> internacionais

    Exemplos:
     | origem      | destino        |
     | "São paulo" | "Punta Cana - República Dominicana" |
     | "São paulo" | "Cancún - México" |



  @MOTOR_HOME @ok
  #done
    Esquema do Cenario: Fazer busca com origem diferente de São paulo com destino a uma cidade internacional
      Quando preencho o campo origem com <origem> e destino <destino> "internacionais"
      E seleciono alguma data
      E clico no botao "buscar"
      Entao valido a busca com <origem> e <destino> internacionais

    Exemplos:
      |      origem        |  destino       |
      | "Porto Alegre"     | "Miami - Estados Unidos"  |
      | "Rio de janeiro"   | "Milão - Itália" |


  ##########################################################

  @MOTOR_REFAZER_BUSCA @ok
  #done
  Cenario: Fazer busca com origem São paulo com destino a uma cidade nacional
    Quando preencho o campo origem com "São paulo" e destino "Recife"
    E seleciono alguma data
    E clico no botao "buscar"
    E clico no botao "REFAZER_BUSCA"
    E seleciono origem "sao paulo" e destino "Rio de janeiro"
    E clico no botao "buscar"
    Entao valido busca com origem "sao paulo" e destino "Rio de Janeiro - Rio de Janeiro - Brasil"

  @MOTOR_REFAZER_BUSCA  @ok
  #ajustado 17/07
  Cenario: Fazer busca com origem São paulo com destino a uma cidade internacional
    Quando clico no botao "internacionais"
    E preencho o campo origem com "São paulo" e destino "Cancun" "internacionais"
    E clico no botao "buscar"
    E clico no botao "REFAZER_BUSCA"
    E seleciono origem "sao paulo" e destino "Roma"
    E clico no botao "buscar"
    Entao valido busca com origem "sao paulo" e destino "Roma - Itália" internacionais

  @MOTOR_REFAZER_BUSCA @ok
  #done
   Cenario: Fazer busca com origem diferente de São paulo com destino a uma cidade nacional
    Quando preencho o campo origem com "rio de janeiro" e destino "salvador"
    E clico no botao "buscar"
    E clico no botao "REFAZER_BUSCA"
    E seleciono origem "rio de janeiro" e destino "Natal - Rio Grande Do Norte - Brasil"
    E clico no botao "buscar"
    Entao valido busca com origem "rio de janeiro" e destino "Natal - Rio Grande do Norte - Brasil"

  @MOTOR_REFAZER_BUSCA @ok
  #done
  Cenario: Fazer busca com origem diferente de São paulo com destino a uma cidade internacional
    Quando clico no botao "internacionais"
    E preencho o campo origem com "Rio de janeiro" e destino "Cancun"
    E seleciono alguma data
    E clico no botao "buscar"
    E clico no botao "REFAZER_BUSCA"
    E seleciono origem "Rio de janeiro" e destino "Buenos Aires"
    E clico no botao "buscar"
    Entao valido busca com origem "Rio de janeiro" e destino "Buenos Aires - Argentina" internacionais

  @MOTOR_REFAZER_BUSCA @ok
   #done
  Cenario: O motor Refazer Busca deve guardar cache das informacoes da pesquisa original na home
    Quando preencho o campo origem com "São paulo" e destino "Recife"
    E clico no botao "buscar"
    E clico no botao "REFAZER_BUSCA"
    Entao Motor Refazer busca deve apresentar destino e origem de acordo com motor home

  ###############################################################

  @Resultados @ok
  #done
  Cenario: Validar Layout Tela de resultados
  Quando preencho o campo origem com "São paulo" e destino "Fortaleza"
    E informo o mes de "OUTUBRO" e o dia "15"
    E clico no botao "buscar"
    E Valido Layout "btnRefazerbusca"
    E Valido Layout "btnBuscardatasdisp"
    E Valido Layout "camposQtdpax"
    E Valido Layout "calendario"
    Entao Valido Layout "resultadosPacote"

  @Resultados  @ok
  #done
  Cenário: Sistema apresenta tela de resultados com filtros e dois adultos como default
    Quando preencho o campo origem com "São paulo" e destino top "Nacional"
    E seleciono alguma data
    E clico no botao "buscar"
    Entao o sistema apresenta campo "Adultos_|+12" anos com "2" como default

  @Resultados @ok
  #done
  Cenário: Apresentar filtro Criancas numerado de 0 à 9
    Quando preencho o campo origem com "São paulo" e destino top "Nacional"
    E seleciono alguma data
    E clico no botao "buscar"
    Entao seleciono a Dropdown_list no campo CRIANÇAS | ATÉ doze e Valido sequencia de zero á nove

  @Resultados @ok
  Cenário: Apresentar filtro Idade da crianca no fim da viagem numerado de 0 à 12
    Quando preencho o campo origem com "São paulo" e destino top "Nacional"
    E seleciono alguma data
    E clico no botao "buscar"
    Entao seleciono a Dropdown list no campo Criancas "1" crianca e Valido opcoes de Idade da Crianças_fim_viagem

  ###########################################################

  @ALT_QUARTO @ok
  #DONE
  Cenario: Validar alteracao de valor no box de precos, de acordo com alteracao de quarto realizada na modal
    Quando preencho o campo origem com "São paulo" e destino "balneario"
    E informo o mes de "SETEMBRO" e o dia "6"
    E clico no botao "buscar"
    E clico em "Escolher_outro_hotel"
    Entao realizo alteracao de tipo de quarto validando precos

  @ALT_QUARTO @ok
  #done
  Cenario: Apresentar valor de taxa, valor por pessoa e total da viagem com taxa para cada opcao de quarto (Layout)
    Quando preencho o campo origem com "São paulo" e destino top "Nacional"
    E seleciono alguma data
    E clico no botao "buscar"
    E clico em "Escolher_outro_hotel"
    E Valido Layout "taxa"
    E Valido Layout "valor por pessoa"
    E Valido Layout "btnSelecionar"
    Entao Valido Layout "total da viagem"

  ###########################################################

  @pacotes1 @bug
 #cenario falh o por conta de bug
  Cenario: Alterar voo e hotel e Validar se voo nao é alterado no box de voo
    Quando preencho o campo origem com "São paulo" e destino "Aquiraz"
    E informo o mes de "OUTUBRO" e o dia "24"
    E clico no botao "buscar"
    E Clico em "Escolher_outro_voo"
    E seleciono "voo_especifico"
    E Clico em "Escolher_outro_hotel"
    E Seleciono "Hotel_especifico"
    Entao Valido que voo nao foi alterado no box de voo

  @pacotes1 @bug
  #cenario falho por conta de bug
  Cenario: Alterar voo e hotel e Validar se voo nao é alterado na bandeja
    Quando preencho o campo origem com "São paulo" e destino "Aquiraz"
    E informo o mes de "OUTUBRO" e o dia "24"
    E clico no botao "buscar"
    E Clico em "Escolher_outro_voo"
    E seleciono "voo_especifico"
    E Clico em "Escolher_outro_hotel"
    E Seleciono "Hotel_especifico"
    Entao Valido infos de voo na bandeja

@pacotes @ok
Cenario: Alterar valor de pacote quando alterar voo e hotel
  Quando preencho o campo origem com "São paulo" e destino "Aquiraz"
  E informo o mes de "NOVEMBRO" e o dia "4"
  E clico no botao "buscar"
  E clico em "Escolher_outro_voo"
  E seleciono "voo_especifico"
  E Valido alteracao de valor no voo
  E Clico em "Escolher_outro_hotel"
  E Seleciono "Hotel_especifico"
  Entao Valido alteracao de valor no hotel

@pacotes @ok
Cenario: Permitir alterar hotel para o pacote
  Quando preencho o campo origem com "São paulo" e destino top "Nacional"
  E informo o mes de "OUTUBRO" e o dia "24"
  E clico no botao "buscar"
  E Clico em "Escolher_outro_hotel"
  E Seleciono "Hotel_especifico"
  Entao Valido alteracao de hotel

@pacotes @ok
Cenario: Apresentar Mais detalhes do voo com informacoes corretas no box de voo
  Quando preencho o campo origem com "São paulo" e destino top "Nacional"
  E seleciono alguma data
  E clico no botao "buscar"
  E Clico em "Mais_detalhes_voo"
  Entao Valido "modal de voo"

@pacotes
Cenario: Apresentar opcoes Mapa, Ver detalhes, Servicos Inclusos, Roteiro dia a dia, Período de reserva e Benefícios em todos os hoteis (Layout)
  Quando preencho o campo origem com "São paulo" e destino top "Nacional"
  E seleciono alguma data
  E clico no botao "buscar"
  E clico em "Escolher_outro_hotel"
  E Valido "Mapa"
  E Valido "Ver detalhes"
  E Valido "Servicos Inclusos"
  E Valido "Roteiro dia a dia"
  Entao Valido "Período de reserva"

@pacotes @ok
#cenario falhando devido a erro de cadastro
Cenario: Apresentar ícones de servicos inclusos corretamente
  Quando preencho o campo origem com "São paulo" e destino "Aquiraz"
  E informo o mes de "OUTUBRO" e o dia "4"
  E clico no botao "buscar"
  Entao Valido icones em servicos inclusos

  ###############################################################################

@Calendario  @ok
Cenario: Calendario aberto com dois meses na tela de resultados
  Quando preencho o campo origem com "São paulo" e destino top "Nacional"
  E seleciono alguma data
  E clico no botao "buscar"
  Entao Valido calendário aberto com "2" meses

@Calendario @ok
Cenario: Calendário apresentar menores precos do mês com tag Melhor Preco
  Quando preencho o campo origem com "São paulo" e destino top "Nacional"
  E seleciono alguma data
  E clico no botao "buscar"
  Entao Valido calendário aberto com "2" meses, Validando TAG MELHOR PRECO

  @Calendario @ok
  Cenario: Calendário permitir navegacao até 365 dias após data atual
    Quando preencho o campo origem com "São paulo" e destino top "Nacional"
    E seleciono alguma data
    E clico no botao "buscar"
    Entao Valido calendário aberto com "2" meses, navegando por todos os meses

  @Calendario @ok
  Cenario: Calendário apresentar precos sem arredondamento
    Quando preencho o campo origem com "São paulo" e destino top "Nacional"
    E seleciono alguma data
    E clico no botao "buscar"
    Entao valido se valores nao estao arredondados

  @Calendario @ok
  Cenario: Calendario apresenta somente datas com disponibilidade
    Quando preencho o campo origem com "São paulo" e destino top "Nacional"
    E seleciono alguma data
    E clico no botao "buscar"
    Entao sistema nao deve apresentar mensagem de indisponibilidade

  @Calendario @ok
  Cenario: Permitir selecionar uma data no calendário
    Quando preencho o campo origem com "São paulo" e destino top "Nacional"
    E seleciono alguma data
    E clico no botao "buscar"
    Entao visualizar pacotes para data selecionada

  @Calendario @ok
  Cenario: Apresentar selecionado no calendário a data pesquisada caso tenha pacote para a mesma
    Quando preencho o campo origem com "São paulo" e destino "rio de janeiro"
    E informo o mes de "OUTUBRO" e o dia "7"
    E clico no botao "buscar"
    Entao Valido se calendario esta com mes aberto de acordo com a pesquisa no motor de busca

  @Calendario @ok
  Cenario: Apresentar selecionado no calendário a data de menor preco caso nao tenha pacote na data pesquisada
    Quando preencho o campo origem com "São paulo" e destino "Aquiraz"
    E informo o mes de "OUTUBRO" e o dia "8"
    E clico no botao "buscar"
    Entao calendario apresenta selecionada data de melhor preco

  ###############################################################################

@bandeja @ok
Cenario: Apresentar opcao Ver mais em cada produto do pacote CVC na bandeja com informacoes dos produtos inclusos (voo/hotel/servicos inclusos), Salvar Orcamento, Imprimir Orcamento e Enviar Orcamento por e-mail na bandeja
  Quando preencho o campo origem com "São paulo" e destino top "Nacional"
  E seleciono alguma data
  E clico no botao "buscar"
  E Envio pacote para "bandeja"
  E Valido "ver mais"
  E Valido "Pacotes"
  E Valido "salvar_orcamento"
  E Valido "Imprimir_orcamento"
  Entao Valido "enviar_orcamento"

@bandeja @ok @Falha
Cenario: Permitir excluir pacote da bandeja
  Quando preencho o campo origem com "São paulo" e destino top "Nacional"
  E seleciono alguma data
  E clico no botao "buscar"
  E Envio pacote para "bandeja"
  E Clico em "excluir"
  Entao valido exclusao de "Pacotes"


@bandeja @ok
Cenario: Permitir adicionar outro pacote na bandeja desde que o mesmo nao tenha data no intervalo de viagem do primeiro
  Quando preencho o campo origem com "São paulo" e destino "Caldas novas"
  E informo o mes de "AGOSTO" e o dia "30"
  E clico no botao "buscar"
  E Envio pacote para "bandeja"
  E adiciono outro pacote a "bandeja"
  Entao Envio pacote para "bandeja"

@bandeja @Falha
Cenario: Permitir adicionar demais produtos (combinacao do drolls) na bandeja
  Quando preencho o campo origem com "São paulo" e destino top "Nacional"
  E seleciono alguma data
  E clico no botao "buscar"
  E Envio pacote para "bandeja"
  Entao Seleciono "PASSAGENS", adicionando a "bandeja"


  ############################################################################
