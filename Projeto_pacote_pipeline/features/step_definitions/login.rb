Dado("que estou logado e autenticado e com {string} selecionado no sistema") do |arg|
  visit($url)
  @login.authenticate(arg)
end
