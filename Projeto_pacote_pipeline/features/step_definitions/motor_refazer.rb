Quando("seleciono origem {string} e destino {string}") do |origem,destino|
  @mtds.motor_busca_ref(origem,destino)
end

Entao("valido busca com origem {string} e destino {string}") do |origem,destino|
  @valid.valida_destino_INT(origem,destino)
end

Quando("preencho o campo origem com {string} e destino {string} {string}") do |origem, destino, internacionais|
  @mtds.motor_busca_inter(origem,destino,internacionais)
end


Entao("valido busca com origem {string} e destino {string} internacionais") do |origem, destino|
  @valid.valida_destino_INT(origem,destino)

end

Entao("Motor Refazer busca deve apresentar destino e origem de acordo com motor home") do


end
