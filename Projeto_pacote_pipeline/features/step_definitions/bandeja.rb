Entao("Envio pacote para {string}") do |arg|
  @mtds.enviaBandeja
  @valid.validaElement(arg)
end

Entao ("valido exclusao de {string}") do |arg|
  @valid.validaExclusao(arg)
end

Quando("adiciono outro pacote a {string}") do |arg|
  @mtds.calender_op_pct
  @valid.validaElement(arg)
end

Entao("Seleciono {string}, adicionando a {string}") do |arg,arg2|
  @PASSAGENS.productPassagens(arg,arg2)
end
