Quando("informo o mes de {string} e o dia {string}") do |arg, arg2|
  @mtds.selecionar_data_especifica(arg,arg2)
end

Quando("clico em {string}") do |arg|
  @mtds.click_btn(arg)
end

Entao("realizo alteracao de tipo de quarto validando precos") do
  Metodos.new.selecionar_hotel_esp
  @valid.valida_preco
end
