Entao("Valido calendário aberto com {string} meses") do |arg|
  @valid.valida_calendario(arg)
end

Entao("Valido calendário aberto com {string} meses, Validando TAG MELHOR PRECO") do |arg|
  @valid.valida_calendario(arg)
  assert_selector(".ember-power-calendar-day-bestprice", wait:40)
end


Entao("Valido calendário aberto com {string} meses, navegando por todos os meses") do |arg|
  @valid.valida_calendario(arg)
  assert_selector(".ember-power-calendar-day-bestprice", wait:40)
  @mtds.nav_calender
end

Entao("valido se valores nao estao arredondados") do
  assert_selector(".ember-power-calendar-day-bestprice", wait:40)
  @valid.valida_arredondamento
end

Entao("sistema nao deve apresentar mensagem de indisponibilidade") do
  assert_selector(".ember-power-calendar-day-bestprice", wait:40)
  @valid.valida_arredondamento
  @mtds.select_all_Data2
end


Entao("visualizar pacotes para data selecionada") do
  assert_selector(".ember-power-calendar-day-bestprice", wait:40, visible:true)
  @valid.valida_arredondamento
  @mtds.select_all_Data2
end

Entao("Valido se calendario esta com mes aberto de acordo com a pesquisa no motor de busca") do
  assert_selector(".ember-power-calendar-day-bestprice", wait:40, visible:true)
  @valid.valida_arredondamento
  @valid.valida_mes
end


Entao("calendario apresenta selecionada data de melhor preco") do
  @valid.validaData
end
