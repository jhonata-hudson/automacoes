##@pacotes 1
Quando("Clico em {string}") do |arg|
  @mtds.click_btn(arg)
end

Quando("seleciono {string}") do |arg|
  @mtds.randAir(arg)
end

Quando("Seleciono {string}") do |arg|
  @mtds.randHotel(arg)
end

Entao("Valido que voo nao foi alterado no box de voo") do
  @valid.valida_horario
end
#########
#########@pacotes 2
Entao("Valido infos de voo na bandeja") do
  @valid.valida_voo_bandeja
end

Quando("Valido alteracao de valor no voo") do
  @valid.valida_alt_valor
end

Entao("Valido alteracao de valor no hotel") do
  @valid.valida_alt_valor
end
#########
Entao("Valido alteracao de hotel") do
  @valid.valida_alt_nome_hotel
end

Entao("Valido {string}") do |arg|
  @valid.validaElement(arg)
end

Entao("Valido icones em servicos inclusos") do
  @valid.valida_icons
end
