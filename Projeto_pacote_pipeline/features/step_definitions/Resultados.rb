Quando("preencho o campo origem com {string} e destino top {string}") do |origem,arg|
  @mtds.motor_busca_top(origem,arg)
end

Quando("seleciono alguma data") do
 @mtds.select_all_Data
end

Quando("Valido Layout {string}") do |arg|
  @valid.valida_layout(arg)
end


Entao("o sistema apresenta campo {string} anos com {string} como default") do |arg,arg2|
  @valid.valida_campo_adultos(arg,arg2)
end

Entao("seleciono a Dropdown_list no campo CRIANÇAS | ATÉ doze e Valido sequencia de zero á nove") do
 @valid.valida_campo_children
end

Entao("seleciono a Dropdown list no campo Criancas {string} crianca e Valido opcoes de Idade da Crianças_fim_viagem") do |arg|
  @mtds.select_childrem(arg)
  @valid.valida_childrem_fim_vg
end
