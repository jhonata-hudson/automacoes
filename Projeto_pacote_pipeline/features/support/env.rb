require 'yaml'
require 'pry'
require 'capybara/cucumber'

EL = YAML.load_file('data/Elements.yml')
DATA = YAML.load_file('data/DATA.yml')

if ENV['TI']
  $url = 'http://lojas-ti.cvc.com.br/'
  p 'AMBIENTE DEV...'
  $user = 'mtzcpd1348'
  $password = 'Cvc2323@#'
  Capybara.default_max_wait_time = 10
elsif ENV['prod']
  $url = 'https://vendas.cvc.com.br'
  p 'AMBIENTE PROD...'
  $user = 'mtzcpd1348'
  $password = 'Cvc2323@#'
  Capybara.default_max_wait_time = 15
elsif ENV['qa']
  $url = 'https://vendasqa.cvc.com.br'
  p 'AMBIENTE QA...'
  $user = 'mtzcpd1348'
  $password = 'on##2018'
  Capybara.default_max_wait_time = 5
end

if ENV['chrome']
  Capybara.default_driver = :chrome
  Capybara.register_driver :chrome do |app|
    Capybara::Selenium::Driver.new(app, browser: :chrome, args: ['--incognito', '--disable-infobars'])
  end
elsif ENV['chrome_headless']
  Capybara.default_driver = :chrome_headless
  Capybara.register_driver :chrome_headless do |app|
    Capybara::Selenium::Driver.new(app, browser: :chrome, switches: ['--incognito', '--headless', 'disable-gpu', 'window-size=1280x720', '--no-sandbox'])
  end
elsif ENV['firefox']
  Capybara.default_driver = :firefox
  Capybara.register_driver :firefox do |app|
    Capybara::Selenium::Driver.new(app, browser: :firefox)
  end
elsif ENV['ie']
  Capybara.default_driver = :ie
  Capybara.register_driver :ie do |app|
    Capybara::Selenium::Driver.new(app, browser: :internet_explorer)
  end
elsif ENV['headless_debug']
  Capybara.default_driver = :poltergeist_debug
  Capybara.register_driver :poltergeist_debug do |app|
    Capybara::Poltergeist::Driver.new(app, inspector: true)
  end
  Capybara.javascript_driver = :poltergeist_debug
elsif ENV['headless']
  Capybara.javascript_driver = :poltergeist
  Capybara.default_driver = :poltergeist
else
  Capybara.default_driver = :selenium
end
