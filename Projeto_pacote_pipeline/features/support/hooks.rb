require 'report_builder'
require 'ffaker'
require 'pry'
require 'pry-nav'


  Before do
    Pry.commands.alias_command 'c', 'continue'
    Pry.commands.alias_command 'e', 'exit'
    Pry.commands.alias_command 's', 'step'
    Pry.commands.alias_command 'n', 'next'
    @mtds = Metodos.new
    @login = LOGIN_B2B.new
    @valid = Validar.new
    @PASSAGENS = PASSAGENS.new
    # @ffaker = Cadastro.new
    page.driver.browser.manage.window.maximize
  end

  # After do |scn|
  #   if scn.failed? do
  #     page.driver.quit
  #       ruby -e, '{`cucumber features/filtro_e_ordenacao.feature qa=true`}'
  #   end
  # end

  at_exit do
    ReportBuilder.configure do |config|
      t = Time.now
      t.to_s

      config.input_path = 'data/reports'
      config.report_path = 'data/reports/REPORT_PACOTES ' + t.strftime("%d_%m_%y") +' '+ t.strftime("%H_%M_%S")
      config.report_title = 'REPORT_PACOTES'
      config.additional_info = { browser: 'Chrome', environment: 'Quality Assurance', Data_execução: t.strftime("%d/%m/%y"), Horário_Conclusão: t.strftime("%H:%M:%S") }
      config.include_images = true
    end
       ReportBuilder.build_report
  end

     After do |scn|
         file_name = scn.name.gsub(' ', '_').downcase!
         target = "data/reports/imagens/#{file_name}.png"
         if scn.failed?
           page.save_screenshot(target)
           embed(target, 'image/png', 'Evidência')
         end
         page.driver.quit
       end
  #
  After do |scn|
    if scn.failed?
    binding.pry
    end
  end
  #  After do |scn|
  #     if scn.passed?
  #     binding.pry
  #     end
  #   end
