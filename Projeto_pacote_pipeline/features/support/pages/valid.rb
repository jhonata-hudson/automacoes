class Validar
  include Capybara::DSL

  def valida_destino_NC(origem, destino)
    if has_text?(destino)

      pacote = all('.destination-destiny', wait: 20)[1].text

      pacote.include?(origem)

      pacote.include?(destino)

    else

      raise "Pacote com o destino #{destino} nao encontrado"

    end
  end

  def valida_destino_INT(origem, destino)
    if has_text?(destino)

      pacote = all('.destination-destiny', wait: 30)[1].text

      pacote.include?(origem)

      pacote.include?(destino)

    else

      raise "Pacote com o destino #{destino} nao encontrado"

    end
    end

  def valida_layout(elemento)
    if has_selector?(EL[elemento]) do
         assert_selector(EL[elemento], wait: 10)
       end

    else

      raise "#{elemento} nao encontrado"

    end
  end

  def validaElement(elemento)
    if has_selector?(EL[elemento]) do
         assert_selector(EL[elemento], wait: 10)
       end

    else

      raise "#{elemento} nao encontrado"

    end
  end

  def valida_campo_children
    assert_no_selector('.general-loader-text', wait: 30)

    all(EL['campo'])[2].click

    padrao = '10'

    padrao2 = within all('.medium-3.large-2.columns')[2] do
      all('option').count
    end

    padrao2 = padrao2.to_s

    raise 'Erro campo Criancas' if padrao != padrao2
  end

  def valida_campo_adultos(elemento, elemento2)
    assert_selector(EL[elemento], visible: true)

    default = elemento2

    default2 = find(EL[elemento]).value

    raise 'Erro campo adultos' if default != default2
  end

  def valida_childrem_fim_vg
    find('#detail-children-age-end-00', wait: 15).click

    assert_text("Idade da crian\xC3\xA7a no fim da viagem", wait: 10)

    fim_viagem = within('#detail-children-age-end-00', wait: 15) do
      all('option').count
    end

    fim_viagem2 = '13'

    fim_viagem3 = fim_viagem2.to_i

    raise 'Erro campo idade crianca no fim da viagem' if fim_viagem != fim_viagem3
  end

  def valida_preco
    assert_text('COMPRAR', wait: 10)

    preco_total_opc_pacotes = first('.package-value-price.actual-price').text

    raise 'ERRO VALOR TOTAL' if $preco_total_opc_pacotes == preco_total_opc_pacotes
  end

  def valida_horario
    horarioVOO = all('.package-summary-flight-each-origin-copy', wait: 10)[1].text

    raise 'Voo alterado' if horarioVOO != $horarioVOO
  end

  def valida_voo_bandeja
    click_button('Comprar', wait: 10)

    while has_selector?(EL['loader'], wait: 10)

      for e in 0..5 do

        p e.to_s

        break if has_selector?(EL['loader']) == false

    end

  end

    find('#flight-251010848-0', wait: 10).click

    horarioBand = all('.flight-info-copy')[4].text

    horarioVOO = $horarioVOO.tr!(':', 'h')

    p horarioVOO

    raise 'Voo alterado na bandeja' if horarioVOO != horarioBand
  end

  def valida_alt_valor
    raise 'Valor nao alterado' if $precoPorPax == $precoPorPax2
  end

  def valida_alt_nome_hotel
    nomeHotel = all('.package-summary-content')[1].all('p')[0].text

    raise 'Hotel nao alterado' if nomeHotel == $nomeHotel
  end

  def valida_icons
    if

      has_selector?('.icon-sprite.icon-plane-go-18', wait: 10)

      has_selector?('.icon-sprite.icon-traslado-primary-21', wait: 10)

      has_selector?('.icon-sprite.icon-hotel-primary-23', wait: 10)

    else

      raise 'ERRO ICONS SERVICOS INCLUSOS'

    end
    end

  def valida_calendario(elemento)
    qtd_mes = all('.ember-power-calendar-nav-title', wait: 10, visible: true).count

    qtd_mes = qtd_mes.to_s

    raise 'ERRO QUANTIDADE MES' if qtd_mes < elemento
  end

  def valida_arredondamento
    assert_selector('.package-detail-heading')

    c = all('.ember-power-calendar-day-value').count

    c -= 1

    while c > 0

      valor = all('.ember-power-calendar-day-value')[c].text

      raise 'ERRO' if valor.include?(',00')

      c -= 1

    end
   end

  def valida_tag
    assert_selector('.ember-power-calendar-day-bestprice', wait: 40)
  end

  def valida_mes
    raise 'Calendario nao esta com data correta selecionada' if has_no_selector?('.ember-power-calendar-day-number-active', exact_text: $data, wait: 10)
  end

  def validaData
    if find('.ember-power-calendar-day-action-active').all('span')[1].text

    else

      raise "Calendario desconsidera melhor pre\xC3\xA7o"

    end
  end

  def validaExclusao(elemento)
    raise "#{elemento} nao deletado" if has_selector?(EL[elemento], wait: 10)
  end
end
