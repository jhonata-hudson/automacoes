class PASSAGENS
  include Capybara::DSL

  def productPassagens(elemento,elemento2)
    find(EL[elemento], wait:10).click
    while has_selector?(EL['loader'], wait: 10) do
      for e in 0..5 do
        p "#{e}"
        break if has_selector?(EL['loader']) == false
      end
    end
    all('.general-loader-container')[0].all('input')[0].set("sao paulo")
    
    all('.general-loader-container')[0].all('input')[0].native.send_keys(:enter)
    all('.general-loader-container')[1].all('input')[0].set("rio de janeiro")
    
    all('.general-loader-container')[1].all('input')[0].native.send_keys(:enter)
    click_button('Buscar', wait: 10)
    while has_selector?('.ember-view.progress', wait: 10) do
      for e in 0..5 do
        p "#{e}"
        break if has_selector?('.ember-view.progress') == false
      end
    end
    all('#flight-selected-', wait: 10)[0].click
    assert_selector('.modal-primary-heading', wait: 10)
    find('#baggage-options-submit', wait: 10).click
    assert_selector(EL[elemento], wait: 10)
  end



























end
