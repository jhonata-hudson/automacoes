class Metodos
  include Capybara::DSL

  def motor_busca_ref(origem, destino)
    if has_selector?("#search-engine-product-content-id", wait: 10) do
      find(:xpath, EL["Campo_origem"]).set(origem)
      find(:xpath, EL["Campo_origem"]).set(origem).native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    if has_selector?("#search-engine-product-content-id", wait: 10) do
      find(EL["Campo_destino"]).find("input", visible: true).set(destino)
      find(EL["Campo_destino"]).find("input").native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end
  end

  def motor_busca_intertop(origem, destino)
    find(EL["internacionais"], wait: 10).click

    if has_selector?("#search-engine-product-content-id", wait: 10) do
      find(:xpath, EL["Campo_origem"]).set(origem)
      find(:xpath, EL["Campo_origem"]).set(origem).native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    if has_selector?(".product-form-error", exact_text: "Selecione uma origem") do
      find(:xpath, EL["Campo_origem"]).set(origem)
      find(:xpath, EL["Campo_origem"]).set(origem).native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    $ORIGEM = first(".selectize-input.items.has-options.full.has-items", wait: 30).find("div").text

    $ORIGEM = $ORIGEM.capitalize

    p $ORIGEM

    if has_selector?("#search-engine-product-content-id", wait: 10) do
      find(EL["Campo_destino"]).find("input", visible: true).set(destino)
      find(EL["Campo_destino"]).find("input").native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    if has_selector?(".product-form-error", exact_text: "Selecione um destino") do
      find(EL["Campo_destino"]).find("input", visible: true).set(destino)
      find(EL["Campo_destino"]).find("input").native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    $DESTINO = all(".selectize-input.items.has-options.full.has-items", wait: 30).last.find("div").text

    $DESTINO = $DESTINO.capitalize

    p $DESTINO
  end

  def motor_busca_top(origem, destino)
    if has_selector?("#search-engine-product-content-id", wait: 10) do
      find(:xpath, EL["Campo_origem"]).set(origem)
      find(:xpath, EL["Campo_origem"]).set(origem).native.send_keys(:enter)
      binding.pry
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    # if has_selector?('.product-form-error', exact_text: 'Selecione uma origem') do
    #
    #   find(:xpath, EL['Campo_origem']).set(origem).native.send_keys(:enter)
    #
    #   end
    #
    # end
    #     while has_selector?(EL['loader'])
    #
    #       for e in 0..5 do
    #
    #         p "Carregando...."
    #
    #         break if has_selector?(EL['loader']) == false
    #
    #   end
    #
    # end
    $ORIGEM = first(".selectize-input.items.has-options.full.has-items", wait: 30).find("div").text

    $ORIGEM = $ORIGEM.capitalize

    p $ORIGEM

    if has_selector?("#search-engine-product-content-id", wait: 10) do
      find(EL["Campo_destino"]).find("input").set(DATA[destino].sample)
      find(EL["Campo_destino"]).find("input").native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    1

    $DESTINO = all(".selectize-input.items.has-options.full.has-items", wait: 30).last.find("div").text

    $DESTINO = $DESTINO.capitalize

    p $DESTINO
    binding.pry
  end

  def motor_busca(origem, destino)
    if has_selector?("#search-engine-product-content-id", wait: 10) do
      find(:xpath, EL["Campo_origem"]).set(origem)
      find(:xpath, EL["Campo_origem"]).set(origem).native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    if has_selector?(".product-form-error", exact_text: "Selecione uma origem") do
      find(:xpath, EL["Campo_origem"]).set(origem)
      find(:xpath, EL["Campo_origem"]).set(origem).native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    $ORIGEM = first(".selectize-input.items.has-options.full.has-items", wait: 30).find("div").text

    $ORIGEM = $ORIGEM.capitalize

    p $ORIGEM

    if has_selector?("#search-engine-product-content-id", wait: 10) do
      find(EL["Campo_destino"]).find("input", visible: true).set(destino)
      find(EL["Campo_destino"]).find("input").native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    if has_selector?(".product-form-error", exact_text: "Selecione um destino") do
      find(EL["Campo_destino"]).find("input", visible: true).set(destino)
      find(EL["Campo_destino"]).find("input").native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end
    1

    $DESTINO = all(".selectize-input.items.has-options.full.has-items", wait: 30).last.find("div").text

    $DESTINO = $DESTINO.capitalize

    p $DESTINO
  end

  def motor_busca_inter(origem, destino, _internacionais)
    find(EL["internacionais"], wait: 10).click

    if has_selector?("#search-engine-product-content-id", wait: 10) do
      find(:xpath, EL["Campo_origem"]).set(origem)
      find(:xpath, EL["Campo_origem"]).set(origem).native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    if has_selector?(".product-form-error", exact_text: "Selecione uma origem") do
      find(:xpath, EL["Campo_origem"]).set(origem)
      find(:xpath, EL["Campo_origem"]).set(origem).native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    $ORIGEM = first(".selectize-input.items.has-options.full.has-items", wait: 30).find("div").text

    $ORIGEM = $ORIGEM.capitalize

    p $ORIGEM

    if has_selector?("#search-engine-product-content-id", wait: 10) do
      find(EL["Campo_destino"]).find("input", visible: true).set(destino)
      find(EL["Campo_destino"]).find("input").native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    if has_selector?(".product-form-error", exact_text: "Selecione um destino") do
      find(EL["Campo_destino"]).find("input", visible: true).set(destino)
      find(EL["Campo_destino"]).find("input").native.send_keys(:enter)
    end
    end
    while has_selector?(EL["loader"])
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    $DESTINO = all(".selectize-input.items.has-options.full.has-items", wait: 30).last.find("div").text

    $DESTINO = $DESTINO.capitalize

    p $DESTINO
  end

  def click_btn1(elemento)
    if has_selector?(EL[elemento], wait: 10) do
      find(EL[elemento], wait: 10).click

      while has_selector?(EL["loader"])
        for e in 0..5
          p "Carregando...."

          break if has_selector?(EL["loader"]) == false
        end
      end
    end
    end

    # while has_selector?('.message-alert-title', wait: 10) do

    # find('#nextMonth').click while has_no_selector?('.ember-power-calendar-day-value')

    #   x = all('.ember-power-calendar-day-value', wait: 10).count

    #   y = rand(0..(x))

    #   z = all('.ember-power-calendar-day-value', wait: 10)[y].click

    #   assert_no_selector(EL['loader'], wait:10)

    #   break if has_selector?('.message-alert-title') == false

    # end
  end

  def click_btn(elemento)
    if has_selector?(EL[elemento], wait: 10) do
      find(EL[elemento], wait: 10).click

      while has_selector?(EL["loader"])
        for e in 0..5

          # p "#{e}"

        end
      end
    end
    end

    while has_selector?(".message-alert-title", wait: 10)
      find("#nextMonth").click while has_no_selector?(".ember-power-calendar-day-value")

      x = all(".ember-power-calendar-day-value", wait: 10).count

      y = rand(0..x)

      z = all(".ember-power-calendar-day-value", wait: 10)[y].click

      assert_no_selector(EL["loader"], wait: 10)

      break if has_selector?(".message-alert-title") == false
    end
  end

  def select_all_Data
    assert_text("DATA", wait: 10)

    find("#date-package-basic", wait: 20).click

    assert_selector(".pika-single.is-bound", wait: 60)

    find(".pika-next", wait: 30).click

    assert_selector(".pika-lendar", wait: 15, visible: true)

    x = all(".pika-table", wait: 15, visible: true)[1].all("button", wait: 15, visible: true).count

    y = rand(0..(x - 1))

    z = all(".pika-table", wait: 15, visible: true)[1].all("button", wait: 15, visible: true)[y].click

    assert_no_selector(".pika-single.is-bound", wait: 10)
  end

  def select_childrem(elemento)
    if has_selector?(".info-step-heading.column", wait: 10) do
      within all(".medium-3.large-2.columns")[2] do
        find("select").select(elemento)
      end
    end
    else
      raise "nao foi possivel selecionar passageiros"
    end
  end

  def selecionar_hotel_esp
    assert_text("PRE\xC3\x87O POR PESSOA", wait: 10)

    find("#select-hotel-2", wait: 10).click

    $preco_total_opc_pacotes = first(".package-value-price.actual-price").text

    assert_text("ESCOLHER OUTRO HOTEL", wait: 10)

    find("#alterRooms4511074142", wait: 10).click

    assert_text("TIPOS DE QUARTO", wait: 10)

    all(".custom-input-text")[1].click

    find(".button-tertiary", exact_text: "CONFIRMAR").click
  end

  def selecionar_data_especifica(elemento, elemento2)
    find("#date-package-basic", wait: 30).click

    find(".pika-next", wait: 30).click while has_no_text?(elemento, wait: 5)

    find(".pika-lendar", text: elemento, wait: 5).find("td", text: elemento2, wait: 5, exact_text: elemento2).click

    $data = elemento2

    $data = $data.to_s

    p $data
  end

  def calender_op_pct
    within all(".ember-power-calendar-day-grid")[1] do
      x = all(".ember-power-calendar-day-value").count

      y = rand(0..x)

      z = all(".ember-power-calendar-day-value")[y].click

      while has_selector?(EL["loader"], wait: 10)
        for e in 0..5
          p "Carregando...."

          break if has_selector?(EL["loader"]) == false
        end
      end
    end
  end

  def randAir(elemento)
    $precoPorPax = first(".package-value-price.actual-price").text

    find(EL[elemento], wait: 10).click

    while has_selector?(EL["loader"], wait: 10)
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    $horarioVOO = all(".package-summary-flight-each-origin-copy", wait: 10)[1].text

    p $horarioVOO

    $precoPorPax2 = first(".package-value-price.actual-price").text
  end

  def randHotel(elemento)
    $precoPorPax = first(".package-value-price.actual-price", wait: 10, visible: true).text

    1

    $nomeHotel = all(".package-summary-content")[1].all("p")[0].text

    find(EL[elemento], wait: 10).click

    while has_selector?(EL["loader"], wait: 10)
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end

    $precoPorPax2 = first(".package-value-price.actual-price").text
  end

  def nav_calender
    while has_no_selector?(".ember-power-calendar-nav-control-disabled")
      find("#nextMonth").click

      assert_no_selector(EL["loader"], wait: 50)
    end

    assert_text("2019")
  end

  def select_all_Data2
    assert_selector(".icon-sprite.icon-calendar-circle-primary-35", wait: 20)

    x = all("button").count

    y = rand(0..(x - 1))

    z = all("button")[y].click

    assert_no_selector(EL["loader"], wait: 10)

    raise "ERRO indisponibilidade" if has_selector?(".message-alert-copy", wait: 20)

    assert_selector(".package-summary-container", wait: 30)
  end

  def enviaBandeja
    click_button("Comprar")

    while has_selector?(EL["loader"], wait: 10)
      for e in 0..5
        p "Carregando...."

        break if has_selector?(EL["loader"]) == false
      end
    end
  end
end
