class LOGIN_B2B
  include Capybara::DSL

  def authenticate(elemento)
    find(EL["campo_login"]).set($user)

    find(EL["campo_senha"]).set($password)

    find(EL["Btn_entrar"], wait: 30).click

    has_text?("Carregando...", wait: 30)

    find(EL["Filial"]).find(EL["Agencia_interna"]).click

    find(EL["Autenticado"], wait: 30).click

    assert_selector(".main-menu-list", wait: 60)

    assert_text("PACOTES", wait: 30)

    2

    if has_selector?(EL[elemento], visible: true) do
      find(EL[elemento], wait: 10, visible: true).click
    end
    else
      raise "nao foi possivel clicar em #{elemento}"
    end

    while has_selector?(EL["loader"], wait: 11)
      for e in 0..5
        p e.to_s

        break if has_selector?(EL["loader"], wait: 11) == false
      end
    end

    if find(".button-primary", exact_text: "INICIAR ATENDIMENTO", wait: 30) do
      click_button("Iniciar Atendimento", wait: 30)
    end
    end
  end
end
