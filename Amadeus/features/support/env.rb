require 'rspec'
require 'pry'
require 'yaml'
require 'net/http'
require 'uri'
require 'date'
require 'json'
require 'crack'
require 'rest-client'
require 'pry-nav'
require 'nokogiri'
require 'httparty'
require 'open-uri'
require 'active_support'
require 'active_support/core_ext'
require 'xml'
require 'gyoku'
require 'rexml/document'
require 'report_builder'
require 'allure-cucumber'
require 'percentage'


Pry.commands.alias_command 'c' , 'continue'
Pry.commands.alias_command 's' , 'step'
Pry.commands.alias_command 'n' , 'next'

 DATA = YAML.load_file('data/Dados.yml')

if ENV['dev']
  $ambiente = "dev"
elsif ENV['integracao']
  $ambiente = "integracao"
elsif ENV['homolog']
  $ambiente = "homolog"
elsif ENV['prod']
  $ambiente = "prod"
else
  $ambiente = "dev"
end


