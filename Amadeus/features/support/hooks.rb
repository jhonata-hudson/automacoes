require 'yaml'
require 'pry'
require 'report_builder'
# require 'selenium-webdriver'
require 'allure-cucumber'

include AllureCucumber::DSL

# Before do
#   # page.driver.browser.manage.window.maximize
#   # @geradores = Geradores.new
#   # @selects = Selects.new
#   # @login = Login.new
# end

# After do |_scn|
#   puts $var
#   puts 'As variaveis nao foram informadas. ' if $var.nil?
#   $var = ''
# end
# at_exit do
#   ReportBuilder.configure do |config|
#     t = Time.now
#     t.to_s
#     config.input_path = 'data/reports'
#     config.report_path = 'data/reports/B2B ' + t.strftime('%d_%m_%y') + ' ' + t.strftime('%H_%M_%S')
#     config.report_types = [:html]
#     config.report_title = 'REPORT B2B - MOVIDA '
#     config.additional_info = { browser: 'Chrome', environment: 'Stage 5', Data_execução: t.strftime('%d/%m/%y'), Horário_Conclusão: t.strftime('%H:%M:%S') }
#     config.include_images = true
#   end
#   ReportBuilder.build_report
# end

# AfterStep do |_scn|
#   target = 'data/reports/imagens/telas_captura.png'
#   page.save_screenshot(target)
#   embed(target, 'image/png', 'Evidência')
# end

# After do |scn|
#   file_name = scn.name.tr(' ', '_').downcase!
#   target = "data/reports/imagens/#{file_name}.png"
#   if scn.failed?
#     page.save_screenshot(target)
#     embed(target, 'image/png', 'Evidência')
#   elsif scn.passed?
#     page.save_screenshot(target)
#     embed(target, 'image/png', 'Evidência')
#   end
#   page.driver.quit
# end

# After do
#   page.driver.browser.manage.delete_all_cookies
# end

# After do |scn|
#    if scn.failed?

#     

#   end
# end

  # Before do
#   driver.start_driver

# end

# After do |scn|

#   driver.driver_quit

#  end

# at_exit do
#     ReportBuilder.configure do |config|
#       t = Time.now
#       t.to_s
#       config.input_path = 'data/reports'
#       config.report_path = 'data/reports/REPORT_Movida ' + t.strftime('%d_%m_%y') + ' ' + t.strftime('%H_%M_%S')
#       config.report_types = [:html]
#       config.report_title = 'REPORT Movida '
#       config.additional_info = { browser: 'Chrome', environment:
# 'Stage 5', Data_execução: t.strftime('%d/%m/%y'), Horário_Conclusão: t.strftime('%H:%M:%S') }
#       config.include_images = true
#     end
#     ReportBuilder.build_report

#     driver.driver_quit

# #   end

AllureCucumber.configure do |c|
  
  c.output_dir = '/data/reports/allure'
  c.clean_dir = true

end

Cucumber::Core::Test::Step.module_eval do
  def name
    return text if text == 'Before hook'

    return text if text == 'After hook'

    return text if text == 'AfterStep hook'

    "#{source.last.keyword}#{text}"
  end
end

AfterStep do |scn|
  Allure.new.attachments(scn)
end

After do |scn|
  Allure.new.attachments(scn)
end

at_exit do
  report_name = Allure.new.build_report

  system("allure open #{Dir.pwd}/data/reports/#{$ambientes}/#{report_name}")

end

Before do 

    @helper = Helpers.new
    @vars = Variables.new
    @payload = Payloads.new
    @comum = Metodos_Comum.new
    @validate = Validate.new
    @dates = Dates.new
    @helper.header_basic(@vars)
    $timeout = DATA[:CONFIG_AMBIENTES][$ambiente]["tempo"]   

  end


After do |scn|
  puts $var + "\n"
  puts 'As variaveis nao foram informadas. ' if $var.nil?
  $var = ''
end






# at_exit do
#   ReportBuilder.configure do |config|
#     t = Time.now
#     t.to_s
#     config.input_path = 'data/reports'
#     config.report_path = 'data/reports/REPORT_MOVIDA ' + t.strftime('%d_%m_%y') + ' ' + t.strftime('%H_%M_%S')
#     config.report_types = [:html]
#     config.report_title = 'REPORT MOVIDA '
#     config.additional_info = { browser: 'Chrome', environment: 'Stage 5', Data_execução: t.strftime('%d/%m/%y'), Horário_Conclusão: t.strftime('%H:%M:%S') }
#   end
#   ReportBuilder.build_report
# end

