class Metodos_Comum

    def seleciona_parceiro(vars,parametro,operadora)

        if operadora == 'ws3' || operadora == 'TravelPort' || operadora == 'decolar'

            parceiro = DATA[:"PARCEIROS_#{operadora}"][parametro][0]

            password = DATA[:"PARCEIROS_#{operadora}"][parametro][1]
        
            vars.password = password
            vars.parceiro = parceiro
        
        else
        
            parceiro = DATA[:"PARCEIROS_#{operadora}"][parametro]
            vars.parceiro = parceiro
        
        end
    end

    def execute_post(vars,*request)
        
        vars.url = Helpers.new.mount_url($ambiente,request[1])
                
        vars.payload_request_tratado = Payloads.new.public_send("payload_#{request[3]}",vars)

        response_post = RestClient::Request.execute(method: request[0], url: vars.url, header: vars.header[:"Content-Type"], payload: vars.payload_request_tratado,:verify_ssl => false  ,timeout: $timeout)
    
        $cenario = request[3]
               
        Validate.new.send("valida_#{request[3]}",vars,response_post,"#{request[2]}","#{request[3]}")

    end 
   
    # def capture_infos_car_rateRule(vars,type_fluxo)
        
    #     ota_veh_avail_rate_RS = vars.response_hash['soap:Envelope']['soap:Body']['OTA_VehRateRuleRS']
        
    #     ota_veh_avail_rate_RS.each do |dados| 
                
    #         break if dados.first == "Target"

    #         Each_tag.new.send(dados.first,vars,dados)
                  
    #     end
    # end
   

    def capture_infos_car_cotacao(vars,type_fluxo)
        
        ota_veh_avail_rate_RS = vars.response_hash['soap:Envelope']['soap:Body']['OTA_VehAvailRateRS']
        
        ota_veh_avail_rate_RS.each do |dados| 
                
            break if dados.first == "xmlns:xsi" 

            Each_tag.new.send(dados.first.downcase,vars,dados)
                  
        end
    end
end