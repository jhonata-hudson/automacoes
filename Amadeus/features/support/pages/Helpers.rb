class Helpers

    def select_filiais_iguais
           
        filial_ret = DATA['Filiais'].sample

        filial_dev = filial_ret

        return filial_ret, filial_dev
    end
   
    def select_filiais_diferentes
           
        filial_ret = DATA['Filiais'].sample

        filial_dev = DATA['Filiais'].sample
        
        while filial_dev == filial_ret

            filial_dev = DATA['Filiais'].sample

        end

        return filial_ret, filial_dev
    end

    def type_veiculo_mais_barato(vars)

        car =  DATA['Categoria_barata'].sample

        vars.car = car
    
    end

    def type_veiculo_mais_caro(vars)

        car =  DATA['Categoria_mais_cara'].sample

        vars.car = car
    
    end


    def defined_traces(vars,response)

        payloads =  []
        
        vars.response_hash = Crack::XML.parse(response)
   
    end
   
    
    def defined_POS(vars,parceiro,operadora)

        vars.operador = operadora
    end
   
   
    def separates_parameters(vars,*parameters)
    
        dados = parameters[0].split('/')

        dates = Dates.new.send("date_#{dados[0]}_#{dados[2]}")
                
        vars.dates = dates 
        
        filiais = self.send("select_#{dados[1]}")
        
        vars.filiais = filiais
        
        self.send("type_#{parameters[1]}",vars)
        
        self.send("#{dados[3]}",vars)
        
        self.send("#{dados[4]}",vars)

        self.send("#{dados[5]}",vars)
    
    end
    

    def select_xsd(vars,type_fluxo)

        if type_fluxo == 'cotacao'

            xsd = "Xsd_#{vars.operador}/OTA_VehAvailRateRS.xsd"

        elsif type_fluxo == 'reserva'

            xsd = "Xsd_#{vars.operador}/OTA_VehResRS.xsd"

        elsif type_fluxo == 'commit'

            xsd = "Xsd_#{vars.operador}/OTA_VehResRS.xsd"

        elsif type_fluxo == 'cancelamento'
        
            xsd = "Xsd_#{vars.operador}/OTA_VehResRS.xsd"
       
        elsif type_fluxo == 'rateRule'


            xsd = "Xsd_#{vars.operador}/OTA_VehRateRuleRS.xsd"
        end
        
        xsdd = xsd.split("/")

        vars.xsd = xsdd[1]

        return xsd 

    end


    def com_condutor_add(vars)

        adicional = 
                    "<Additional>
                 
                        <PersonName>
       
                           <GivenName>QA</GivenName>
       
                            <Surname>Automação</Surname>
       
                        </PersonName>
     
                     </Additional>"
        
        vars.adicional = adicional
     end


    def sem_condutor_add(vars)

        adicional = ""
                   
        vars.adicional = adicional
        
    end
    
    
    def com_equipamento(vars)

        equipamento = DATA['Equipamentos'].sample
        
        vars.equipamento = equipamento
        
        $var = $var.to_s + "\n\n" + "Equipamento - #{vars.equipamento}"
        
    end
    
    
    def sem_equipamento(vars)

        equipamento = ''

        vars.equipamento = equipamento

        $var = $var.to_s + "\n\n" + "Cenario de Cotação nao requer equipamentos especificos !!"

    end

    
    def com_protecao(vars)
       
        protecoes = DATA['Protecoes'].sample
        
        vars.protecoes = protecoes
        
        protecao = ' <CoveragePrefs>
                        <CoveragePref CoverageType='"'#{vars.protecoes[0]}'"' />
                        <CoveragePref Code='"'#{vars.protecoes[1]}'"' />
                    </CoveragePrefs>'

        vars.payload_protecao = protecao

        $var = $var.to_s + "\n\n" + "Proteções - #{vars.protecao}"
        
    end
    

    def sem_protecao(vars)

        protecao = ''
        
        vars.payload_protecao = protecao
        
        $var = $var.to_s + "\n\n" + "Cenario de Cotação nao requer proteção especifica !!"
        
    end


    def header_basic(vars)

        header = {"Content-Type": "aplication/xml", "User-Agent": "Automated-Test-Groove"}
        
        vars.header = header

    end

    def select_equipamentos(vars)

        equipamento = DATA['Equipamentos'].sample

        vars.equipamento = equipamento

    end

    def mount_url(ambiente,operadora)
        
        url = DATA[:CONFIG_AMBIENTES][ambiente]["url_#{operadora}"]

        return url
    end

    def calcula_RateTotalAmount(vars,*valores) 

        *valores = valores 

         valor_final = 0
         
         valores.each do |cont|

            valor_final += cont 
            
        end

        vars.rate_total_amount = valor_final.to_s
    end


    def soma_valores_array(vars,*infos)
        total = 0
        valores = 0 

        if infos.include?(nil)

            infos.delete(nil)            
       
        end

        qtd_valor = infos.count - 1 

        while total <= qtd_valor
                
            infos[total].each do |cont|
                        
                valores += cont 

            end  
            total += 1 
        end
    
        vars.estimatedTotalAmountCalculado = valores
    end


    def trata_response(vars,response)        
    
        response = response.body
        
        responseArray = response.split(' ')

        binding.pry

        if responseArray.first != "<?xml"
            
            armazena_warning(vars)
         
        end
       
        qtd_tag = responseArray.count - 1
        
        var = vars.xsd.split(".")

        while responseArray.first != "<#{var[0]}"
            
            responseArray.delete(responseArray.first)

        end
        
        qtd_tag = responseArray.count - 1

        tag_first = responseArray[0].delete("<")
        
        tag_first.freeze

        while qtd_tag > 10 
          
            responseArray.first.gsub!(/[^0-9A-Za-z]/, '') 
           
            responseArray.last.gsub!(/[^0-9A-Za-z]/, '')    
            
            if responseArray.first != responseArray.last

                responseArray.delete(responseArray.last)
            
            else    
                break if responseArray.last == responseArray.first
            end
        end
                
        responseArray[0] = "<#{tag_first}"              

        position = responseArray.count - 1 

        responseArray[position] = "</#{tag_first}>"

        return responseArray
    end
    
    def armazena_warning(vars)

        t = Time.now
        t.to_s
    
        nome_file = ['request',vars.payload_request_tratado,'response',vars.response_hash]
                    
        qtd_file = nome_file.count - 1 

        cont = 0 

        while cont < qtd_file 

            file = File.new("data/warnings/#{nome_file[cont]}Warning_#{t.strftime('%d_%m_%y')}_#{t.strftime('%H_%M_%S')}.xml",'wb') 
            
            cont += 1
            
            file.write(nome_file[cont])

            file.close
            
            cont += 1
            
        end
        
        raise "XML apresentando erro warning" 

    end

    def mount_xml(response_tratado)

        qtd_tag = response_tratado.count - 1

        xmls = ''

        (0..qtd_tag).each do |cont|
            
            xmls << response_tratado[cont] + "\t"
        end

        save_text_all(xmls)
    end
    
    def save_text_all(response)
        
        File.open('data/responseRest.xml','wb') do |file|
       
            file.write("#{response}")

            file.close

        end
    end

    def get_status_banco
                
        response = HTTParty.get('https://xml-hmg.movida.com.br/healthcheck')
        
        valida_status_banco(response)

    end

    
    def valida_status_banco(body)        
        
        banco = ['DATABASE','REDIS','BOOKING-REST']

        x = banco.count - 1 

        (0..x).each do |cont|

            if body['services'][banco[cont]].include?('errors') == false

                p body['services'][banco[cont]]['status']

            else

                raise "#{body['services'][banco[cont]]['name']} - #{body['services'][banco[cont]]['errors'].to_param}"            

            end        
        end
    end
end