class Payloads

   def payload_cotacao(vars)

      xml = '
         <OTA_VehAvailRateRQ xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd" Target="Production" Version="2.000" TransactionIdentifier="SCA" SequenceNmbr="1" PrimaryLangID="EN">
            <POS>
               <Source AgentSine="A" PseudoCityCode="NCE1A0955" ISOCountry="FR" ISOCurrency="EUR" AgentDutyCode="GS" AirlineVendorID="1A" AirportCode="NCE">
               <RequestorID Type="5" ID='"'#{vars.parceiro}'"' ID_Context="IATA" />       
               </Source>   
            </POS>            
            <VehAvailRQCore>         
               <VehRentalCore PickUpDateTime='"'#{vars.dates[0]}'"' ReturnDateTime='"'#{vars.dates[1]}'"'>            
                  <PickUpLocation LocationCode='"'#{vars.filiais[0]}'"' CodeContext="IATA" />           
                  <ReturnLocation LocationCode='"'#{vars.filiais[1]}'"' CodeContext="IATA" />            
               </VehRentalCore>           
               <VendorPrefs>            
                  <VendorPref Code="MO" />              
                  <VehPref Code='"'#{vars.car}'"' CodeContext="ACRISS"/>             
                </VendorPrefs>                           
                <SpecialEquipPrefs>                 
                  <SpecialEquipPref EquipType='"'#{vars.equipamento[0]}'"' Quantity="1"/>               
                </SpecialEquipPrefs>        
                </VehAvailRQCore>          
            <VehAvailRQInfo>                 
            <Customer>                  
               <!-- condutor adicional -->       
               '"'#{vars.adicional}'"'  
            </Customer>  
            <ArrivalDetails TransportationCode="14" Number="2935">     
               <OperatingCompany Code="AA" />
            </ArrivalDetails>
            <!-- preferencia por determinadas protecoes - Tabela de protecoes OTA Veh ConverageTypes -->
            '"'#{vars.payload_protecao}'"'
         </VehAvailRQInfo>
         </OTA_VehAvailRateRQ>'

         p vars.parceiro
         p vars.dates[0]
         p vars.dates[1]
         p vars.filiais[0]
         p vars.filiais[1]
         p vars.car
         p "Proteções - #{vars.protecao}"
         p vars.equipamento[0]
         p vars.equipamento[1]
      
       return xml
   end


   def payload_reserva(vars)

      xml = '<OTA_VehResRQ xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_VehResRQ.xsd" Target="Production" Version="2.000" TransactionIdentifier="Book" SequenceNmbr="12536" PrimaryLangID="EN">
               <POS>
                  <Source AgentSine="A" PseudoCityCode="NCE1A0955" ISOCountry="FR" ISOCurrency="EUR" AgentDutyCode="GS" AirlineVendorID="1A" AirportCode="NCE">
                  <RequestorID Type="5" ID='"'#{vars.parceiro}'"' ID_Context="IATA" />       
                  </Source>   
               </POS>   
               <VehResRQCore OptionChangeIndicator="false">
                  <VehRentalCore PickUpDateTime='"'#{vars.dates[0]}'"' ReturnDateTime='"'#{vars.dates[1]}'"'>            
                  <PickUpLocation LocationCode='"'#{vars.filiais[0]}'"' CodeContext="IATA" />           
                  <ReturnLocation LocationCode='"'#{vars.filiais[1]}'"' CodeContext="IATA" />            
                  </VehRentalCore>          
               <Customer>
               <Primary>
                  <PersonName>
                     <NamePrefix>Mr.</NamePrefix>
                     <GivenName>QA</GivenName>
                     <Surname>Teste</Surname>
                     <NameSuffix></NameSuffix>
	               </PersonName>
               </Primary>
               '"'#{vars.adicional}'"'   
                  </Customer>
               <VendorPref Code="MO"/>
               <VehPref Code='"'#{vars.car}'"' CodeContext="ACRISS"/>           
                  <SpecialEquipPrefs>
                     <SpecialEquipPref EquipType='"'#{vars.equipamento[0]}'"' Quantity="1"/>               
                  </SpecialEquipPrefs>
               </VehResRQCore>
            <VehResRQInfo ResStatus="Book">
             '"'#{vars.payload_protecao}'"'
         </VehResRQInfo>
         </OTA_VehResRQ>'

         return xml
      end


      def payload_commit(vars)

         begin

            xml = '<OTA_VehResRQ xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_VehResRQ.xsd" Target="Production" Version="2.000" TransactionIdentifier="Commit" SequenceNmbr="12536" PrimaryLangID="EN">
                     <POS>
                        <Source PseudoCityCode="RIO" ISOCountry="BR" ISOCurrency="BRL" AirlineVendorID="1A"/>
                     </POS>
                     <VehResRQCore>
                        <UniqueID Type="24" ID="WSOPA" ID_Context="AMADEUS"/>
                        <UniqueID Type="8" ID='"'#{vars.dados_commit[3]}'"' />
                        <VehRentalCore/>
                        <Customer>
                           '"'#{vars.adicional}'"'                    
                        </Customer>
                        <VendorPref Code="ZT"/>
                        <VehPref Code='"'#{vars.car}'"' CodeContext="ACRISS"/>
                        <RateQualifier RateCategory="16"/>
                     </VehResRQCore>
                     <VehResRQInfo ResStatus="Commit"/>
                  </OTA_VehResRQ>'
         rescue NoMethodError
            


         end

         return xml

      end


      def payload_cancelamento(vars)

         xml = '<OTA_VehResRQ xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_VehResRQ.xsd" Target="Production" Version="2.000" TransactionIdentifier="Cancel" SequenceNmbr="35073" PrimaryLangID="EN">
                  <POS>
                     <Source PseudoCityCode="RIO" ISOCountry="BR" ISOCurrency="BRL" AirlineVendorID="1A">
                        <RequestorID Type="5" ID='"'#{vars.parceiro}'"' ID_Context="IATA"/>
                     </Source>
                  </POS>
                  <VehResRQCore> 
                     <UniqueID Type="24"  ID='"'#{vars.dados_commit[1]}'"'  ID_Context="AMADEUS" />
                     <UniqueID Type="8"  ID='"'#{vars.dados_commit[3]}'"'/>
                     <VehRentalCore PickUpDateTime='"'#{vars.dates[0]}'"' ReturnDateTime='"'#{vars.dates[1]}'"' >
                        <PickUpLocation LocationCode='"'#{vars.filiais[0]}'"' CodeContext="IATA" />           
                        <ReturnLocation LocationCode='"'#{vars.filiais[1]}'"' CodeContext="IATA" />   
                     </VehRentalCore>
                     <VendorPref Code="MO"/>
                     <VehPref Code='"'#{vars.car}'"' CodeContext="ACRISS"/>
                     <RateQualifier RateCategory="16"/>
                  </VehResRQCore>
			         <VehResRQInfo ResStatus="Cancel"/>
               </OTA_VehResRQ>'

         return xml
      end

      def payload_cancelamento_confid(vars)

         xml = '<OTA_VehResRQ xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_VehResRQ.xsd" Target="Production" Version="2.000" TransactionIdentifier="Cancel" SequenceNmbr="35073" PrimaryLangID="EN">
                  <POS>
                     <Source PseudoCityCode="RIO" ISOCountry="BR" ISOCurrency="BRL" AirlineVendorID="1A">
                        <RequestorID Type="5" ID='"'#{vars.parceiro}'"' ID_Context="IATA"/>
                     </Source>
                  </POS>
                  <VehResRQCore> 
                     <UniqueID Type="24"  ID='"'#{vars.dados_commit[1]}'"'  ID_Context="AMADEUS" />
                     <UniqueID Type="8"  ID='"'#{vars.dados_commit[3]}'"'/>
                     <VehRentalCore PickUpDateTime='"'#{vars.dates[0]}'"' ReturnDateTime='"'#{vars.dates[1]}'"' >
                        <PickUpLocation LocationCode='"'#{vars.filiais[0]}'"' CodeContext="IATA" />           
                        <ReturnLocation LocationCode='"'#{vars.filiais[1]}'"' CodeContext="IATA" />   
                     </VehRentalCore>
                     <VendorPref Code="MO"/>
                     <VehPref Code='"'#{vars.car}'"' CodeContext="ACRISS"/>
                     <RateQualifier RateCategory="16"/>
                  </VehResRQCore>
			         <VehResRQInfo ResStatus="Cancel"/>
               </OTA_VehResRQ>'

         return xml
      end










































































#       def payload_cotacao(vars)
         
#          xml = '<?xml version="1.0" encoding="UTF-8"?>
#          <OTA_VehAvailRateRQ xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd" Version="1.002">
#             '"'#{vars.payload_request}'"'
#             <VehAvailRQCore Status="Available">
#                <VehRentalCore PickUpDateTime='"'#{vars.dates[0]}'"' ReturnDateTime='"'#{vars.dates[1]}'"'>
#                   <PickUpLocation LocationCode='"'#{vars.filiais[0]}'"' />
#                   <ReturnLocation LocationCode='"'#{vars.filiais[1]}'"'>
#                </ReturnLocation>
#                </VehRentalCore>
#                <VehPrefs>
#                 <VehPref Code='"'#{vars.car}'"' CodeContext="ACRISS"/>
#                </VehPrefs>
#                <SpecialEquipPrefs>
#                 <SpecialEquipPref EquipType='"'#{vars.equipamento[0]}'"' Quantity="1"/>
#                </SpecialEquipPrefs>
#                <RateQualifier RateQualifier="" PromotionCode=""/>	
#                </VehAvailRQCore>
#             <VehAvailRQInfo>
#                <Customer>
#                   <Primary>
#                      <PersonName>
#                         <GivenName>Henrique</GivenName>
#                         <Surname>Silveira</Surname>
#                      </PersonName>
#                   </Primary>
#                   '"'#{vars.adicional}'"'
#                </Customer>
#                '"'#{vars.payload_protecao}'"'
#             </VehAvailRQInfo>
#          </OTA_VehAvailRateRQ>'
        
#          p vars.dates[0]
#          p vars.dates[1]
#          p vars.filiais[0]
#          p vars.filiais[1]
#          p vars.car
#          p vars.equipamento[0]
#          p vars.equipamento[1]

#          return xml
#       end

#       def payload_rateRule(vars)
        
#          xml = '<?xml version="1.0" encoding="UTF-8"?>
#          <OTA_VehRateRuleRQ xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_VehRateRuleRQ.xsd" Target="Production" Version="2.000" SequenceNmbr="1" PrimaryLangID="EN" CompanyShortName="MO">
#          '"'#{vars.payload_request}'"'
#             <RentalInfo>
#                <VehRentalCore PickUpDateTime='"'#{vars.dates[0]}'"' ReturnDateTime='"'#{vars.dates[1]}'"'>
#                   <PickUpLocation LocationCode='"'#{vars.filiais[0]}'"' />
#                   <ReturnLocation LocationCode='"'#{vars.filiais[1]}'"'>
#                </ReturnLocation>
#                </VehRentalCore>
#                <VehicleInfo Code='"'#{vars.car}'"' CodeContext="ACRISS" />
#                <SpecialEquipPrefs>
#                   <SpecialEquipPref EquipType='"'#{vars.equipamento[0]}'"' Quantity="1" />
#                </SpecialEquipPrefs>
#                <RateQualifier RateQualifier="" PromotionCode="" />
#             </RentalInfo>
#             <VehAvailRQInfo>
#                <Customer>
#                   <Primary>
#                      <PersonName>
#                         <GivenName>John</GivenName>
#                         <Surname>Doe</Surname>
#                      </PersonName>
#                   </Primary>
#                   '"'#{vars.adicional}'"'
#                </Customer>
#                '"'#{vars.payload_protecao}'"'
#             </VehAvailRQInfo>
#          </OTA_VehRateRuleRQ>'
       
#          p vars.dates[0]
#          p vars.dates[1]
#          p vars.filiais[0]
#          p vars.filiais[1]
#          p vars.car
#          p vars.equipamento[0]
#          p vars.equipamento[1]
#          p vars.adicional
#          p vars.payload_protecao
#          return xml

#       end



#       def payload_reserva(vars)

#          xml = '<?xml version="1.0" encoding="UTF-8"?>
#          <OTA_VehResRQ xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05     OTA_VehResRQ.xsd" Version="1.002">
#          '"'#{vars.payload_request}'"'
#             <VehResRQCore>
#                <VehRentalCore PickUpDateTime='"'#{vars.dates[0]}'"' ReturnDateTime='"'#{vars.dates[1]}'"'>
#                <PickUpLocation LocationCode='"'#{vars.filiais[0]}'"' />
#                <ReturnLocation LocationCode='"'#{vars.filiais[1]}'"' />
#                </VehRentalCore>
#                <Customer>
#                   <Primary>
#                      <PersonName>
#                         <NamePrefix>QA.</NamePrefix>
#                         <GivenName>AUTOMACAO</GivenName>
#                         <Surname>TESTE</Surname>
#                         <NameSuffix />
#                      </PersonName>
#                   </Primary>
#                   '"'#{vars.adicional}'"'
#                   <VehPrefs>
#                      <VehPref Code='"'#{vars.car}'"' CodeContext="ACRISS"/>
#                   </VehPrefs>
#                   <PaymentForm>
#                      <Voucher ValueType="FPF" />
#                   </PaymentForm>
#                </Customer>
#                <SpecialEquipPrefs>
#                   <SpecialEquipPref EquipType='"'#{vars.equipamento[0]}'"' Quantity="1"/>
#                </SpecialEquipPrefs>
#                <RateQualifier RateQualifier="" PromotionCode="" RateAuthorizationCode ='"'#{vars.authorizationCode}'"'/>
#             </VehResRQCore>
#             <VehResRQInfo>
#             '"'#{vars.payload_protecao}'"'
#             </VehResRQInfo>
#          </OTA_VehResRQ>
#          '
#       end
         


#       def payload_cancelamento(vars)

#          xml = '<?xml version="1.0" encoding="UTF-8"?>
#          <OTA_VehCancelRQ xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_VehCancelRQ.xsd" Version="1.002">
#          '"'#{vars.payload_request}'"'
#             <VehCancelRQCore CancelType="Book">
#                <UniqueID Type="8" ID='"'#{vars.dados_commit[1]}'"'/>
#             </VehCancelRQCore>
#          </OTA_VehCancelRQ>'
#       end



#       def pos_latam_ws3(vars)

#          pay = '<POS>         
#                   <Source>                    
#                      <RequestorID Type="5" ID='"'#{vars.parceiro}'"' MessagePassword='"'#{vars.password}'"' ID_Context="MOVIDA"/>
#                    </Source>     
#                </POS>'
         
#          vars.payload_request = pay
         
#          p vars.parceiro
#          p vars.password
         
#       end

#       def pos_Rentcars_ws3(vars)

#          pay = '<POS>         
#                   <Source>
#                      <RequestorID Type="5" ID='"'#{vars.parceiro}'"' MessagePassword='"'#{vars.password}'"' ID_Context="MOVIDA" />
#                   </Source> 
#                </POS>'
         
#          vars.payload_request = pay
         
#          p vars.parceiro
#          p vars.password
         
#       end

end



