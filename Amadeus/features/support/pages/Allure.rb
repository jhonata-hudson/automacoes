class Allure

  def config_environment

    environment = []

    environment.push({"name"=>"Ambiente","values"=>[$ambientes]})

    environment.push({"name"=>"Endpoint","values"=>[$endpoint]})

    environment.push({"name"=>"Pedidos abertos","values"=>$pedidos})

    return environment

  end


  def config_category

    categories = []

    categories.push({"name"=>"Teste pulado","matchedStatuses"=>["skipped"]})

    categories.push({"name"=>"Teste completo com sucesso","matchedStatuses"=>["passed"]})

    categories.push({"name"=>"Elemento ausente","matchedStatuses"=>["failed"],"messageRegex"=>".*nil*"})

    categories_config = File.open("#{Dir.pwd}/data/reports/allure/categories.json","w")

    categories_config.puts(categories.to_json)

    categories_config.close

  end


  def build_report

    report_name = "Report_#{Dir.pwd.split('/').last}"

    if $ambientes == nil

      $ambientes = 'default'

    end

    if File.directory?("#{Dir.pwd}/data/reports/#{$ambientes}")

      reports = Dir.new("#{Dir.pwd}/data/reports/#{$ambientes}").entries.reject { |f| File.directory? f }

      if reports.last != nil

      reports = reports.sort_by{|s| [s.length, s]}

        if reports.count >= 10

          FileUtils.rm_rf("#{Dir.pwd}/data/reports/#{$ambientes}/#{reports.first}")

          reports = reports - [reports.first]

        end

        report_number = reports.last.split('_').last.to_i

        report_number += 1

        report_name = "#{report_name}_#{report_number}"

      else

        report_name = "#{report_name}_1"

        reports = []

      end

    else

      report_name = "#{report_name}_1"

      reports = []

    end

    if !File.exists?("#{Dir.pwd}/data/reports/allure/")

      Dir.mkdir("#{Dir.pwd}/data/reports/allure/")

    end

    config_category

    system("allure generate #{Dir.pwd}/data/reports/allure --clean -o #{Dir.pwd}/data/reports/#{$ambientes}/#{report_name}/")

    reports.push(report_name)

    reports = reports.reverse

    widget_path = "#{Dir.pwd}/data/reports/#{$ambientes}/#{reports.first}/widgets"

    environment = config_environment

    environment_config = File.open("#{widget_path}/environment.json","w")

    environment_config.puts(environment.to_json)

    environment_config.close

    build_hist(reports,'history')

    build_hist(reports,'duration')

    build_hist(reports,'categories')

    #build_hist(reports,'retry')

    FileUtils.rm_rf("#{Dir.pwd}/data/reports/#{$ambientes}/#{report_name}/history/retry-trend.json")

    FileUtils.rm_rf("#{widget_path}/retry-trend.json")

    return report_name

  end


  
  def build_hist(reports, type)

    full_trend = []

    tot_trend = reports.count

    cont_trend = 0

    while cont_trend < tot_trend

      trend_path = "data/reports/#{$ambientes}/#{reports[cont_trend]}/history"

      trend_data = JSON.parse(File.open("#{trend_path}/#{type}-trend.json","r").read)[0]

      cur_trend = {}

      cur_trend['buildOrder'] = reports[cont_trend].split('_').last.to_i

      cur_trend['reportName'] = "Report_#{reports[cont_trend]}"

      cur_trend['data'] = trend_data['data']

      full_trend.push(cur_trend)

      cont_trend += 1

    end

    widget_path = "#{Dir.pwd}/data/reports/#{$ambientes}/#{reports.first}/widgets"

    trend_config = File.open("#{widget_path}/#{type}-trend.json","w")

    trend_config.puts(full_trend.to_json)

    trend_config.close

  end

  def attachments(scn)

    folder_report_data = "data/reports/attachments"

    if !File.exists?(folder_report_data)

      Dir.mkdir(folder_report_data)

    end

    temp_text = "#{folder_report_data}/temp_text.txt"

    new_text = File.open(temp_text, "w")

    new_text.puts($var)

    new_text.close

    AllureCucumber::DSL.attach_file('report', new_text)

    $var = ""

  end

end
