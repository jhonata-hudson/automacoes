class Dates 

    def date_1_dia_de_reserva_sem_hora_extra

        data_inicial = Time.now + 86400 * 20
         
        dia_mes_ano = data_inicial.strftime("%Y-%m-%d")

        hora_min = data_inicial.strftime("%H:%M")            

        data_inicial = "#{dia_mes_ano}T#{hora_min}:00"
       
        ##############################################

        data_inicial_new = Time.now + 86400 * 10

        dia_mes_ano_rest = data_inicial_new.strftime("%d-%m-%Y")

        $data_ida_booking_rest = "#{dia_mes_ano_rest.gsub!("-","/")} #{hora_min}"
       
        ####################################

        data_final = Time.now + 86400 * 30
        
        dia_mes_ano = data_final.strftime("%Y-%m-%d")

        hora_min = data_final.strftime("%H:%M")            

        data_final = "#{dia_mes_ano}T#{hora_min}:00"
       
        ################################################
       
        data_final_new = Time.now + 86400 * 30

        dia_mes_ano_rest = data_final_new.strftime("%d-%m-%Y")
       
        $data_volta_booking_rest = "#{dia_mes_ano_rest.gsub!("-","/")} #{hora_min}"
 
        return data_inicial, data_final
    end

    def date_1_dia_de_reserva_com_hora_extra

        data_inicial = Time.now + 86400 * 10
         
        dia_mes_ano = data_inicial.strftime("%Y-%m-%d")

        hora_min = data_inicial.strftime("%H:%M")            

        data_inicial = "#{dia_mes_ano}T#{hora_min}:00"

        ##############################################

        data_inicial_new = Time.now + 86400 * 10

        dia_mes_ano_rest = data_inicial_new.strftime("%d-%m-%Y")

        $data_ida_booking_rest = "#{dia_mes_ano_rest.gsub!("-","/")} #{hora_min}"

        ####################################

        data_final = Time.now + 86400 * 11 + 10000
        
        dia_mes_ano = data_final.strftime("%Y-%m-%d")

        hora_min = data_final.strftime("%H:%M")            

        data_final = "#{dia_mes_ano}T#{hora_min}:00"
        ################################################
       
        data_final_new = Time.now + 86400 * 30

        dia_mes_ano_rest = data_final_new.strftime("%d-%m-%Y")
       
        $data_volta_booking_rest = "#{dia_mes_ano_rest.gsub!("-","/")} #{hora_min}"

        return data_inicial, data_final
    end

    def date_30_dias_de_reserva_com_hora_extra

        data_inicial = Time.now + 86400 * 10
         
        dia_mes_ano = data_inicial.strftime("%Y-%m-%d")

        hora_min = data_inicial.strftime("%H:%M")            

        data_inicial = "#{dia_mes_ano}T#{hora_min}:00"
       
        ##############################################

        data_inicial_new = Time.now + 86400 * 10

        dia_mes_ano_rest = data_inicial_new.strftime("%d-%m-%Y")

        $data_ida_booking_rest = "#{dia_mes_ano_rest.gsub!("-","/")} #{hora_min}"
        ####################################

        data_final = Time.now + 86400 * 40 + 20000
        
        dia_mes_ano = data_final.strftime("%Y-%m-%d")

        hora_min = data_final.strftime("%H:%M")            

        data_final = "#{dia_mes_ano}T#{hora_min}:00"
        
         ################################################
       
         data_final_new = Time.now + 86400 * 30

         dia_mes_ano_rest = data_final_new.strftime("%d-%m-%Y")
        
         $data_volta_booking_rest = "#{dia_mes_ano_rest.gsub!("-","/")} #{hora_min}"

        return data_inicial, data_final
    end

    def date_30_dias_de_reserva_sem_hora_extra

        data_inicial = Time.now + 86400 * 10
         
        dia_mes_ano = data_inicial.strftime("%Y-%m-%d")

        hora_min = data_inicial.strftime("%H:%M")            

        data_inicial = "#{dia_mes_ano}T#{hora_min}:00"
        
         ##############################################

         data_inicial_new = Time.now + 86400 * 10

         dia_mes_ano_rest = data_inicial_new.strftime("%d-%m-%Y")
 
         $data_ida_booking_rest = "#{dia_mes_ano_rest.gsub!("-","/")} #{hora_min}"
        ###########################################

        data_final = Time.now + 86400 * 40
        
        dia_mes_ano = data_final.strftime("%Y-%m-%d")

        hora_min = data_final.strftime("%H:%M")            

        data_final = "#{dia_mes_ano}T#{hora_min}:00"
        
        ################################################
       
         data_final_new = Time.now + 86400 * 30

         dia_mes_ano_rest = data_final_new.strftime("%d-%m-%Y")
        
         $data_volta_booking_rest = "#{dia_mes_ano_rest.gsub!("-","/")} #{hora_min}"

        return data_inicial, data_final
    end
end



