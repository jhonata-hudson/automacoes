class Each_tag 

    def vehavailrscore(vars,vehavailrscore)

        vehavailrscore[1].each do |dados| 
            
           send(dados.first.downcase,vars,dados)

        end
    end
   

    def valida_customer(vars,customer)
    
        customer.each do |cont|
    
           send(cont.first,vars,cont)
        end
    end
 
    
    def pricedcoverages(vars,pricedcoverages)

        pricedcoverages[1].each do |dados| 
            
           send(dados.first.downcase,vars,dados)
        
        end
    end

    def vehavailinfo(vars,vehavailinfo)

        vehavailinfo[1].each do |dados|
        
           send(dados.first.downcase,vars,dados)
            
        end
    end


    def vehavailcore(vars,vehavailcore)
    
        vehavailcore[1].each do |dados|
        
           send(dados.first.downcase,vars,dados)
        end
    end


    def vehavails(vars,vehavails)
    
        vehavails[1]['VehAvail'].each do |dados|
                        
           send(dados.first.downcase,vars,dados)
        
        end
    end


    def vehvendoravails(vars,vehvendoravails)

        vehvendoravails[1]['VehVendorAvail'].each do |dados|
            
           send(dados.first.downcase,vars,dados)

        end
    end

    def vehcancelrscore(vars,vehcancelrscore)
       
        $var = $var.to_s + "\n\n" + "Status cancelamento invalido" if vehcancelrscore[1]["CancelStatus"] != "Cancelled"

    end

    def confid(vars,confid)

        dados_confid =  Array.new 

        confid[1].each do |position|
            
            dados_confid << position['Type']
            dados_confid << position['ID']
        end  
        
        vars.dados_commit = dados_confid
    end

    def vendor(vars,vendor)

        $var = $var.to_s + "\n\n" + "valores vendor invalidos" if vendor[1]["CompanyShortName"] != 'MOVIDA' || vendor[1]["Code"] != "MO"        
    
    end
    

    def vehresrscore(vars,vehresrscore)

        vehresrscore[1].each do |dados| 

            send(dados.first.downcase,vars,dados)
            
        end
    end



    def vehreservation(vars,vehreservation)

        dados = vehreservation[1]["VehSegmentCore"]["ConfID"]

        raise "ERRO - Type ConfID INVÁLIDO !! (#{dados["Type"]})" if dados["Type"] != vars.dados_commit[0]    
        
        raise "ERRO - ID ConfID INVÁLIDO !! (#{dados["ID"]})" if dados["ID"] != vars.dados_commit[1]    

        if $cenario == 'commit'

            raise "ERRO - ID_Context INVÁLIDO !! (#{dados["ID_Context"]})" if dados["ID_Context"] != "Committed"    

        end
    end


    def reservationstatus(vars,reservationstatus)
       
        if $cenario ==  'commit'
        
            raise "ERRO - Status do commit INVÁLIDO  !! (#{reservationStatus[1]})" if reservationstatus[1] != "Committed"
        
        else
 
            raise "ERRO - Status do cancelamento INVÁLIDO !! (#{reservationStatus[1]})" if reservationstatus[1] != "Cancelled"

        end
    end


    def errors(vars,errors)
        


        # if errors[1]['Error']['ShortText'].include?('LOJA NAO TEM EXPEDIENTE NA DATA ESPECIFICADA:') || errors[1]['Error']['ShortText'].include?('NENHUM GRUPO DE VEICULO DISPONIVEL')




            
        # end
        if errors[1]['Error']['ShortText'].encoding.to_s != "UTF-8"
       
             raise $var = $var.to_s + "\n\n" + "#{errors[1]['Error']}"  
        
        else            
           
            raise $var = $var.to_s + "\n\n" + "#{errors[1]['Error']}"  

        end

    end
        
    def fees(vars,fees)

        p 'ok'
    end

    def success(vars,success)
        
        p 'ok'

    end


    def locationdetails(vars,locationdetails)

        p 'ok'

    end


    def vehrentalcore(vars,vehrentalcore)
       
        tags = Array.new

        vehrentalcore[1].each do |pos|
            tags << pos.first
        end
    
        qtd_core = tags.count - 1        
        cont = 0
    
        while cont <= qtd_core
            
            if vehrentalcore[1][tags[cont]].include?("ExtendedLocationCode")

                $var = $var.to_s + "\n\n" + "ERRO - Filiais Diferentes ao validar dados de retorno " if vehrentalcore[1][tags[cont]]["ExtendedLocationCode"] != "MO#{vars.filiais[cont]}"
                
                cont += 1
            
            elsif vehrentalcore[1][tags[cont]]

                vars.dates.each do |data|

                    $var = $var.to_s + "\n\n" + "ERRO - Data de retirada ou devolução invalidas" if vehrentalcore[1][tags[cont]] != data ##valida datas
                   
                    cont += 1
                end
            end      
        end
    end

    
    def vehicle(vars,vehicle)
    
        $var = $var.to_s + "\n\n" + "ERRO - Veiculo retornado invalido" if vehicle[1]['Code'] != vars.car
        
    end

    def rentalrate(vars,rentalrate) 
        
        cont = 0         
        infos_rental = rentalrate[1]['VehicleCharges']['VehicleCharge']
        
        
        if vars.status == 'capture infos'

            vars.vehicleCharge = Array.new

            infos_rental.each do |preco|
    
                if preco['IncludedInRate'] == "false" || preco['IncludedInEstTotalInd'] == "false"
    
                    p 'false'
                else
                    
                    if preco['Purpose'] == '83' 

                        vars.vehicleCharge << preco['Amount'].to_f.round(2)  

                        vars.taxa_adm = preco['Amount'].to_f.round(2)  
                        
                        cont += 1

                    # elsif preco['Purpose'] == '12' 
                    
                    #         
                    
                    else 

                        vars.vehicleCharge << preco['Amount'].to_f.round(2)  
                
                        cont += 1
                    end
                end
            end
       
        elsif  vars.status == 'valida vehicleCharge'
        
            $dados = [] 
            
            infos_rental.each do |preco|

                if preco['IncludedInRate'] == "false" || preco['IncludedInEstTotalInd'] == "false"

                    p 'false'

                elsif preco['IncludedInRate'] == "false" || preco['IncludedInEstTotalInd'] == "true"
                    $var = $var.to_s + "\n\n" + "ERRO - #{preco['Description']} - VALOR INVALIDO" if preco['Amount'].to_f.round(2) != vars.vehicleCharge[cont]
                
                    $dados << preco['Amount'].to_f.round(2)

                    cont += 1
                end
            end
        end 
    end
    
    
    def pricedequips(vars,pricedequips)        
        
        cont = 0 
        erro = 0
        qtd_equip = pricedequips[1]['PricedEquip'].count - 1
      
        if vars.status == 'capture infos'
            
            vars.valor_equipamento = Array.new

            while cont <= qtd_equip 

                if pricedequips[1]['PricedEquip'][cont]['Charge']['IncludedInRate'] == 'true'

                    vars.valor_equipamento << pricedequips[1]['PricedEquip'][cont]['Charge']['Amount'].to_f.round(2)
                    
                    $var = $var.to_s + "\n\n" + "Equipamento - #{pricedequips[1]['PricedEquip'][cont]["Equipment"]['Description']} = #{pricedequips[1]['PricedEquip'][cont]['Charge']['IncludedInRate']}"

                    cont += 1 
               
                elsif pricedequips[1]['PricedEquip'][cont]['Charge']['IncludedInRate'] == 'false'
  
                    $var = $var.to_s + "\n\n" + "Equipamento - #{pricedequips[1]['PricedEquip'][cont]["Equipment"]['Description']} = #{pricedequips[1]['PricedEquip'][cont]['Charge']['IncludedInRate']}"

                    cont += 1 
                end      
           end
          
           $var = $var.to_s + "\n\n" + "Equipamento solicitado nao é retornado" if vars.valor_equipamento.empty?

        elsif vars.status == 'valida vehicleCharge'
            
            if qtd_equip > 2 
        
                while cont <= qtd_equip 
                
                    if pricedequips[1]['PricedEquip'][cont]["Equipment"]["EquipType"] != vars.equipamento[0]  && pricedequips[1]['PricedEquip'][cont]["Equipment"]["Description"] != vars.equipamento[1]
                                                            
                        $var = $var.to_s + "\n\n" + "Equipamento  - #{pricedequips[1]['PricedEquip'][cont]["Equipment"]['Description']} = #{pricedequips[1]['PricedEquip'][cont]['Charge']['IncludedInRate']}"

                        cont += 1 
                    
                        erro += 1        
                        
                        $var.to_s + "\n\n" + "Equipamento solicitado nao retornado" if erro == qtd_equip        
                    else
                        
                        $var = $var.to_s + "\n\n" + "Equipamento  - #{pricedequips[1]['PricedEquip'][cont]["Equipment"]['Description']} = #{pricedequips[1]['PricedEquip'][cont]['Charge']['IncludedInRate']} - Sucesso !!"

                        break if pricedequips[1]['PricedEquip'][cont]["Equipment"]["EquipType"] == vars.equipamento[0]  && pricedequips[1]['PricedEquip'][cont]["Equipment"]["Description"] == vars.equipamento[1]
                    end
                end
            
            else

                pricedequips[1].each do |equip|
                    
                    $var = $var.to_s + "\n\n" + "ERRO - Equipamento invalido" if equip[1]["Equipment"]["EquipType"] != vars.equipamento[0]  
                                
                    p  "ERRO - Equipamento invalido" if equip[1]["Equipment"]["Description"] != vars.equipamento[1]

                end
            end
        end
    end
       
    
    def customer(vars,customer)

        customer[1].each do |dados|

            send(dados.first.downcase,vars,dados)

        end
    end

    def additional(vars,additional)
        p 'ok'

    end

    def primary(vars,primary)
            p  'ok'
    
            ###METODO PRONTO PARA VALIDAÇAO DO MESMO CASO SEJA NECESSARIO
            
    end
        
    def totalcharge(vars,totalcharge)

        if vars.status == 'capture infos'
            
            tarifa = totalcharge[1]["EstimatedTotalAmount"].to_f.round(2) - totalcharge[1]["RateTotalAmount"].to_f.round(2)

            if tarifa.to_f.round(2) != vars.taxa_adm.to_f.round(2)

                $var = $var.to_s + "\n\n" + "ERRO - VALIDAÇÃO TARIFA INVÁLIDA !!"

            else
                 
                $var = $var.to_s + "\n\n" + "VALIDACAO TARIFA OK !!"
            end
    
            vars.estimatedTotalAmountResponse = totalcharge[1]["EstimatedTotalAmount"].to_f.round(2)

        elsif vars.status == 'valida vehicleCharge'

            $var = $var.to_s + "\n\n" + "VALIDAÇÃO ESTIMATED TOTAL AMOUNT DADOS DE RETORNO"

            if vars.estimatedTotalAmountResponse.to_f.round(2) !=  vars.estimatedTotalAmountCalculado.to_f.round(2)
                
                  $var = $var.to_s + "\n\n" + "ERRO - EstimatedTotalAmount Invalido. VALOR ESPERADO: R$#{vars.estimatedTotalAmountCalculado.to_f.round(2)} -  VALOR RETORNADO: R$#{vars.estimatedTotalAmountResponse}"        
            
            else    

                $var = $var.to_s + "\n\n" + "SUCESSO - ESTIMATE TOTAL AMOUNT CALCULADO #{vars.estimatedTotalAmountCalculado.to_f.round(2)}" 
              
                $var = $var.to_s + "\n\n" + "SUCESSO - ESTIMATE TOTAL AMOUNT RESPONSE #{vars.estimatedTotalAmountResponse.to_f.round(2)}" 
            end
        
            rate_total_amount = totalcharge[1]['RateTotalAmount']

            if rate_total_amount.to_f.round(2) == vars.estimatedTotalAmountResponse && vars.taxa_adm == 0.0

                $var = $var.to_s + "\n\n" + "TAXA INCLUSA NO VALOR LIQUIDO"

            else 
                
                Validate.new.valida_calculo_valor(vars,rate_total_amount)
            end
        end
    end


    def tpa_extensions(vars,tpa_extensions)

        p 'ok'

    end


    def vehavailrsinfo(vars,vehavailrsinfo)

        p 'ok'
    end

    def pricedcoverage(vars,pricedcoverage)        
        cont = 0  
        erro = 0 
        
        if vars.protecoes != nil 
            
            qtd_coverage = pricedcoverage[1].count - 1
            
            if vars.status == 'capture infos'
                
                vars.name_protecao = Array.new
                vars.valores_protecoes = Array.new
                
                while cont <= qtd_coverage 

                    if pricedcoverage[1][cont]['Charge']['IncludedInRate'] == 'true'

                        vars.valores_protecoes << pricedcoverage[1][cont]['Charge']['Amount'].to_f.round(2)

                        vars.name_protecao << pricedcoverage[1][cont]['Coverage']['CoverageType']

                        vars.name_protecao << pricedcoverage[1][cont]['Coverage']['Code']

                        cont += 1 

                    elsif  pricedcoverage[1][cont]['Charge']['IncludedInRate'] == 'false'

                        cont += 1 
                        erro += 1 
                        
                    end
                end
                $var = $var.to_s + "\n\n" + "protecao nao retornada" if vars.valores_protecoes.empty?
       
            elsif vars.status == 'valida vehicleCharge'

                if vars.name_protecao.include?(vars.protecoes[0]) == false && vars.name_protecao.include?(vars.protecoes[1]) == false

                    $var = $var.to_s + "\n\n" + "PROTEÇOES NAO RETORNADAS"

                else 

                    $var = $var.to_s + "\n\n" + "PROTEÇOES RETORNADAS COM SUCESSO"

                end
            end
        else
 
            vars.name_protecao = Array.new
            vars.valores_protecoes = Array.new
            qtd_coverage = pricedcoverage[1].count - 1
 
            while cont <= qtd_coverage 

                if pricedcoverage[1][cont]['Charge']['IncludedInRate'] == 'true'

                    vars.valores_protecoes << pricedcoverage[1][cont]['Charge']['Amount'].to_f.round(2)

                    vars.name_protecao << pricedcoverage[1][cont]['Coverage']['CoverageType']

                    vars.name_protecao << pricedcoverage[1][cont]['Coverage']['Code']

                    cont += 1 
               
                elsif  pricedcoverage[1][cont]['Charge']['IncludedInRate'] == 'false'

                    cont += 1 
                    
                end        
            end 
        end
    end

    def info(vars,infos)
        

        p  'ok'

    end

   
    def status(vars,status)

        $var = $var.to_s + "\n\n" + "status invalido" if status[1] != 'Available'

    end











end