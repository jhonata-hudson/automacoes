class Validate 

    def valida_calculo_valor(vars,valor)

        total = BigDecimal(valor) + Percentage(12)
        
        total = total.to_f.round(2)
        
        bruto = valor.to_f + total
                
        vars.taxa_adm = 0.0 if vars.taxa_adm == nil
        
        sobrou_bruto = vars.estimatedTotalAmountResponse - bruto.round(2)
        sobrou_taxa = vars.taxa_adm - total
        # binding.pry
        begin
            if vars.taxa_adm == 0.0 
         
                 "Valor de calculo invalido"  if valor.to_f - vars.estimatedTotalAmountResponse != vars.taxa_adm
                        # binding.pry

            elsif sobrou_bruto.round(2) == 0.01 || sobrou_taxa.round(2) == 0.01
                
                $var = $var.to_s + "\n\n" +  "TAXA ADM Calculado: #{total.round(2)} / TAXA ADM Retornado: #{vars.taxa_adm.round(2)} \nValor Bruto Calulado: #{bruto.round(2)} / Valor Bruto Response: #{vars.estimatedTotalAmountResponse.round(2)} | OK" 
            
            elsif bruto.round(2) != vars.estimatedTotalAmountResponse.round(2) || total.round(2) != vars.taxa_adm.round(2)
         

                 $var = $var.to_s + "\n\n" +  "TAXA ADM Calulado: #{total.round(2)} / TAXA ADM Retornado: #{vars.taxa_adm.round(2)} \nValor Bruto Calulado: #{bruto.round(2)} / Valor Bruto Response: #{vars.estimatedTotalAmountResponse.round(2)} | FALHOU" 
            else 
                
                $var = $var.to_s + "\n\n" +  "TAXA ADM Calulado: #{total.round(2)} / TAXA ADM Retornado: #{vars.taxa_adm.round(2)} \nValor Bruto Calulado: #{bruto.round(2)} / Valor Bruto Response: #{vars.estimatedTotalAmountResponse.round(2)} | OK" 
                
            end
        rescue NoMethodError
     
            
            
        end
    end
    
    
    def valida_tarifa(vars,infos_cars,vehicleCharge)
        
        surcharge = infos_cars['TotalCharge']['EstimatedTotalAmount'].to_f.round(2) - infos_cars['TotalCharge']['RateTotalAmount'].to_f.round(2)
        
        if surcharge.to_f.round(2) != vars.taxa_adm.to_f.round(2)
            
             $var = $var.to_s + "\n\n" + "VALOR SURCHARGE ERRO. VALOR ESPERADO: R$#{surcharge.to_f.round(2)} VALOR RETORNADO: R$#{vars.taxa_adm} ERRO !! "
        
        else 
            
            $var = $var.to_s + "\n\n" + "VALOR SURCHARGE OK. VALOR ESPERADO: R$#{surcharge.to_f.round(2)} VALOR RETORNADO: R$#{vars.taxa_adm} SUCESSO !! " 
        end
    end

    def validacoes_response(vars,*infos)

        @helper = Helpers.new

        valid_encode(infos[0])

        valida_status_code(vars,infos[0])

        @helper.defined_traces(vars,infos[0])
    
        send("verifica_xml_valido_#{infos[2]}",vars)
                
        response_tratado = @helper.trata_response(vars,infos[0])
        
        @helper.mount_xml(response_tratado)

        valida_xsd(infos[1])
    end


    def valida_cancelamento(vars,*infos)
       
        validacoes_response(vars,infos[0],"#{infos[1]}","#{infos[2]}")

        valida_dados_retorno_cancelamento(vars)
   
    end

    def valida_dados_retorno_cancelamento(vars)
        
        caminho_comum = vars.response_hash['soap:Envelope']['soap:Body']['OTA_VehResRS']
        
        caminho_comum.each do |dados|
            
            break if dados.first.downcase == "xmlns"

            Each_tag.new.send(dados[0].downcase,vars,dados)
        
        end
    end

  
    def valida_cotacao(vars,*infos)

        validacoes_response(vars,infos[0],"#{infos[1]}","#{infos[2]}")
        
        vars.status = 'capture infos'
        
        Metodos_Comum.new.capture_infos_car_cotacao(vars,infos[0])

        Helpers.new.soma_valores_array(vars,vars.vehicleCharge,vars.valores_protecoes,vars.valor_equipamento)
                        
        vars.status = 'valida vehicleCharge'

        valida_dados_retorno_cotacao(vars)
    end
    

    def valida_reserva(vars,*infos)
        
        validacoes_response(vars,infos[0],"#{infos[1]}","#{infos[2]}")
                
        valida_dados_retorno_reserva(vars)
        
    end
    
    def valida_commit(vars,*infos)

 

        validacoes_response(vars,infos[0],"#{infos[1]}","#{infos[2]}")

        valida_dados_retorno_commit(vars)
    end

    def valida_rateRule(vars,*infos)
        
        vars.status = 'capture infos'
        
        validacoes_response(vars,infos[0],"#{infos[1]}","#{infos[2]}")
        
        Metodos_Comum.new.capture_infos_car_rateRule(vars,infos[0])
        
        Helpers.new.soma_valores_array(vars,vars.vehicleCharge,vars.valores_protecoes,vars.valor_equipamento)
        
        vars.status = 'valida vehicleCharge'
        
        valida_dados_rateRule(vars)

    end


    def valida_dados_rateRule(vars)

        caminho_comum = vars.response_hash['soap:Envelope']['soap:Body']['OTA_VehRateRuleRS']

        caminho_comum.each do |dados|
            
            break if dados.first.downcase == "target"

            Each_tag.new.send(dados.first.downcase,vars,dados)

        end
    end

    def valida_dados_retorno_commit(vars)

        caminho_comum = vars.response_hash['soap:Envelope']['soap:Body']['OTA_VehResRS']

        caminho_comum.each do |dados|

            break if dados.first.downcase == "xmlns"

            Each_tag.new.send(dados.first.downcase,vars,dados)

        end
    end


    def valida_xsd(xsd)
       
        $var = $var.to_s + "\n\n" + "VALIDAÇÃO XSD #####################"
       
            xml = XML::Document.file('data/responseRest.xml')
            
            schema_document = XML::Document.file("Xsd_amadeus/#{xsd}")
            
            schema = XML::Schema.document(schema_document)
            
            begin
               
                $var = $var.to_s + "\n\n" + "#{xml.validate_schema(schema)}" 
                
                $var = $var.to_s + "\n\n" + "VALIDAÇAO SCHEMA XSD OK !!"  
                
            rescue LibXML::XML::Error => erro            
                
                $var = $var.to_s + "\n\n" + erro.message;
            end
        
        $var = $var.to_s + "\n\n" + "##################################"
    end
   
    def valida_dados_retorno_cotacao(vars)

        caminho_comum = vars.response_hash['soap:Envelope']['soap:Body']['OTA_VehAvailRateRS']['VehAvailRSCore']

        caminho_comum.each do |dados| 
        
            Each_tag.new.send(dados.first.downcase,vars,dados)
    
        end
    end


    def valida_dados_retorno_reserva(vars)
        
        $var = $var.to_s + "\n\n" + "DADOS RESPONSE OTA_VehResRS"  
        
        caminho_comum = vars.response_hash['soap:Envelope']['soap:Body']['OTA_VehResRS']['VehResRSCore']['VehReservation']['VehSegmentCore']                
        
        caminho_comum.each do |dados| 
            
            Each_tag.new.send(dados.first.downcase,vars,dados)
           
        end        
    end

   
    def Validate_response(vars,*infos)
        
        @helper = Helpers.new

        valid_encode(infos[0])

        valida_status_code(vars,infos[0])

        @helper.defined_traces(vars,infos[0])
       
        verifica_xml_valido(vars)
    end


    def valid_encode(response)

        response.each_char do |character| 

            if character.encoding.to_s != "UTF-8"
            
                 "Caracter invalido fora do padrao UTF-8 - #{z}" 
            end
        end
    end


    def valida_status_code(vars,response)
        
        $var = $var.to_s + "\n\n" + "INFOS VALIDACAO RESPONSE"

        if response.code == 200
            
            $var = $var.to_s + "\n\n" + "STATUS CODE OK - #{response.code}" 

        else
           
            $var = $var.to_s + "\n\n" + "FALHA STATUS AO TENTAR #{vars.api}"
       
        end
    end


    def verifica_xml_valido_cotacao(vars)

        $var = $var.to_s + "\n\n" + "INFOS COTACAO"

        if vars.response_hash.include?('soap:Envelope')

            if vars.response_hash['soap:Envelope']['soap:Body']['OTA_VehAvailRateRS'].include?('Errors') == true 
                
                $var = $var.to_s + "\n\n" + "ERRO - #{vars.response_hash['soap:Envelope']['soap:Body']['OTA_VehAvailRateRS']['Errors']['Error']['ShortText']}"
                          
            else    

                $var = $var.to_s + "\n\n" + "RESPONSE OK"

            end                
        else
            $var = $var.to_s + "\n\n" + "ERRO - #{vars.response_hash['br']}"
        end
    end
    

    def verifica_xml_valido_reserva(vars)
       
        $var = $var.to_s + "\n\n" + "INFOS RESERVA"

        if vars.response_hash.include?('soap:Envelope')
                  
            if vars.response_hash['soap:Envelope']['soap:Body']['OTA_VehResRS'].include?('Errors') == true 
                                
                $var = $var.to_s + "\n\n" + "ERRO - #{vars.response_hash['soap:Envelope']['soap:Body']['OTA_VehResRS']['Errors']['Error']['ShortText']}"
                          
            else    

                $var = $var.to_s + "\n\n" + "RESPONSE OK"

            end                
        else
            
            $var = $var.to_s + "\n\n" + "ERRO - #{vars.response_hash['br']}"
        end
    end


    def verifica_xml_valido_commit(vars) 

        $var = $var.to_s + "\n\n" + "INFOS COMMIT"
                
        reservationStatus = vars.response_hash['soap:Envelope']['soap:Body']['OTA_VehResRS']

        unless reservationStatus.include?('Errors') 

            if reservationStatus['VehResRSCore']['ReservationStatus'] != 'Committed' 

                 $var = $var.to_s + "\n\n" + "STATUS ESPERADO: Committed.  STATUS RETORNADO: #{reservationStatus['VehResRSCore']['ReservationStatus']}. ERRO !!"
            else

                $var = $var.to_s + "\n\n" + "STATUS ESPERADO: Committed.  STATUS RETORNADO: #{reservationStatus['VehResRSCore']['ReservationStatus']}. SUCESSO !!"
            end
        else

            $var = $var.to_s + "\n\n" + "ERRO - #{reservationStatus['Errors']['Error']['ShortText']}"
        end
    end


    def verifica_xml_valido_rateRule(vars)

        $var = $var.to_s + "\n\n" + "INFOS COTACAO"

        if vars.response_hash.include?('soap:Envelope')

            if vars.response_hash['soap:Envelope']['soap:Body']['OTA_VehRateRuleRS'].include?('Errors') == true 
                                
                $var = $var.to_s + "\n\n" + "ERRO - #{vars.response_hash['soap:Envelope']['soap:Body']['OTA_VehRateRuleRS']['Errors']['Error']} Rate Rule"
                          
            else    

                $var = $var.to_s + "\n\n" + "RESPONSE OK"

            end                
        else
            $var = $var.to_s + "\n\n" + "ERRO - #{vars.response_hash['br']}"
        end
    end


    def verifica_xml_valido_cancelamento(vars)
        
        $var = $var.to_s + "\n\n" + "###### INFOS CANCELAMENTO ######"

        cancelation_status = vars.response_hash['soap:Envelope']['soap:Body']

        if cancelation_status.include?('OTA_VehCancelRS')
    
            if cancelation_status['OTA_VehCancelRS']['VehCancelRSCore']['CancelStatus'] != 'Cancelled' 

                $var = $var.to_s + "\n\n" + "STATUS ESPERADO: Cancelled, STATUS RETORNADO: #{cancelation_status['OTA_VehCancelRS']['VehCancelRSCore']['CancelStatus']}. ERRO !!"
            
            else 
               
                $var = $var.to_s + "\n\n" + "STATUS ESPERADO: Cancelled, STATUS RETORNADO: #{cancelation_status['OTA_VehCancelRS']['VehCancelRSCore']['CancelStatus']}. SUCESSO !!"
                
            end
        else
          
            p 'ok' 
        end
    end
end



        