      #language: pt


      @Bookingamadeus
      Funcionalidade: Realizar Booking de veiculo selecionado

      # @cancel
      # Cenario: realizar cotação de veiculo mais barato com 1 dia de reserva com filiais iguais sem hora extra, com equipamentos com proteçao com condutor adicional
      
      #       Quando consulto confID

      #       Entao realizo cancelamento validando esquemas xsd

    
      @amadeus3355
      Esquema do Cenario: realizar cotação de veiculo mais barato com 1 dia de reserva com filiais iguais sem hora extra, com equipamentos com proteçao com condutor adicional
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

    

      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | hostway            |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |
         


      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais iguais sem hora extra com equipamentos com proteçao com condutor adicional  
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |




      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes sem hora extra com equipamentos com proteçao com condutor adicional  
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @barato1133
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais com hora extra com equipamentos com proteçao com condutor adicional    
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @barato11344
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais com hora extra com equipamentos com proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |

      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos com proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @baratoeee
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes com hora extra  

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @baratoeee
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes sem hora extra  

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |

      #SEM-EQUIPAMENTOS
      #########################################################################################################################################


      @barato11
      Esquema do Cenario: realizar cotação de veiculo mais barato com 1 dia de reserva com filiais iguais sem hora extra, sem equipamentos com proteçao com condutor adicionalcom condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd




     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais iguais com hora extra sem equipamentos com proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes sem hora extra sem equipamentos com proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes com hora extra sem equipamentos com proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais sem hora extra sem equipamentos com proteçao com condutor adicional    
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |




      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais com hora extra sem equipamentos com proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes sem hora extra sem equipamentos com proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes com hora extra sem equipamentos com proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


      Exemplos:

      | agencia_parceiro |
      | latam            |
      | Rentcars         |

  


      #SEM-PROTECAO
      ############################################################################################################################

      @barato11
      Esquema do Cenario: realizar cotação de veiculo mais barato com 1 dia de reserva com filiais iguais sem hora extra, com equipamentos sem proteçao co
      m condutor adicionalcom condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd



     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais iguais com hora extra com equipamentos sem proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes sem hora extra com equipamentos sem proteçao com condutor adicional  
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes com hora extra com equipamentos sem proteçao com condutor adicional   
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais sem hora extra com equipamentos sem proteçao com condutor adicional    
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |




      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais com hora extra com equipamentos sem proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos sem proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @barato1112
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes com hora extra com equipamentos sem proteçao com condutor adicional  
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      #SEM-CONDUTOR
      ##################################################################################################
      @barato11
      Esquema do Cenario: realizar cotação de veiculo mais barato com 1 dia de reserva com filiais iguais sem hora extra, com equipamentos com proteçao se      m condutor adicionalsem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd



      Exemplos:

      | agencia_parceiro |
      | latam            |
      

      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais iguais com hora extra com equipamentos com proteçao sem condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes sem hora extra com equipamentos com proteçao sem condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |




      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes com hora extra com equipamentos com proteçao sem condutor adicional    
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais sem hora extra com equipamentos com proteçao sem condutor adicional    
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @barato111
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais com hora extra com equipamentos com proteçao sem condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

      Exemplos:

        | agencia_parceiro |
        | Rentcars         |
        | latam            |



      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos com proteçao sem condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |




      @barato11
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes com hora extra com equipamentos com proteçao sem condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

    
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |

      @barato11ww
      Esquema do Cenario: realizar cotação de veiculo mais barato com 1 dia de reserva com filiais iguais sem hora extra, sem equipamentos sem proteçao sem condutor adicionalsem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/sem_equipamento/sem_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

    
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |

@barato1144
      Esquema do Cenario: realizar cotação de veiculo mais barato com 1 dia de reserva com filiais iguais sem hora extra, sem equipamentos sem proteçao sem condutor adicionalsem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/sem_equipamento/sem_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

    
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |

      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################


      @MAIS_CARO
      Esquema do Cenario: realizar cotação de veiculo mais caro com 1 dia de reserva com filiais iguais sem hora extra, com equipamentos com proteçao com condutor adicionalcom condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd



     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais iguais com hora extra com equipamentos com proteçao com condutor adicional  
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |

      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes sem hora extra com equipamentos com proteçao com condutor adicional  
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |





      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes com hora extra com equipamentos com proteçao com condutor adicional    
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais sem hora extra com equipamentos com proteçao com condutor adicional    
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @MAIS_CARO1
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais com hora extra com equipamentos com proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |

      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos com proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes com hora extra com equipamentos com proteçao com condutor adicional 

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      #SEM-EQUIPAMENTOS
      #########################################################################################################################################



      @MAIS_CARO
      Esquema do Cenario: realizar cotação de veiculo mais caro com 1 dia de reserva com filiais iguais sem hora extra, sem equipamentos com proteçao co
      m condutor adicionalcom condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais iguais com hora extra sem equipamentos com proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes sem hora extra sem equipamentos com proteçao com condutor adicional  
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |





      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes com hora extra sem equipamentos com proteçao com condutor adicional     
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais sem hora extra sem equipamentos com proteçao com condutor adicional     
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @MAIS_CARO1
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais com hora extra sem equipamentos com proteçao com condutor adicional  
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |

      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes sem hora extra sem equipamentos com proteçao com condutor adicional  
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes com hora extra sem equipamentos com proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


    
     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      #SEM-PROTECAO
      ############################################################################################################################
      @MAIS_CARO
      Esquema do Cenario: realizar cotação de veiculo mais caro com 1 dia de reserva com filiais iguais sem hora extra, com equipamentos sem proteçao co
      m condutor adicionalcom condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd



      
     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais iguais com hora extra com equipamentos sem proteçao com condutor adicional  
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes sem hora extra com equipamentos sem proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |






      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais sem hora extra com equipamentos sem proteçao com condutor adicional    
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @MAIS_CARO1
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais com hora extra com equipamentos sem proteçao com condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |

      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos sem proteçao com 
      condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes com hora extra com equipamentos sem proteçao com condutor adicional  
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


      
     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      #SEM-CONDUTOR
      ##################################################################################################

      @MAIS_CARO
      Esquema do Cenario: realizar cotação de veiculo mais caro com 1 dia de reserva com filiais iguais sem hora extra, com equipamentos com proteçao se
      m condutor adicionalsem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


      Exemplos:
      
      | agencia_parceiro |
      | Rentcars         |
      | latam            |



      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais iguais com hora extra com equipamentos com proteçao sem condutor adicional   
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes sem hora extra com equipamentos com proteçao sem condutor adicional   
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |





      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes com hora extra com equipamentos com proteçao sem condutor adicional   
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |




      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes com hora extra com equipamentos com proteçao sem condutor adicional   
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |




      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais sem hora extra com equipamentos com proteçao sem condutor adicional    
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |




      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais filiais_diferentes com hora extra com equipamentos com proteçao sem condutor adicional  
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd


     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |

      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos com proteçao sem condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |


       @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos com proteçao sem condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/sem_equipamento/sem_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |
     
       @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos com proteçao sem condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |



      @MAIS_CARO22
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos com proteçao sem condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/sem_equipamento/sem_protecao/sem_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |
     
      


      @MAIS_CARO
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos com proteçao sem condutor adicional 
      
      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"


      E realizo 'cotacao' validando esquemas xsd

      E realizo 'reserva' validando esquemas xsd

      E realizo 'commit' validando esquemas xsd

      Entao realizo 'cancelamento' validando esquemas xsd

     
      Exemplos:

      | agencia_parceiro   |
      | latam              |
      # | trend              |
      # | flytour            |
      # | europlus           |
      # | congonhas_travel   |
      # | artico_american    |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |
