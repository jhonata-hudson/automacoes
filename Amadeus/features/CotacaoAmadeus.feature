      #language: pt

      @Cotacaoamadeus
      Funcionalidade: Validar booking de veiculos, validando diversos tipos de veiculos amadeus


      #  Contexto: Relizar consulta de status banco de dados

      # Dado que verifico status "DATABASE", "BOOKING-REST"

      @amadeus22
      Esquema do Cenario: realizar cotação de veiculo mais barato com 1 dia de reserva com filiais iguais sem hora extra, com equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | artico_american    |
      # | trend              |
      # | flytour            |
      # | latam              |
      # | europlus           |
      # | congonhas_travel   |
      # | hostway            |
      # | multicidades       |
      # | maringa            |
      # | promotional_travel |
      # | augustus           |
      # | invalido           |
      # #




      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais iguais com hora extra com equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |





      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes sem hora extra com equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |






      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes com hora extra com equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |





      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais sem hora extra com equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |





      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais com hora extra com equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd


      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |



      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |





      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes com hora extra

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      #SEM-EQUIPAMENTOS
      #########################################################################################################################################


      @amadeus
      Esquema do Cenario: realizar cotação de veiculo mais barato com 1 dia de reserva com filiais iguais sem hora extra, sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |



      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais iguais com hora extra sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes sem hora extra sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |






      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes com hora extra sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |





      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais sem hora extra sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |






      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais com hora extra sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd


      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes sem hora extra sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes com hora extra sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      #SEM-PROTECAO
      ############################################################################################################################

      @amadeus
      Esquema do Cenario: realizar cotação de veiculo mais barato com 1 dia de reserva com filiais iguais sem hora extra, com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeus44
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais iguais com hora extra com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |





      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes sem hora extra com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes com hora extra com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |





      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais sem hora extra com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |






      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais com hora extra com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd


      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |






      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes com hora extra com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      #SEM-CONDUTOR
      ##################################################################################################
      @amadeus
      Esquema do Cenario: realizar cotação de veiculo mais barato com 1 dia de reserva com filiais iguais sem hora extra, com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais iguais com hora extra com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |





      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes sem hora extra com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |






      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 1 dia de reserva com filiais diferentes com hora extra com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |





      @amadeus333333
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais sem hora extra com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |





      @amadeus1
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais iguais com hora extra com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/sem_equipamento/sem_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |





      @amadeus
      Esquema do Cenario: realizar reserva de veiculo mais barato com 30 dias de reserva com filiais diferentes com hora extra com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_barato", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################
      ############################################################################################################


      @amadeusCaro
      Esquema do Cenario: realizar cotação de veiculo mais caro com 1 dia de reserva com filiais iguais sem hora extra, com equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |



      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais iguais com hora extra com equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes sem hora extra com equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |





      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes com hora extra com equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais sem hora extra com equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeusCaro1
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais com hora extra com equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |



      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes com hora extra

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      #SEM-EQUIPAMENTOS
      #########################################################################################################################################



      @amadeusCaro
      Esquema do Cenario: realizar cotação de veiculo mais caro com 1 dia de reserva com filiais iguais sem hora extra, sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |



      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais iguais com hora extra sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |



      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes sem hora extra sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |







      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes com hora extra sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais sem hora extra sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeusCaro1
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais com hora extra sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd


      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |



      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes sem hora extra sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes com hora extra sem equipamentos com proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/sem_equipamento/com_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      #SEM-PROTECAO
      ############################################################################################################################
      @amadeusCaro
      Esquema do Cenario: realizar cotação de veiculo mais caro com 1 dia de reserva com filiais iguais sem hora extra, com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |



      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais iguais com hora extra com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |



      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes sem hora extra com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |







      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes com hora extra com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais sem hora extra com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeusCaro1
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais com hora extra com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd


      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |



      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes com hora extra com equipamentos sem proteçao com condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/sem_protecao/com_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |





      #SEM-CONDUTOR
      ##################################################################################################

      @amadeusCaro
      Esquema do Cenario: realizar cotação de veiculo mais caro com 1 dia de reserva com filiais iguais sem hora extra, com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |



      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais iguais com hora extra com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |





      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes sem hora extra com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |






      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 1 dia de reserva com filiais diferentes com hora extra com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "1_dia_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais sem hora extra com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd




      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais iguais com hora extra com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_iguais/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd


      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |



      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes sem hora extra com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/sem_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |




      @amadeusCaro
      Esquema do Cenario: realizar reserva de veiculo mais caro com 30 dias de reserva com filiais diferentes com hora extra com equipamentos com proteçao sem condutor adicional

      Quando seleciono informacoes de cotacao com "veiculo_mais_caro", "<agencia_parceiro>", "amadeus", "30_dias_de_reserva/filiais_diferentes/com_hora_extra/com_equipamento/com_protecao/sem_condutor_add"

      # E realizo 'rateRule' validando esquemas xsd

      Entao realizo 'cotacao' validando esquemas xsd



      Exemplos:

      | agencia_parceiro   |
      | latam              |
      | trend              |
      | flytour            |
      | europlus           |
      | congonhas_travel   |
      | artico_american    |
      | hostway            |
      | multicidades       |
      | maringa            |
      | promotional_travel |
      | augustus           |
      | invalido           |
