require 'selenium-webdriver'
require 'report_builder'
require 'capybara/cucumber'
require 'capybara'
require 'allure-cucumber'
require 'net/smtp'
require 'mail'
require 'pony'
require 'open-uri'


Before do |scn|

    @sinistro =  Sinistro_Notificacao.new
    @odonto = Odonto_Methds.new
    @login_site = Login_site.new
    @produtos = Produtos.new  
    @validate = Validate.new
    @helpers = Helpers.new
    @vars =  Vars.new
    @digital_card = Digital_card.new
    @methods = All_Methods.new
    @prev = Previdencia.new
    @saude = Saude.new
    @fale_conosco = Fale_Conosco.new
    @ouvidoria = Ouvidoria.new    
    page.current_window.resize_to(1920, 1080)
    @vars.tag_current = scn.source_tag_names.last
    @helpers.prepara_exec(@vars,scn.source_tag_names) unless scn.source_tag_names.last.include?('login_invalido') || scn.source_tag_names.last.include?('@acesso')
  end

After do |scn|
  puts $var + "\n" if $var !=  nil
  $var = ''
end


# After do |scn|
#   if scn.failed?
#     binding.pry
#   end
# end
#




After do |scn|
  at_exit do
      
    @helpers.trata_name_report('data/reports/*.html') if File.exist?("data/reports/resultado_cenarios_#{$number}.html") == false
 
    ReportBuilder.configure do |config|
      t = Time.now
      t.to_s  
      config.input_path = 'data/reports'
      config.report_path = "data/reports/resultado_cenarios_#{$number}"
      config.report_types = [:html]
      config.report_title = 'Resultados Produtos Portal PF'
      config.additional_info = {browser: 'Chrome', Data_execucao: t.strftime('%d/%m/%y'), Horario_Conclusao: t.strftime('%H:%M:%S'), Observação: "Gráfico a esquerda refere-se a quantidade de features executadas.\nGráfico a direita refere-se a quantidade de cenários executados."}
      config.color = 'blue'
      config.include_images = true    
    end    

    if File.exist?("data/reports/resultado_cenarios_#{$number}.html") == false
            
      ReportBuilder.build_report
      
      @helpers.send_mail(@vars) 
    end
  end    
end


AfterStep do |scenario|
  target = "data/imagens/testete.png"
  sleep 1
  page.save_screenshot(target)
  embed(target, 'image/png', 'Evidência')
end

