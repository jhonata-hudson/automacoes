
class Login_site

    include Capybara::DSL

    def loga_site(vars,*dados)
        
        if vars.nao_logado == nil and dados == []
     
            cont = 0 

            cont += 1 and p cont and p 'Aguarde a página ser carregada...' while page.has_no_title?('Login Cliente | Seguros Unimed')
            
            raise "Não foi possivel carregar a Página de Login" if  cont == 60

            within first('.d-flex.flex-column', wait: 20) do
                
                trace_login = EL[:login]
                
                find(trace_login['campo_login'], wait: 10).set(vars.login)
                
                find(trace_login['campo_senha']).set(vars.senha)
            end
            
            click_button('Entrar')     
            Avisos.new.detecta_aviso('.lds-ellipsis.mx-auto', 10) if dados == nil || dados == []

        elsif vars.nao_logado == nil and dados != []

            visit '/login-cliente' ##
            
            within first('.d-flex.flex-column', wait: 10) do
                
                trace_login = EL[:login]
                
                find(trace_login['campo_login'], wait: 10).set('0000000000000')
                
            end
            
            click_button('Entrar')     
            Avisos.new.detecta_aviso('.lds-ellipsis.mx-auto', 10) if dados == nil || dados == []

        elsif vars.nao_logado == true  and dados == [] 

            p 'Cenário nao exige cliente logado'
        
        end 
    end
end





