class Fale_Conosco

    include Capybara::DSL


    def faker_data
        
        data = Time.now - rand(200000000...3000000000)

        dados = {"name": "#{FFaker::NameBR.name} Teste",
    
        "cpf": FFaker::IdentificationBR.pretty_cpf,

        "tel": FFaker::PhoneNumberBR.mobile_phone_number,
        
        "email": FFaker::Internet.email,

        "txt": "É um prazer fazer parte dessa empresa !!",
        
        "numero_do_cartao": rand(0..100),
       
        "data_nascimento": "#{data.strftime("%d%m%Y")}",
        
        "nome_completo_segurado": "#{FFaker::NameBR.name} Teste",
        
        "corretor": '1',

        "crm": rand(100..10000),

        'cnpj': FFaker::IdentificationBR.pretty_cnpj,
        
        'cpf_cnpj': [FFaker::IdentificationBR.pretty_cnpj,FFaker::IdentificationBR.pretty_cpf].sample,

        'tipo_contrato': rand(1000..5000),
        
        'cep': ['69912-440','78557-749','82810-602','75143-372','49026-040','86066-180', '57043-255', '59090-657', '59135-530'].sample,
        
        'numero_casa': rand(0...2000)
        }

        return dados
    end

    def executa_metodos_fale_conosco(vars,param)

        begin

            find(EL[:Elementos]['eusou'], wait: 20).select(param)

        rescue Selenium::WebDriver::Error::StaleElementReferenceError            
            sleep 0.3           
            find(EL[:Elementos]['eusou'], wait: 20).select(param)
        end

        conoso = FALE_CONOSCO

        dados = faker_data
            
        usuario_mtd = Helpers.new.remove_acento(param)

        usuario_mtd = usuario_mtd.gsub('-','_') if usuario_mtd.include?('-')

        fale_conosco_inputs(dados,FALE_CONOSCO[:"Fale_conosco_inputs_#{usuario_mtd}"])
        
        fale_conosco_selects(dados,FALE_CONOSCO[:Fale_conosco_selects])

        fale_conosco_import_file(dados[:txt],all('button', text: 'Anexar')) if has_selector?('button', text: 'Anexar')
        
        vars.scn = param
    end        
    
    
    def fale_conosco_import_file(txt,trace)
        
        cont = 0 
        
        qtd = trace.count - 1 
        
        while cont <= qtd 
            
            all(EL[:Elementos]['input_file'], visible: false)[cont].set('data/reports/nf/foto.jpg')
            
            cont += 1
        end

        find('#mensagem-fale-conoscoInput').set(txt) if find('#mensagem-fale-conoscoInput').value == ""
    end


    def fale_conosco_inputs(dados,trace)

        trace.each do |infos| 
            
            if has_selector?(infos[1])            
                begin 

                    find(infos[1], wait: 10).set(dados[infos.first.to_sym])
                    
                rescue Selenium::WebDriver::Error::ElementNotInteractableError
                    find('.page').click

                    sleep 1 and p 'aguarde' while has_selector?("input[id=#{infos[1].gsub('#','')}][disabled]")

                    find(infos[1], wait: 10).set(dados[infos.first.to_sym])
                end
            end
        end
    end


    def captura_protocolo(vars)

        res = find(EL[:Elementos]['protocol_fale'], wait:  10).text
    
        raise "#{find('.erro-container-modal').text.gsub("\n"," ")}" if res == "Solicitação inválida."

        $var = $var.to_s + "\n\n" + "#{res}"

        File.open("data/protocolos/numeros_protocolo_#{vars.scn.downcase}_txt", 'wb') do |file| 
            
            file.write("#{res}\n")

            file.close
        end
    end

    
    def fale_conosco_selects(dados,trace)
        sleep 0.2

        trace.each do |infos| 
            
            if has_selector?(infos[1]) && infos[0] != 'txt'
            

                qtd = find(infos[1], wait: 10).all('option').count
                posi = rand(1...qtd)
                find(infos[1]).all('option')[posi].click                       
           
            # elsif has_selector?(infos[1]) && infos[1] == 'txt'
    
            #     find(infos[1]).set(dados[:txt])
            end        
        end
    end







    




        











        










end