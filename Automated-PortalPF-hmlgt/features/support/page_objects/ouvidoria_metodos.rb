class Ouvidoria 

    include Capybara::DSL



    def seleciona_processo(processo)
        
        Helpers.new.foco_browser
        
        Avisos.new.detecta_aviso('.spinner', 10)
        
        find(EL[:Elementos]['radio_processo'], exact_text: processo).click
    
    end

    def seleciona_numero_protocolo(vars)

        files = Dir.entries("data/protocolos").select {|f| !File.directory? f}
        
        protocolo = File.read("data/protocolos/#{files.sample}").split

        protocolo while (protocolo.first.gsub(/\p{Ascii}/, "") =~ /\p{Latin}/) == 0  

        number_protocolo = protocolo.first

        find(EL[:Elementos]['protocoloInput']).set(number_protocolo)

        click_button('Validar este protocolo')

        Avisos.new.detecta_aviso('.lds-ellipsis.mx-auto', 10)

        vars.botao_name = 'Concluir'
    end








end