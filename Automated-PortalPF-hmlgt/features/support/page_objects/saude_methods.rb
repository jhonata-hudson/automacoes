class Saude

    include Capybara::DSL


    def guia_medico(vars,*arg)
        
        trace =  EL[:Guia_medico]
        
        sleep 0.3

        find(trace['plano_saude'], wait: 10).click
        
        Avisos.new.detecta_aviso('.spinner',10) if has_selector?('.spinner')
          
        find(trace['estabelecimento'], wait: 10).find('option', exact_text: arg.first, wait: 10).click
        
        find('#localization', wait: 11).set('Alameda santos, 1978') if find('#localization', wait: 10).value == ""
  
        click_button('Especialidade', wait: 10)

        find(trace['search'], wait: 10).set(arg[1])

        find(EL[:Elementos]['options_modal'], exact_text: arg[1], wait: 10).click

        click_button('Selecionar')
                 
        seleciona_km(arg.last)

        click_button('Realizar busca')
        
        Avisos.new.detecta_aviso('.spinner',10) if has_selector?('.spinner')
    end


    def seleciona_km(km)
        
        trace =  EL[:Guia_medico]
        
        km_definir = find(trace['name_km']).find('strong').text.split('k')

        if km.to_i > km_definir.first.to_i
            symbol = :right
        elsif km.to_i < km_definir.first.to_i  
            symbol = :left
        end
    
        find(trace['index_km']).native.send_keys(symbol)  while find(trace['name_km']).find('strong').text != "#{km}km" 
    end

    def resultados_guia_medico
        all('.provider-card.card')
    end

    def seleciona_farmacia(farm)

        cont = 0 

        trace = all('.card-body.pb-0.px-3.pt-3', wait: 10)
        
        cont += 1 while trace[cont].all('h3', wait:10)[0].text != farm

        trace[cont].all('.link', wait: 10)[0].click
    end
    
    
    def valida_dados_farmacias_parceiras(parceiros)

        Helpers.new.trata_name_report_pdf('data/*.pdf')

        Helpers.new.foco_browser
        
        sleep 2

        url = page.current_url

        url = url.split('.')
        
        t = Time.now

        if url.last == 'pdf'

            File.open("data/PDF_results_#{$pdf}.pdf",'wb') do |file|
        
                file.write open(page.current_url).read
            end
        end       
    end


    def valida_dados_farmacias_parceiras_headless(parceiros)
        
        sleep 2
       
        Helpers.new.trata_name_report_pdf('data/*.pdf')

        t = Time.now

        result = page.driver.browser.window_handles
        
        raise "Pagina nao encontrada" if result.size < 2
        
        if parceiros == 'Farmácias Unimed'    
            
            url = "https://seguros-unimed-arquivos-publicos.s3-sa-east-1.amazonaws.com/anexos-portal/InformacoesDescontosMedicamentosFarmaciasUnimed.pdf"
            
        elsif parceiros == 'RaiaDrogasil'    
            
            url = 'https://seguros-unimed-arquivos-publicos.s3-sa-east-1.amazonaws.com/anexos-portal/InformacoesDescontosMedicamentosRaiaDrogasil.pdf'
            
        elsif parceiros == 'Drogarias Pacheco & Drogaria São Paulo'
            
            url = 'https://seguros-unimed-arquivos-publicos.s3-sa-east-1.amazonaws.com/anexos-portal/InformacoesDescontosMedicamentosDrogariasPachecoDrogariaSaoPaulo.pdf'
        end
        
        Capybara.app_host = url
        
        open_new_window
        
        Helpers.new.foco_browser

        visit '/'

        File.open("data/PDF_results_#{$pdf}.pdf",'wb') do |file|
        
            file.write open("#{url}").read
        end
        
        Capybara.app_host = $url_default
    end
end