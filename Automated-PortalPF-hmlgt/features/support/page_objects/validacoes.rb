class Validate
   
    include Capybara::DSL

    def valida_situacao_dados_titular(param)        
        caminho_produto = EL[:produtos]

        if param.include?('Emitida') ||  param.include?('Confirmada')

            raise "Status diferente de #{param}" if all(caminho_produto['produto_emitido'], text: 'Situação', wait: 20)[0].text.gsub("\n",' ') != param
        else

        
            raise "Status diferente de #{param}" if first(caminho_produto['class_cliente'], text: 'Situação', wait: 20).text.gsub("\n",' ') != param
        end
        end


    def valida_status_ativo_link(*status)
        
        qtd = status[0].count - 1
        
        cont = 0
        
        while cont <= qtd
        
          status_ativo = status[0][cont].text
        
          status_ativo = status_ativo.split(' ')

          raise "erro status ativo" if status_ativo.last != status.last

          cont += 1
          
        end
    end

    def valida_dados_carteirinha_odonto(vars)

        cont = 0

        vars.front_card[0].each do |infos|
            
            if infos.byteslice(0..4) == '00000'
                5.times do
                    infos.delete_prefix!('0')
                end
            end

            raise "informaçoes carteirinha invalido" if infos != vars.front_card[1][cont]

            p $var = $var.to_s + "\n\n" + "Informações Carteirinha #{infos} / Informações Titular #{vars.front_card[1][cont]}" 

            cont +=1 
        end
        valida_verso_carteirinha_odonto(vars)
    end

    
    def valida_verso_carteirinha_odonto(vars)

        trace = EL[:Elementos]

        click_button('Cartão digital')
        
        find(trace['verse_radio'], visible: false, wait: 10).click

        raise "Informaçoes do verso do cartao sao invalidas" if find(trace['text_capture']).text.gsub("\n", '') != trace['text_verse_odonto']
    end

    def valida_dados_carteirinha_saude(vars)
        
        vars.front_card.each do |infos|        
            infos.pop
        end
        
        vars.front_card[0][0] = vars.front_card[0][0].delete_prefix!('0')

        vars.front_card[0].each do |infos|

            posi = vars.front_card[1].find_index(infos)
    
            next if posi == nil
            
            raise 'Informacoes carteirinha saude invalido !!' if vars.front_card[1][posi] != infos
            
            $var = $var.to_s + "\n\n" + "Informacoes Carteirinha #{infos} / Informações Titular #{vars.front_card[1][posi]}" 
            
        end
        valida_verso_carteirinha_saude(vars)
    end
    
    
    def valida_verso_carteirinha_saude(vars)
        
        trace = EL[:Elementos]
        
        click_button('Cartão digital')
        
        find(trace['verse_radio'], visible: false).click
        
        
        trace_element = find(trace['dados_verse']).all(trace['text_infos_verse'])
        
        qtd = trace_element.count - 1      
        cont = 0
        
        while cont <= qtd
            
            text = trace_element[qtd].text
            
            text = text.delete_suffix('S') if text.include?('S')
            
            text_var = Helpers.new.remove_acento(vars.verse_card[cont])
            text = Helpers.new.remove_acento(text)
            
            text_var = text_var.gsub!("-", " ")            
            text = text.gsub!("-", " ")            
            
            raise "INFORMAÇAO INVALIDA - Resultado Esperado - #{text_var} / Resultado Obtido #{text}" if text_var != text

            cont += 1
            qtd -= 1
        end
    end

    
    def valida_hist_pagamento(vars)
    

        $var = $var.to_s + "\n\n" + "#{find(EL[:Elementos]['infos_results'], wait: 10).all('thead', wait: 10)[0].text}"
               
        if find(EL[:Elementos]['infos_results'], wait: 10).has_selector?('tbody') == false 
          
            $var = $var.to_s + "\n\n" + "Não existe resultados para esse range de datas" 
        else 
           
            $var = $var.to_s + "\n\n" + "#{find(EL[:Elementos]['infos_results'], wait: 10).all('tbody', wait: 10)[0].text}"
        end
    end
        
    def valida_tela_boleto

        Helpers.new.foco_browser
        
       assert_selector(EL[:Elementos]['tela_boleto'])
    end


    def valida_geracao_pdf(file)
                
        if has_text?('Não existe documento a ser gerado para os dados selecionados.')
             
            $var = $var.to_s + "\n\n" + 'Não existe documento a ser gerado para os dados selecionados.'
            
        else             
    
            find('#button-addon2').click
        
            Helpers.new.foco_browser
            
            sleep 1
            
            target = "data/#{Time.now.strftime('%M')}_#{file}.png"
            
            page.save_screenshot(target)           
        end
    end

    def valida_dados_rendimentos
        
        Avisos.new.detecta_aviso('.spinner',5)

        valida_geracao_pdf('rendimentos')
    end

    def valida_dados_comparticipacao
    
        Avisos.new.detecta_aviso('.spinner',10)
       
        valida_geracao_pdf('comparticipacao')
    end

    def valida_numero_protocolo(vars)
         
        if  vars.botao_name == 'Cenario finalizado'

            p $var = $var.to_s + "\n\n" + "Cenario validado" 
            
        else 
            if vars.botao_name == nil   
                
                click_button('Solicitar prévia')

                click_button('Enviar')
                
            else 
                click_button(vars.botao_name)
            end
            
            Avisos.new.detecta_aviso('.spinner',10) if has_selector?('.spinner')
            # 
            $var = $var.to_s + "\n\n" + "#{find(EL[:Elementos]['solic_enviada']).text}"

            click_button('OK')    
        end 
    end
   


    def valida_formulario_previa(vars)

        sleep 0.6
    
        

        raise "Paciente invalido" if first(EL[:Elementos]['infos_previa'], wait: 10).text != vars.name
        
        Helpers.new.converte_nome_datas(vars)

        raise "Data invalida" if vars.datas !=  all(EL[:Elementos]['infos_previa'], wait: 10)[1].text
    end

    
    def valida_previdencia(text)
        
        trace =  all(EL[:Previdencia]['dados_prev'])
        
        if text == 'Composição da Reserva do Previdência'
            
            raise "Dados da #{text}" if has_selector?('.row.mb-4.pb-2') == false 
            
            dados = trace[0].find('.card-footer').text
            
            dados = dados.split("\n")
    
            dados.each do |infos| 
    
                $var = $var.to_s + "\n\n" + infos 
            end
        elsif text == 'Rentabilidade de Fundos do Previdência'
            
            raise "Dados da #{text}" if has_selector?('.row.mb-4.pb-2') == false 
                        
            all('.card-body.d-flex.header', wait: 10).last.all('button', wait: 10).last.click           
      
        elsif text == 'Emissão de extrato'
                  
            valida_geracao_pdf('emissao_extrato')

        elsif text == '2ª via de boleto'

            Avisos.new.detecta_aviso('.spinner',10)
          
            Helpers.new.foco_browser
        end
        sleep  1.5
    end

    def valida_retorno_guia_medico
        cards = all('.provider-card.card')
        cards[1].click
    end


    def valida_solicitacao_ouvidoria

        $var = $var.to_s + "\n\n" + "#{find(EL[:Elementos]['descricao_protocol'], wait: 10).text}" 
    end 
        
    def valida_pdf(file) 
                    
        target = "data/reports/imagens/#{Time.now.strftime('%M')}_#{file}.png"
        
        page.save_screenshot(target)           
    end
        


    
        
        
        
        
        

        












end