class Digital_card

    include Capybara::DSL

    def executa_methodos_carteirinha_front(vars,produto)
    
        infos_frente_carteirinha(vars,produto)
        
        send("infos_verso_carteirinha_saude",vars) if produto == 'saude'
    end
    
    def infos_frente_carteirinha(vars,produto)

        dados_titular = first('.section-content-wrapper', wait: 10).text.split("\n")
        
        click_button(all("button[class= 'head-button d-flex align-items-center text-left rounded border py-1 mx-auto mb-3']").last.text, wait: 10)
        
        @numbers = [numbers_titular= Array.new,numbers_card = Array.new]
        
        cont = 0 

        Avisos.new.detecta_aviso('.spinner', 10)

        dados = [dados_card = find(EL[:Carteirinha]["dados_card"], wait: 10).text.split("\n") ,dados_titular]
       
        while cont <= dados.count - 1
            
            dados[cont].each do |infos| 
                
                if infos.match(/[0-9]/).class == MatchData #numberss

                    infos = Helpers.new.remove_acento(infos)

                    infos = infos.gsub!('-','') unless infos.include?('-') == false
                    
                    infos = infos.gsub!(/[^\d\.]/,'') unless (/[^\d\.]/ === infos) == false
                   
                    @numbers[cont] << infos unless infos.size <= 3           
                end
            end
            cont += 1
        end  
        vars.front_card = @numbers

        click_button('OK')
    end
   


    def infos_verso_carteirinha_saude(vars)

        Avisos.new.detecta_aviso('.spinner', 10)

        dadoscli = all(EL[:Dados_cliente]['principal'], wait: 10)[1].text.split("\n")

        cont = 0

        vars.verse_card = Array.new

        infos_card = 0

        qtd = EL[:Dados_verso_cartao].count - 1
        
        while cont <= qtd
        
            posi = dadoscli.find_index(EL[:Dados_verso_cartao][infos_card])

            vars.verse_card  <<  dadoscli[posi += 1].upcase          

            infos_card += 1

            cont += 1
        end
    end
end