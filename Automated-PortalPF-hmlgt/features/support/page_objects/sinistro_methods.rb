class Sinistro_Notificacao

    include Capybara::DSL


    def seleciona_opcoes_cadastro_aviso(opc)

        within_frame first('.w-100') do
    
            find('a', exact_text: opc).click            
        end
    end        

    def mass_segurado
        
        nascimento = Time.now - rand(200000000...3000000000)
       
        data = Time.now + 86400
        
        segurado = {'#txt_CPF':FFaker::IdentificationBR.pretty_cpf,
            '#txtCEP': ['69912-440','78557-749','82810-602','75143-372','49026-040','86066-180', '57043-255', '59090-657', '59135-530'].sample, 
            '#txtEmail': FFaker::Internet.email,
            '#txtNomeSegurado': "#{FFaker::NameBR.name} Teste",
            '#txtDtNascimento': "#{nascimento.strftime("%d/%m/%Y")}" ,
            '#txtRG': FFaker::IdentificationBR.pretty_rg, 
            '#txtTelefone': FFaker::PhoneNumberBR.phone_number, 
            '#txtCelular': FFaker::PhoneNumberBR.mobile_phone_number, 
            '#txtNumero': rand(0...2000),
            '#txtComplemento': ['casa', "ap #{rand(1..50)} bl #{rand(10..1000)}"].sample, 
            '#txtDataEvento': "#{data.strftime("%d/%m/%Y")}", 
            '#txtCausaSinistro': 'Teste automatizado',
            '#txtDescricaoOcorrencia': 'Teste Teste',
            '#txtTpLogradouro': rand(10...1000),
            '#txtDtOcorrencia': "#{data.strftime("%d/%m/%Y")}"
        }

        segurado
    end


    def cadastro_de_aviso(opc)
        
        massa_dados = send("mass_#{opc.downcase}")

        within_frame first('.w-100') do

            find("#uniform-#{opc.downcase}").click

            massa_dados.each  do |ids|                       
                
                if has_selector?(ids.first.to_s)
                    
                    find(ids.first.to_s).set(ids.last)
                else
                    next
                end
            end        
            preenche_campos_vazios(massa_dados)
            
            select_produto_sinistro

            find('#btnSalvar').click
        end    
    end

    
    def select_produto_sinistro

        options = find('#uniform-ddl_Produtos', wait: 10).all('option', wait: 10).count

        find('#uniform-ddl_Produtos', wait: 10).all('option', wait: 10)[rand(1...options)].click

        sleep 2.5
        
        nome_cobertura = all('.input-group.group-checked', wait: 10).count 
        
        all('.input-group.group-checked', wait: 10)[rand(2...nome_cobertura)].find('.checker', wait: 10).click

        find('#rblEnvioSMS_1').click

    end

    def preenche_campos_vazios(massa_dados)
                    
        sleep 1  and p 'aguarde campo endereco ser preenchido' while find('#txtEndereco').value == ""
    
        massa_dados.each do |ids|

            if has_selector?(ids.first.to_s)

                if find(ids.first.to_s).value == "" 
             
                    find(ids.first.to_s).set(ids.last)
                else 
                    next
                end
            else
                next
            end
        end
    end  

    def valida_geracao_sinistro

        number = find("#mensagemOperacao").find('h1').text

        File.open('data/sinistro_numbers/numbers.txt', 'wb') do |file| 

            file.write(number) 

            file.close
        end    

        $var = $var.to_s + "\n\n" + number
    end


    def cadastro_de_aviso_re(status)
        
        dados = mass_segurado

        within_frame first('.w-100') do

            dados.each do |ids|
                
                if has_selector?(ids.first.to_s)

                    find(ids.first.to_s).set(ids.last)
                else
                    next
                end
            end

            preenche_campos_vazios(dados)

            seleciona_apolice(status)

            find('#rblEnvioSMS_1').click

            find("input[type='file']", visible: false).set('data/imagens/testete.png')
            
            find('#btnIncluir').click
            
            valores = captura_valores_campos(dados)
        end
    end

    def seleciona_apolice(status)

        find('#btnPesquisaApolice', wait: 10).click
                
        cont = 1
                
        sleep 2

        cont += 1 while find('#linhaApolice', wait: 20).all('tr', wait: 20)[cont].all('td', wait: 20).last.text != status
        
        find('#linhaApolice').all('tr')[cont].first('td').click
        
        qtd = all('.col-12.form-group', wait: 10).count - 1
        
        qtd_times = rand(0..qtd)
                
        time = *(0..qtd_times)
        
        time.each do |posi|

            all('.col-12.form-group')[posi].find('.checker').click

        end

        find('#txtEstimativaPrejuizo', wait: 10).set(rand(10..100000).to_s)
    end

    def captura_valores_campos(dados)

        valores = *()

        dados.each do |ids|
                
            if has_selector?(ids.first.to_s)
            
                valores << find(ids.first.to_s).value

            else
                next
            end
        end
        valores
    end



    def valida_aviso_digital

        within_frame first('.w-100') do
            
            find('#btnSalvar').click

            sleep 1
            begin 
           
                alert = page.driver.browser.switch_to.alert.text
                
                alert = I18n.transliterate(alert)

                page.driver.browser.switch_to.alert.accept
                
                if alert.is_a?(NilClass)
    
                    raise "Não foi possivel visualizar protocolo"
                
                elsif alert.class == String
    
                   raise $var = $var.to_s + "\n\n" + "#{alert}"
                end
           
                $var = $var.to_s + "\n\n" + "#{alert}"

            rescue Selenium::WebDriver::Error::NoSuchAlertError
            
            end
        end
    end










end