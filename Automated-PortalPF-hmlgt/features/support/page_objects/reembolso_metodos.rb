class Reembolso 

    include Capybara::DSL

    def paciente(vars,param,arg)

        p 'okok'

        click_button(param[0], wait: 20) unless has_selector?("button[disabled]", wait: 2) 
        
        trace_capy = all(param[1])   
        posi = trace_capy.count - 1
        arg = arg.split('_')
        
        if arg.last.include?('Aleatorio')

            trace_capy[rand(0..posi)].click
            
        elsif arg.last.include?('Especifico') || arg[1].include?('Especifico')
            cont = 0 
                       
            if arg.first == 'Maior' || arg.first == 'Menor' || arg.first == 'Previa' 

                cont += 1 while all(param[1], wait: 20)[cont].text.include?(param.last) != true
                
                trace_capy[cont].click

                click_button(param[2])
            end                
        end
    end
        

    def tipo_do_procedimento(vars,param,arg)

        trace_capy = first(param[0], wait: 10)
        
        arg = arg.split('_')

        if arg.last.include?('Aleatorio')
            
            options = trace_capy.all(param[1]).count

            posi = rand(1..options)

            trace_capy.all(param[1], wait: 10)[posi].click

        elsif arg.last.include?('Especifico') || arg[1].include?('Especifico')

            trace_capy.select(param[2])

        end
           
        especialidade(vars,param,arg) if trace_capy.value.include?('consulta') || trace_capy.value.include?('avaliaçõesTerapias') ||  trace_capy.value.include?('3: 36')
    end            

    
    def especialidade(vars,param,arg)

        

        click_button('Especialidade')
        
        trace_capy = first(EL[:Elementos]['modal'],visible: false).all(EL[:Elementos]['options_modal'])
                
        if arg.last.include?('Aleatorio')

            posi = trace_capy.count
            
            trace_capy[rand(1...posi)].click
            
        elsif arg.last.include?('Especifico') || arg[1].include?('Especifico')
        
            find(param[3]['especialidade'][0]).set(param[3]['especialidade'][1])

            

            find(param[3]['especialidade'].last).click
        end
       

        click_button('Selecionar')
    end
    

    def valor_do_procedimento(vars,param,arg)

        trace = first(param[0], wait: 10)

        arg = arg.split('_')

        if arg.last.include?('Aleatorio')
            
            trace.set(rand(10000..100000).to_s)
        
        elsif arg.last.include?('Especifico') || arg[1].include?('Especifico')
        
            trace.set(param.last)
        end
    end
        
    def data_do_procedimento(vars,param,arg)

        trace = find(param[0], wait: 10)

        arg = arg.split('_')

        if arg.last.include?('Aleatorio')            
            
            data_procedimento = Time.now - rand(1000..900000)
          
            data_procedimento = data_procedimento.strftime("%d%m%Y")
          
            trace.set(data_procedimento) 

        elsif arg.last.include?('Especifico') || arg[1].include?('Especifico')

            trace.set(param.last)
        end
    end
        
    def nome_favorecido(vars,arg,param)

        dados = Helpers.new.dados_PJ(vars)
        dados = [vars.name,vars.cpf]
        cont = 0
        arg = arg.split('_')

        if arg.last.include?('Aleatorio')

            param.each do |infos| 
    
                find(infos).set(dados[cont])
                cont += 1
            end
        
        elsif arg.last.include?('Especifico') || arg[1].include?('Especifico')
            
            while cont <= 1 
                
                find(param[cont]).set(param[2]['input_infos'][cont])

                cont += 1 
            end 
        end
    end

    def tipo_pay(vars,arg,param)

        arg = arg.split('_')

        if arg.last.include?('Aleatorio')

            cont = 0
            qtd = find(param[0]).all(param[1]).count
            posi = rand(1...qtd)
            find(param[0]).all(param[1])[posi].click
            name_index = param[2]['el_dados_bancario'].keys
            index = 2

        elsif arg.last.include?('Especifico') || arg[1].include?('Especifico')
            
            find(param[0]).select(param[2])
            name_index = param[3]['el_dados_bancario'].keys
            index = 3
        end
        if find(param[0]).value == '1: 1' || find(param[0]).value == '2: 8' 
            
            name_index.each do |infos|
                send("#{infos.downcase}",vars,arg,param[index]['el_dados_bancario'][infos])          
            end            
        end  

        click_button('Enviar solicitação')
    end
        
    def banco(vars,arg,param)
        
        if arg.last.include?('Aleatorio')

            trace_capy = all(param[1])
            qtd = trace_capy.count 
            posi = rand(0..qtd)

            if posi  == 1
                
                find('#OutrosRadio', visible: false).click 
            else
                trace_capy[posi].click
            end            
        elsif arg.last.include?('Especifico') || arg[1].include?('Especifico')
           
            click_button(param.first)
            
            find(param[1]).set(param.last)            

            find(param[2]).click
        end
        click_button('Selecionar')        
    end


    def infos_conta(vars,arg,param) 


        cont = 0 
        
        if arg.last.include?('Aleatorio')
                    
            dados = [rand(1111...9999).to_s,rand(11111...9999999).to_s,rand(1..9).to_s]
            
            param.each do |infos| 
                
                find(infos).set(dados[cont])
                cont += 1
            end        

        elsif arg.last.include?('Especifico') || arg[1].include?('Especifico')

            while cont <= 2

                find(param[cont]).set(param[3]['input_conta'][cont])

                cont += 1
            end
        end
    end
        
    def input_prestador_servico(vars,arg,param)

        arg = arg.split('_')

        name_index = vars.trace[param].keys
        cont = 0        
        if arg.last.include?('Aleatorio')            
            
            dados = Helpers.new.dados_PJ(vars)
            while cont <= 50    
                
                find(vars.trace[param][name_index[cont]][0]).set(dados.first)
                cont += 1 
                dados.delete_at(0)
                
                break if dados.empty? 
            end       
        elsif arg.last.include?('Especifico') || arg[1].include?('Especifico')
         
            name_index.each do |infos| 

                find(vars.trace[param][name_index[cont]].first).set(vars.trace[param][name_index[cont]].last)  

                cont += 1
                
                break if infos == 'cep'
            end
        end
    end

    def select_infos_conselho(vars,arg,param)

        arg = arg.split('_')

        if all(vars.trace[param]['Conselho'][2]).last.value == '0: null'
            
            trace_element = all(vars.trace[param]['Conselho'][2]).last
             
            if arg.last.include?('Especifico') || arg[1].include?('Especifico')

                trace_element.find(vars.trace[param]['Conselho'][1], exact_text: vars.trace[param]['Conselho'].last).click
                
            elsif arg.last.include?('Aleatorio')
 
                qtd = trace_element.all(vars.trace[param]['Conselho'][1]).count
                posi = rand(1...qtd)
                trace_element.all(vars.trace[param]['Conselho'][1])[posi].click
            end
        end
    end
            
    def valida_idade_reembolso(idade)

        text = select_text_validate
                
        begin
            if idade == 'Maior de idade'

                cont = 0 

                cont += 1 while all('.card.mt-lg-4.mt-3', wait: 20)[cont].text.include?('Elis Martins Lopes') == false 

                all('.card.mt-lg-4.mt-3', wait: 20)[cont].click 

                raise "Nao foi possivel visualizar solicitacao de reembolso paciente maior de idade !" if has_selector?('.card.no-container-sm', wait: 20) == false 

            elsif idade == 'Menor de idade'
            
                cont = 0 

                cont += 1 while all('.card.mt-lg-4.mt-3', wait: 20)[cont].text.include?('Kely Bianca Martins Lopes') == false 

                all('.card.mt-lg-4.mt-3', wait: 20)[cont].click 

                raise "Nao foi possivel visualizar mensagem de erro quando solicito detalhes de reembolso para menor de idade!" if find('.modal-content', wait: 20).text != text
            end
        rescue 
            
            Avisos.new.detecta_aviso('.spinner', 10) 
        end
    end

    def select_text_validate

        text =  "Atenção\nDetalhamentos de reembolso de pacientes maiores de 18 anos só podem ser visualizados pelo paciente.\nOK"  if find('.title.section-title', wait: 20).text == 'Solicitações'

        text = "Atenção\nDetalhamentos de prévia de pacientes maiores de 18 anos só podem ser visualizados pelo paciente.\nOK" if find('.title.section-title', wait: 20).text == 'Prévias'

        text
    end




















end
     
    

    
    