class Cad_cliente 

include Capybara::DSL

    def read_file_csv(vars,csv)        
        sleep 1 

        data = CSV.read(csv)

        data 
    end

    def seleciona_cliente(vars, data)

        qtd_data =  data.count 

        vars.cliente = data[rand(1...qtd_data)].first.split("\t")
        
        vars.cliente

    end


    def write_cli_txt(vars,path)
        
        File.open(path, 'a') do |infos|
        
            infos.puts vars.cliente.last
            
            infos.close
        end
    end


    def detecta_cliente_cadastrado(vars,path)
        
        file = IO.readlines(path) 
        
        seleciona_cliente(vars, "data/dados_clientes/dados_csv.csv") while file.find_index(vars.cliente.last) != nil
    end
        
    def massa_dados_cadastro(vars)

        email = 'see.klein@mailnet.top'

        dados_primeiro_cad = {"seu cpf": vars.cliente.first, 
         "seu nome": vars.cliente.last,
         "sua data de nascimento": vars.cliente[1], 
         "seu celular": "119#{rand(11111111..99999999).to_s}",
         "seu e-mail": email,
         "confirme seu e-mail": email        
        }
        dados_primeiro_cad
    end


    def exec_cad_cliente(vars)
        
        data = read_file_csv(vars,"data/dados_clientes/dados_csv.csv")
      
        seleciona_cliente(vars,data)

        detecta_cliente_cadastrado(vars,'data/dados_clientes/clientes_cadastrados.txt')

        write_cli_txt(vars,'data/dados_clientes/clientes_cadastrados.txt')

        cont = 0 
        trace = all(EL[:Elementos]['class_all'])

        qtd = trace.count - 1
        
        trata_error_cpf(vars) if vars.cliente.first.size == 9 ||  vars.cliente.first.size == 10
        
        dados_clientes = massa_dados_cadastro(vars)

        EL[:Primeiro_acesso].each do |ids| 

            find(ids.last).set(dados_clientes[ids.first.to_sym])

        end

        find('.link').click
    end
 
  
    def trata_error_cpf(vars)
        vars.cliente.first.gsub!("#{vars.cliente.first}","#{vars.cliente.first}0") if vars.cliente.first.size == 10
        vars.cliente.first.gsub!("#{vars.cliente.first}","#{vars.cliente.first}00") if vars.cliente.first.size == 9    
    end

    def gera_senha
        
        last_handle = page.driver.browser.window_handles.last
        page.driver.browser.close
           
        Helpers.new.foco_browser
        
        find("label[class='mb-0']").click

        click_button('Próximo')
        
        Avisos.new.detecta_aviso('.my-2',10) 

        all(EL[:Elementos]['class_all'], wait: 10)[0].find('input').set('Teste123*')
        
        all(EL[:Elementos]['class_all'], wait: 10)[1].find('input').set('Teste123*')

        click_button('Próximo')

        Avisos.new.detecta_aviso('.my-2',10) 

    end
 







end