class Produtos 

    include Capybara::DSL

    def seleciona_produtos(produtos)
        
        produto = produtos.split(' ')
        caminho_produto = EL[:produtos]
        
        if produto.first == 'botao'
            
            trace = all('button', wait: 10,exact_text:produto.last)    
            
            trace[rand(0...trace.count)].click if trace.count > 1     
                     
            Avisos.new.detecta_aviso('.spinner',10) 
            
            click_button("#{produto[1]}", wait: 10) if produto[2] == nil
            
            click_button("#{produto[1]} #{produto[2]}", visible: false , wait: 10) if produto[2] != nil
            
        elsif produto.first == 'produto'
            
            Helpers.new.produtos(caminho_produto,produto[1])
        end
    end
    
    def seleciona_plano(status)

        Avisos.new.detecta_aviso('.spinner',10) if has_selector?('.spinner')

        within('tbody') do 

            position = all('td', text: status).count

            raise "Nao foram encontrados produtos com situacao #{status}" if position == 0

            all('tr')[rand(0...position)].find('a').click
        end
    end
end


 






