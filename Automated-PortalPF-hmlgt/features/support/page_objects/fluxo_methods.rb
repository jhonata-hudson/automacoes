class All_Methods

    include Capybara::DSL

    def select_drop(selects,text_elemento)
    
        trace = EL[:Menu_option]
       
        trace_all = all(trace['selects'], wait: 10)
                     
        find(trace['selects'], text: selects).click
       
        trace_id = find('#dropdown-financial', wait: 10)

        if text_elemento == '2ª via de boleto' ||  text_elemento == 'Informes de rendimentos' || text_elemento == 'Utilização e coparticipação' || text_elemento == 'Guia médico'  

            trace_id.click_button(text_elemento)
        else 
            
            text_elemento = "Descontos em Medicamentos" if text_elemento == "Descontos em medicamentos" and ENV['preprod'] == 'true'
                
            trace_id.find('a', exact_text: text_elemento, wait: 10).click  
    
        end
    end


    def historico_de_pagamentos(vars,*drops)
    
        @cont = 0 
        
        EL[:Historico_pagamento].each do |infos| 

            if infos[0] == 'dt_ida' || infos[0] == 'dt_final'
                
                find(infos[1]).set(drops[@cont])
                
            elsif infos[0] == 'produto' 

                sleep 1

                begin                
                    if drops.first == ""  
    
                        find(infos.last, wait: 20).first('option', wait: 10).click 
                    else 
                        find(infos.last, wait: 20).find('option', text: drops.first, wait: 10).click  
                    end
                rescue Capybara::ElementNotFound 

                    raise "Elemento nao foi carregado a tempo"
                end
            else                            
                
                find(infos[1]).select(drops[@cont])
            end
            
            @cont += 1
        end
                    
        click_button('Atualizar busca', wait: 10)

        Avisos.new.detecta_aviso('.spinner',5) unless vars.tag_current.include?('ngtv')
    end

    def trata_name_method(metodo)
        
        metodo = metodo.delete!(metodo.byteslice(0..1))

        return metodo
    end

    def viadeboleto(produto)
   
        if has_selector?('.modal-content', wait: 10) == false 
            
            Avisos.new.detecta_aviso('.spinner',5)
        else
            # Avisos.new.detecta_aviso('.spinner',5)
           
            find(EL[:Boletos]['produto'], wait: 20).select(produto)
            click_button('Confirmar')
        end
    end   
    
    def informes_de_rendimentos(vars,*dados)
    
        @cont = 0
        
        EL[:Rendimentos].each do |infos| 
        
            if has_selector?("select[disabled][id=#{infos.last.gsub('#','')}]") 
                
                p 'Campo desabilitado'
            
            else  
                
                find(infos[1]).select(dados[@cont])                 
                @cont += 1
            end
        end
    
        click_button('Gerar informe', wait: 25)
    end



    def utilizacao_e_coparticipacao(vars,*dados)
        
        trace_find = EL[:Coparticipacoes]

        cont = 0 
        
        if dados[1] == 'Por período'
            
            datas = [dados[2],dados[3]]
        else
            datas = [dados[4],dados.last,dados[1]]
            
        end
     
        # sleep 1.3
        
        if has_selector?(trace_find['produto']) 
            
            find(trace_find['produto'], wait: 10).find('option', exact_text: dados.first, wait: 10).click
        end
    
        find(trace_find['radio'], exact_text: dados[1]).click                
        
        send("#{dados[1].urlize.gsub!("-","_")}",datas)         
      
        Avisos.new.detecta_aviso('.spinner',5) unless vars.tag_current.include?('ngtv')
    end
    
    
    def por_semestre(arg)
        
        trace =EL[:Coparticipacoes]
        
        find(trace['semestre'][0]).set(arg[0])
        
        find(trace['semestre'][1]).select(arg[1])
        
        click_button('Gerar relatório')    
    end

    def por_periodo(arg)

        cont = 0 

        EL[:Coparticipacoes]['periodo'].each do |infos|

            find(infos).set(arg[cont])

            cont += 1
        end
        click_button('Gerar relatório')
    end
 

    def reembolsos_e_previas(vars,*arg)
               
        cont = 0
        cont_arg = 0
        cont_while = 0

        while cont_while <= arg.count - 1
                       
            vars.trace = MASS["#{arg[cont_arg]}".to_sym]
            
            name_index = vars.trace.keys

            send("#{name_index[cont].downcase}",vars,arg[cont_arg],name_index[cont])

            cont += 1
            cont_arg += 1    
            cont_while += 1  
        end
    end


    def dados_do_procedimento(vars,arg,param)
        
        reembolso = Reembolso.new
        
        vars.trace[param].keys.each do |infos| 
            reembolso.send("#{infos.downcase}",vars,vars.trace[param][infos],arg)
        end
    
    end


    def documentos_necessarios(vars,arg,param)
                    
        arg = arg.split('_')
        
        if arg.last.include?('Aleatorio')
            
            find(vars.trace[param]['documentos'][0]).click
            
            find(vars.trace[param]['documentos'][1]).set('NF TESTE')
            
        elsif arg.last.include?('Especifico')  || arg[1].include?('Especifico')
            
            Helpers.new.importa_arquivo(EL[:Elementos]['input_file'],vars.trace[param]['documentos'].last)
        end
        
        if arg.first == 'Previa' 
    
            all('.col-6.px-2', wait: 20).last.click

            vars.botao_name = 'Enviar'
        end
    end



            

    def prestador_de_servico(vars,arg,param)

        reembolso = Reembolso.new
        
        reembolso.input_prestador_servico(vars,arg,param)
        
        reembolso.select_infos_conselho(vars,arg,param)
    end
        
        
    def dados_bancarios(vars,arg,param)

        status = ''

        name_index = vars.trace[param].keys
        
        name_index.each do |infos| 

            if infos == 'Nome_favorecido' && status == '1: Outros' || infos == 'Tipo_pay'                 

                Reembolso.new.send("#{infos.downcase}",vars,arg,vars.trace[param][infos])

            elsif infos == 'Favorecido'

                2.times do 
                    click_button(vars.trace[param][infos].first)
                end
                
                if arg == 'Aleatorio'
                                        
                    qtd = all(vars.trace[param][infos][1]).count
                    
                    posi = rand(0...qtd)
                    
                    all(vars.trace[param][infos][1])[posi].click
                    
                    click_button(vars.trace[param][infos].last)
                                        
                    click_button('Selecionar')

                    status = find('#beneficiaryInput').value

                elsif arg.include?('Especifico')

                    cont = 0 
                    
                    cont += 1 while all(vars.trace[param][infos][1])[cont].text.include?(vars.trace[param][infos].last) != true 
                    
                    all(vars.trace[param][infos][1])[cont].click
                   
                    click_button('Selecionar')
                end
            end
            status = find('#beneficiaryInput').value
        end
        vars.botao_name = 'Enviar'
    end

   
    def formulario(vars,paciente,data)
        
        # Avisos.new.detecta_aviso('.spinner', 10) if has_selector?('.spinner')

        cont = 0 
                    
        if find('#patientNameInput', wait: 20).text != paciente       
            
            click_button('Paciente')
            
            cont += 1 while all(EL[:Elementos]['paciente_previa'])[cont].text.include?(paciente) == false 
            
            all(EL[:Elementos]['radio'])[cont].click
            
            click_button('Selecionar')
            
        end

        find(EL[:Elementos]['input_consulta'], wait: 10).set(data)
        
        click_button('Calcular prévia')
        
        click_button('Enviar', visible: false, wait: 10)
        
        Avisos.new.detecta_aviso('.spinner', 10)
        
        vars.name = paciente 
        vars.datas = data

    end
        
    def seleciona_proposta(vars,proposta)   

        trace = all(EL[:Propostas]['proposta'], wait: 10)

        cont = 0 
                        
        # proposta = 'Tajumar Custodio Martins' if ENV["hmlg_#{vars.name_tag}"] == nil

        cont += 1 while trace[cont].text.include?(proposta) == false     
        
        trace[cont].find('a', text:'Ver detalhes').click       
    end

     












end