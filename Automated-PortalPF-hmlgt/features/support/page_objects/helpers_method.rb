# encoding: utf-8

class Helpers 

    include Capybara::DSL

    def seleciona_link_produto(*dados)     
     
        Validate.new.valida_status_ativo_link('Ativo')  
    
    end


    def trata_name_report(trace)

        if Dir[trace] == []

            $number = '01'
        else
            report = Dir[trace]
            report = report.last.split('_')
            report_first = report.last.split('.')
            $number = report_first.first.to_i + 1
         end 
     end

    def trata_name_report_pdf(trace)

        if Dir[trace] == []

            $pdf = '1'

        else
            report = Dir[trace]
            report = report.last.split('_')
            report_first = report.last.split('.')
            $pdf = report_first.first.to_i + 1
         end
    end
    
    
    def separa_tag(vars,tag)
                
        qtd_bytes = tag.first.size - 1

        last_byte = tag.first.byteslice(qtd_bytes)

        tag.first.delete_prefix!(last_byte) if last_byte.match(/[0-9]/).class == MatchData
        
        vars.name_tag = tag.first.byteslice(1..qtd_bytes)
    end


    def define_mensagem(vars)
            
        hora = Time.now
        hora = hora.strftime("%H")
        vars.saudacao = "Bom dia ! Segue anexo resultados da execução CircleCI." if hora <= '12' || hora >= '01'
        vars.saudacao = "Boa tarde ! Segue anexo resultados da execução CircleCI." if hora <= '18' || hora >= '13'
        vars.saudacao = "Boa noite ! Segue anexo resultados da execução CircleCI." if hora <= '00' || hora >= '19'
    end


    def send_mail(vars)
        
        define_mensagem(vars)
    
        gmail = Gmail.connect('jhonata.hudson.oliv@gmail.com','oifjesbxnhpynzko')
        
        gmail.deliver do
            to 'jhonata.oliveira.prime@segurosunimed.com.br,daniel.mizobuchi.prime@segurosunimed.com.br,daniel.lopes@segurosunimed.com.br'
                        
            subject "Resultado execução testes automatizados Circle CI"
            
            text_part do
                content_type 'text/html; charset=UTF-8'

                body "#{vars.saudacao}"
            end
            
            html_part do
                content_type 'text/html; charset=UTF-8'

                body "#{vars.saudacao}"
            end

            Dir.glob("data/reports/resultado_cenarios_#{$number}.html") do |file|
                add_file file
            end

            if vars.tag_current == "@saude16" ||  vars.tag_current == "@odonto9"

                Dir.glob("data/*.pdf") do |file|
                    add_file file
                end
            end            
        end        

        Dir.glob('data/*.pdf').each {|file| File.delete(file)}

        p 'Report enviado com sucesso !!'
    end

    def prepara_exec(vars,arg)
    
        separa_tag(vars,arg)

        if arg.last.include?('nao_logado') && ENV['prod'] == 'true'
            
            visit '/'
            
            vars.nao_logado = true  
            
            vars.login = '029.159.718-11'

        elsif arg.last.include?('nao_logado') == false && ENV['prod'] == 'true'
            
            visit "/login-cliente"  

            vars.login = '029.159.718-11'

            vars.senha = 'Taju@257'

        elsif arg.last.include?('nao_logado') && ENV['prod'] == 'true'

            visit "/"  

            vars.login = '029.159.718-11'

            vars.senha = 'Taju@257'

        elsif arg.last.include?('nao_logado') && ENV['prod'] == nil
            
            visit '/' ##

            vars.nao_logado = true  

            vars.login = DOCS[:cpfs]["#{vars.name_tag}"]    
            
        elsif  arg.last.include?('ngtv') &&  ENV['prod'] == nil
                        
            visit "/login-cliente" 

            vars.login = '31579750800' ####
            

            vars.senha = 'Teste123@'
        
        elsif  arg.last.include?('ngtv') && ENV['prod'] == 'true'

            visit "/login-cliente"  

            vars.login = '029.159.718-11'
            
            vars.senha = 'Taju@257'

        elsif arg.last.include?('nao_logado') == false  && ENV['prod'] == nil

            visit "/login-cliente" 

            vars.login = DOCS[:cpfs]["#{vars.name_tag}"]

        elsif arg.last.include?('nao_logado') && ENV['prod'] == nil
            
            visit '/'
            
            vars.login = DOCS[:cpfs]["#{vars.name_tag}"]            
        end
        vars.senha = DOCS["senha"] unless arg.last.include?('ngtv') || ENV['prod'] == 'true'

        Login_site.new.loga_site(vars)  
    end


    

    def produtos(*arg)
        
        cont = 0 
        qtd_produtos =  all(arg[0]['produtos_select']).count - 1

        cont += 1 while all(arg[0]['produtos_select'], wait: 20)[cont].all('div', wait: 20)[0].text.include?(arg[1]) == false

        begin
            all(arg[0]['produtos_select'])[cont].find('button').click
        
        rescue Capybara::Ambiguous
        
            all(arg[0]['produtos_select'])[cont].all('button')[1].click
        end
    end


    def trata_dados_cartao(vars,dados)

        vars.datas = []
        dados.each do |infos| 

            if infos.match(/[0-9]/).class == MatchData #number 

                infos.gsub!(/[^a-zA-Z0-9 ]/,"")

                infos.gsub!(/[^\d\.]/, '')

                vars.number_card = infos if infos.size == 17

                vars.datas << infos if infos.size == 8

                vars.cns = infos if infos.size == 15

            elsif infos[0].match(/[0-9]/).class == NilClass  #letters

                infos.gsub!(/[^a-zA-Z0-9 ]/,"")

                if infos == vars.front_card[1]
                    vars.name = infos
                else 
                    infos.clear
                end
            end
        end
    end

    def remove_acento(string)

        I18n.config.available_locales = :en

        string = string.urlize

        return string
    end

    def seleciona_opcao_menu(vars,opcao)

        dados = opcao.split('/')
        metodo = dados[1]
        
        All_Methods.new.select_drop(dados[0],dados[1])

        if (metodo.gsub(/\p{Ascii}/, "") =~ /\p{Latin}/) == 0
            
            metodo = remove_acento(dados[1]) 
        
            metodo = metodo.downcase.gsub!("-", "_")
        else            

            metodo = metodo.downcase.gsub!(" ", "_")
        end
        return metodo
    end
    
    def trata_name_method(metodo)

        metodo = metodo.delete!(metodo.byteslice(0..1))

        return metodo
    end

  
    def foco_browser
            
        result = page.driver.browser.window_handles.last
        page.driver.browser.switch_to.window(result) 
    end

    def atribui_dados(vars)

        vars.valor_procedimento = rand(100..1000)

        vars.data_procedimento = Time.now - rand(1000..900000)
        
        vars.data_procedimento = vars.data_procedimento.strftime("%d%m%Y")
    end

    def dados_PJ(vars)

        vars.name = "#{FFaker::NameBR.name} Teste"
        
        vars.cnpj_cpf = FFaker::IdentificationBR.pretty_cnpj

        vars.cpf = FFaker::IdentificationBR.pretty_cpf

        vars.numero_conselho = rand(1111..9999).to_s  
        
        vars.cep = MASS['Ceps'].sample
        
        return vars.name,vars.cnpj_cpf,vars.numero_conselho,vars.cep
    end

    def importa_arquivo(trace,file)        
        
        first('.d-flex.justify-content-end.align-items-center.pr-2').find(trace, visible: false).set(file)

    end


    def converte_nome_datas(vars)

        vars.datas = vars.datas.split('/')

        if vars.datas[1] == '01'        
            
            vars.datas = "#{vars.datas.first} jan #{vars.datas.last}"

        elsif vars.datas[1] == '02'

            vars.datas = "#{vars.datas.first} fev #{vars.datas.last}"

        elsif vars.datas[1] == '03'
            vars.datas = "#{vars.datas.first} mar #{vars.datas.last}"

        elsif vars.datas[1] == '04'

            vars.datas = "#{vars.datas.first} abr #{vars.datas.last}"

        elsif vars.datas[1] == '05'

            vars.datas = "#{vars.datas.first} mai #{vars.datas.last}"

        elsif vars.datas[1] == '06' 

            vars.datas = "#{vars.datas.first} jun #{vars.datas.last}"

        elsif vars.datas[1] == '07'

            vars.datas = "#{vars.datas.first} jul #{vars.datas.last}"

        elsif vars.datas[1] == '08' 
           
            vars.datas = "#{vars.datas.first} ago #{vars.datas.last}"

        elsif vars.datas[1] == '09' 
           
            vars.datas = "#{vars.datas.first} set #{vars.datas.last}"

        elsif vars.datas[1] == '10'
           
            vars.datas = "#{vars.datas.first} out #{vars.datas.last}"

        elsif vars.datas[1] == '11'
           
            vars.datas = "#{vars.datas.first} nov #{vars.datas.last}"

        elsif vars.datas[1] == '12'

            vars.datas = "#{vars.datas.first} dez #{vars.datas.last}"
        end
    end
end
        

        






     

















