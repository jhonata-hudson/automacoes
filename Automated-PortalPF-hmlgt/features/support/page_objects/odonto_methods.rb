class Odonto_Methds 

    include Capybara::DSL


    
    def select_movimentacao(vars,text)
        
        Avisos.new.detecta_aviso('.spinner',5)
        
        find('.d-flex.align-items-center.cursor-pointer.mb-3', text: text).click

        click_button('Avançar', wait: 10)
       
        page.execute_script("document.getElementById('contentToPdf').scrollTop += 10000")
    
        find("#checkAccept", visible: false ,wait: 10).click
         
        click_button('Avançar', wait: 10)
    
        dados = mass_movimentacao 
        
        EL[:Movimentacao].each do |ids| 

            if ids.first == "tipo_pay" || ids.first == 'type_telefones'

                find(ids.last, wait: 10).find('option',text:dados[ids.first.to_sym], wait: 10).click
            else 
                
                find(ids.last, wait: 10).set(dados[ids.first.to_sym])               
            end
        end
        
        Reembolso.new.banco(vars,['','Especifico'],['Banco', '#inputSearch',EL[:Elementos]['radio'],'Itau'])
    
        dados = [rand(1111...9999).to_s,rand(11111...9999999).to_s,rand(1..9).to_s]

        find('#agencyInputInput').set(dados.first)
        
        first("#accountInputInput").set(dados[1])
        
        all("#accountInputInput").last.set(dados.last)

        all('.d-flex.align-items-center.cursor-pointer')[rand(0..1)].click

        click_button('Enviar requerimento')

    end

    def mass_movimentacao

        dados = { "cep": ['69912-440','78557-749','82810-602','75143-372','49026-040','86066-180', '57043-255', '59090-657', '59135-530'].sample,
         "num_casa": rand(100..9000).to_s,
         "complemento": ['CASA', "AP - #{rand(1..10)} BL - #{rand(10...100)}"].sample,
         "telefones": FFaker::PhoneNumberBR.mobile_phone_number,
         "cpf": FFaker::IdentificationBR.pretty_cpf,
         "nome_titular": FFaker::NameBR.name,
         "tipo_pay": ['Conta corrente', 'Conta poupança'].sample,
         "type_telefones": ['Celular', 'Telefone'].sample           
     }
    
     dados
 end

 def valida_links_descontos_medicamento

    qtd_links = all('.link.pt-1').count - 2

    cont = 0 

    while cont <= qtd_links
        
        all('.link.pt-1')[cont].click 
        
        Helpers.new.foco_browser
        
        sleep 1

        target = "data/imagens/links_medicamentos/link_#{cont}.png"
        
        page.save_screenshot(target)
        
        window = page.current_window
        window.close

        Helpers.new.foco_browser

        cont += 1
        # 
    end
    

    all('.link.pt-1').last.click
    Helpers.new.foco_browser
end

 def finish_scn

    page.quit
end







end


