class Previdencia

    include Capybara::DSL


    def cadastro_de_aporte(vars,valor,fundos,cobranca,data)
       
        Avisos.new.detecta_aviso('.spinner',10) if has_selector?('.spinner')

        comum = Reembolso.new

        trace = EL[:Cadastro_aporte]
        cont = 0 

        distribui_valores(valor,fundos) unless has_selector?("select[disabled][id='coverageSelect']")

        find(trace['form_cobranca'], wait: 10).select(cobranca)
        
        find(trace['data'], wait: 10).set(data)
                
        if cobranca == 'Débito em Conta'

            comum.banco(vars,['','Especifico'],['Banco', '#inputSearch',EL[:Elementos]['radio'],'Banco Santander'])
            
            comum.infos_conta(vars,['','Aleatorio'],['#agencyInput', '#accountNumberInput','#accountDigitInput'])

            find('#valueCurrencyInput', wait: 5).set(rand(10000..1000000).to_s) if has_selector?('#valueCurrencyInput')
            
            vars.botao_name = 'Cadastrar aporte'

        elsif cobranca == 'Boleto'
    
            find('#valueCurrencyInput', wait: 5).set(rand(10000..1000000).to_s) if has_selector?('#valueCurrencyInput')
            
            click_button('Cadastrar aporte')
            
            Avisos.new.detecta_aviso('.spinner',10) if has_selector?('.spinner')

            vars.botao_name = 'Cenario finalizado'            
                
            Helpers.new.valida_geracao_pdf('cadastro_aporte')
        end
    end
    

    def distribui_valores(valor,fundos)

        trace = find(EL[:Previdencia_elements]['valor_aporte'])
        
        trace.set(rand(10000..500000).to_s)
        
        valor = trace.value
        
        valor_dist = separa_valores(valor,fundos)
                
        cont = 0
        loop do

            all('.col-md-4.col-6.my-3')[cont].find('.d-flex').click
            
            find("##{valor_dist[1][cont]}CurrencyInput").set(valor_dist.first)
            
            cont += 1
            
            puts cont 
        
            break if cont >= valor_dist.last.count  
        end

        $var = $var.to_s + "\n\n" + all('.d-flex.align-items-end.flex-column')[0].text

        click_button('Continuar')

        Avisos.new.detecta_aviso('.spinner',10) 
    end


    def separa_valores(valor,fundos)

        valor = valor.split(' ')
        
        valor.last.gsub!('.','') unless valor.last.include?('.') == false

        valor.last.gsub!(',','') unless valor.last.include?(',') == false

        fundos = fundos.split(',')

        valor_dist = valor.last.to_i / fundos.count

        return valor_dist, fundos
    end


    def cadastro_de_emissao(*datas)        
        cont = 0
        
        EL[:Previdencia]['periodo'].each do |infos|
            
            find(infos, wait: 5).set(datas[cont])
            cont += 1 
        end
        
        click_button('Gerar extrato')
        
        Avisos.new.detecta_aviso('.spinner',10) if datas.first.include?('1945') == false
    end

    def seleciona_boletos
        p '1'

        if has_selector?('button', text: '2ª via')
            p '2'
            all('button', text: '2ª via', wait: 30)[0].click
            p '3'
            Avisos.new.detecta_aviso('.spinner',10) if has_selector?('.spinner')
            p '4'
            click_button('Baixar')
        end
        p '5'
    end


     def valida_regulamento

        find('.title-extra-large', wait: 20).text
    end





























end