class Avisos

    include Capybara::DSL

    def detecta_aviso(elm,seg)

        cont = 0 

        while has_selector?(elm)
            sleep 0.5
            
            p "Aguarde" 

            cont += 1
            
            raise "Tempo de espera execido !!" if cont == 60            
            
            break if has_selector?(elm) == false 
        end

        EL[:Avisos].each do |elements| 
        
            raise $var = $var.to_s + "\n\n" + "#{all(elements[1])[0].text}" if has_selector?(elements[1]) == true
        end
    end
end