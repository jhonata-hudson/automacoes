require 'yaml'
require 'capybara/cucumber'
require 'pry'
require 'pry-nav'
require 'report_builder'
require 'string-urlize'
require 'ffaker'
require 'selenium-webdriver'
require 'capybara-screenshot'
require 'csv'
require 'gmail'
require 'mail'
require 'net/smtp'
require 'pony'
require 'open-uri'
require 'i18n'
# require 'allure-cucumber'


EL = YAML.load_file('data/elements.yml')
MASS = YAML.load_file('data/mass.yml')
FALE_CONOSCO = YAML.load_file('data/Mass_fale_conosco.yml')
DOCS = YAML.load_file('data/docs.yml')


Pry.commands.alias_command 'c' , 'continue'
Pry.commands.alias_command 's' , 'step'
Pry.commands.alias_command 'n' , 'next'

Capybara.default_max_wait_time = 0.8


if ENV['headless']
  Capybara.register_driver :selenium_chrome do |app|
    Capybara::Selenium::Driver.new app, browser: :chrome,
    options: Selenium::WebDriver::Chrome::Options.new(args: %w[window-size=1440,900 headless disable-gpu]  )
  end
else
  Capybara.default_driver = :selenium_chrome
end

Capybara.configure do |config|
  config.default_driver = :selenium_chrome 
  config.default_max_wait_time = 0.5
end


if ENV['prod']  

  p 'Bem vindo ao ambiente de Produção !'

  $url_default = 'https://www.segurosunimed.com.br'  

  Capybara.app_host = 'https://www.segurosunimed.com.br'  

elsif ENV['pre_prod']
  
  p 'Bem vindo ao ambiente de Pré Producao !'

  $url_default = 'https://www.segurosunimed.com.br'
  
  Capybara.app_host = 'https://preproducao-www.segurosunimed.com.br/' 
  
else

  p 'Bem vindo ao ambiente de Homologação !'
  
  $url_default = 'https://homologacao-www.segurosunimed.com.br'  

  Capybara.app_host = 'https://homologacao-www.segurosunimed.com.br'
end
    