Entao("valido mensagem de erros") do
    
    Negative.new.validate_text_error(@vars)

end


Entao("valido que mensagem {string} seja exibida") do |error|
    
    raise "Mensagem obrigatoria nao é exibida" if has_text?(error, wait: 20) == false 
end

Quando("preencho campo de login e senha com dados invalidos") do

    @login_site.loga_site(@vars,'invalidos')

end