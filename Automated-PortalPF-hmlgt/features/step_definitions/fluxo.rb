Quando("clico no {string}") do |produto|
     
     @produtos.seleciona_produtos(produto)
     
end


Quando("escolho qual {string} sera emitida segunda via de boleto") do |produto|

     @methods.viadeboleto(produto)
end
   

Quando("realizo selecao do {string}") do |plano|

     @produtos.seleciona_plano(plano)
end


Quando("capturo informacoes da certeirinha {string}") do |produto|

    @digital_card.executa_methodos_carteirinha_front(@vars,produto.downcase)
end


Quando("seleciono {string}") do |opcao_menu|     
     @metodo = @helpers.seleciona_opcao_menu(@vars,opcao_menu)

     if @metodo != nil
 
          @metodo = @helpers.trata_name_method(@metodo) if @metodo.byteslice(0).match(/[0-9]/).class == MatchData
     end
end


Quando("seleciono {string}, {string}, {string}, {string}") do |produto,status,dt_ini,dt_final|

     

     @methods.send("#{@metodo}",@vars,produto,status,dt_ini,dt_final)
end


Quando("visualizo modal selecionando {string}") do |produto|

     @methods.send("#{@metodo}",produto)
end

Quando("seleciono {string}, {string}, {string}") do |produto,segmento, ano|
         
     @methods.send("#{@metodo}",produto,segmento,ano)
end

Quando("seleciono {string}, {string}, {string}, {string}, {string}, {string}") do |produto, inter, dt_in, dt_fn, ano,semes|
        
     @methods.send("#{@metodo}",@vars,produto,inter,dt_in,dt_fn,ano,semes)
end


Quando("realizo {string}") do |text|

     @helpers.foco_browser

     begin 

          find('a', exact_text: text, wait: 20).click

     rescue  Capybara::ElementNotFound

          Avisos.new.detecta_aviso('.spinner',5)
     end
end


Quando("seleciono {string}, {string},   {string}, {string}") do |arg, arg2, arg3, arg4|



     @methods.send("#{@metodo}",@vars,arg,arg2,arg3,arg4)
end


Quando("escolho {string}, {string}") do |paciente, data|
    
     @methods.formulario(@vars,paciente,data)    
end


Quando("seleciono {string}, {string}") do |arg, arg2|

     @methods.send("#{@metodo}",@vars,arg,arg2)
end



Quando("visualizo detalhes de proposta {string}") do |proposta|

     @methods.seleciona_proposta(@vars,proposta)
end
   
Quando("realizo cadastro de aporte com {string}, {string}, {string}, {string}")do |valor,fundos,cobranca,data|

     @prev.cadastro_de_aporte(@vars,valor,fundos,cobranca,data)
     
end

Quando("realizo selecao da {string}, {string}") do |dt_inicial, dt_final|

     @prev.cadastro_de_emissao(dt_inicial,dt_final)
end


Quando("seleciono boleto bancario") do
 
     @prev.seleciona_boletos
end


Quando("busca de atendimento é realizada por {string}, {string}, {string}, {string}") do |estabelecimento, especialidade, nome, km|

     @saude.send("#{@metodo}",@vars,estabelecimento,especialidade,nome,km)
end


Quando("realizo selecao de usuario em {string}") do |usuario|
     
     @helpers.foco_browser
     
     if (usuario.gsub(/\p{Ascii}/, "") =~ /\p{Latin}/) == 0
          
          usuario_mtd = @helpers.remove_acento(usuario)

          @vars.scn = usuario_mtd.gsub('-','_') unless usuario_mtd.include?('-') == false
     else

          @vars.scn = usuario
     end

     @fale_conosco.executa_metodos_fale_conosco(@vars,usuario)     
end

Quando("seleciono uma das opções para seguir com o {string}") do |processo|

     @ouvidoria.seleciona_processo(processo)
end

Quando("insiro numero de protocolo existente") do 

     @ouvidoria.seleciona_numero_protocolo(@vars)
end

Quando("preencho os campos correspondentes ao cadastro") do
     
     Cad_cliente.new.exec_cad_cliente(@vars)

end

Quando("escolho senha de acesso") do
     
     Cad_cliente.new.gera_senha

end
   

Quando("realizo procedimento de {string}, termos, formulario") do |movimentacao_type|
     
     @odonto.select_movimentacao(@vars,movimentacao_type)

end

Quando("seleciono opcao de {string}") do |sinistro|

     @sinistro.seleciona_opcoes_cadastro_aviso(sinistro)
     
     @sinistro_opc = sinistro
end

Quando("realizo o procedimento de cadastro de aviso com {string}") do |cadastrante|

     sinistro_mthds = @sinistro_opc.gsub(' ', '_')

     @sinistro.send("#{sinistro_mthds.downcase}",cadastrante)

end

Quando("visualizo tela de {string} das Farmácias Unimed selecionando alguma delas") do |farm|

     @saude.seleciona_farmacia(farm)
end
   
Quando("preencho os campos de dados cadastrais, selecionando uma Apólice com {string}") do |status|


     @sinistro.cadastro_de_aviso_re(status)

     
end
  


  


  
  
  