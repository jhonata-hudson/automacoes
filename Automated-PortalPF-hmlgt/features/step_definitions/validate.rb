Entao("valido que status do produto seja {string}") do |status|
        
    @validate.valida_situacao_dados_titular(status)
end

Entao("valido dados do Cartão digital {string}") do |produto| 

    @validate.send("valida_dados_carteirinha_#{produto.downcase}",@vars)
end
  
Entao("valido dados de resposta") do
    
    @validate.valida_hist_pagamento(@vars)
end
    
Entao("valido tela de envio de boleto") do

    @validate.valida_tela_boleto
end

Entao("valido dados de informe de rendimento") do

    @validate.valida_dados_rendimentos
end

Entao("valido dados de comparticipação") do

    @validate.valida_dados_comparticipacao
end

Entao("valido que previa de consulta medico e efetuada com sucesso") do

    @validate.valida_formulario_previa(@vars)
    
end


Entao("valido que numero de protocolo seja exibido") do

    @validate.valida_numero_protocolo(@vars)
    
end


Entao("valido {string}") do |prev| 

    @validate.valida_previdencia(prev)

end
    
Entao("valido resultados") do
    res = @saude.resultados_guia_medico
    expect(res.size).to be > 0
    res[1].click
end

Entao("valido numero de protocolo armazenando o mesmo") do
 
    Avisos.new.detecta_aviso('.lds-ellipsis.mx-auto', 10)
    
    @fale_conosco.captura_protocolo(@vars) if expect(find(EL[:Elementos]['protocol_fale'], wait: 10).visible?).to eq true 

end

Entao("valido que solicitacao foi registrada com sucesso") do

    click_button(@vars.botao_name)

    Avisos.new.detecta_aviso('.lds-ellipsis.mx-auto', 10)    

    @validate.valida_solicitacao_ouvidoria if expect(find(EL[:Elementos]['descricao_protocol'], wait: 10).visible?).to eq true 
end

Entao("validao geracao do boleto em pdf") do

    @helpers.foco_browser

    @validate.valida_pdf('vida') 
end


Quando("valido termos de aceite") do

    @helpers.foco_browser
end


Entao("valido ambiente de teste") do
   
    puts 'Tudo pronto para execução'
end


Entao("valido solicitacao de cancelamento") do
    
    Avisos.new.detecta_aviso('.spinner',5)
    ## elaborar metodo de validacao
end
  

Entao("valido impressao") do
   
    @helpers.foco_browser
end


Entao("finalizo procedimento de impressao") do

    @odonto.finish_scn

end
  

Entao("valido tela de descontos em medicamentos") do


    expect(find('.pb-0.pb-lg-2.px-md-2.pt-md-2.px-lg-3.pt-lg-3', wait: 10).visible?).to eq true 

end

Entao("valido que historico nao seja apresentado") do
    
    @validate.valida_hist_pagamento(@vars)

end
  
  
Entao("valido que notificacao de sinistro gere numero de atendimento") do

    within_frame first('.w-100') do
        
        if expect(find('#mensagemOperacao', wait: 10).visible?).to eq true 
    
            @sinistro.valida_geracao_sinistro 
    
        else 

            raise "NAO FOI POSSIVEL GERAR NUMERO DE ATENDIMENTO"
        end
    end
end
  

Entao("valido que tela de FAQ seja exibida") do

    @helpers.foco_browser

    expect(find('.input-container', wait: 20).visible?).to eq true 
end


Entao("valido tela de regulamentos {string}") do |regulamento_type|     
    
    @helpers.foco_browser

    res = @prev.valida_regulamento
        
    expect(res.include?(regulamento_type)).to eq true
end


Entao("valido que {string} funcione de acordo com idade do paciente") do |idade|

    Reembolso.new.valida_idade_reembolso(idade)
end


Entao("espero visualizar tela de impresao do PDF conforme {string}") do |parceria|
    
    @saude.valida_dados_farmacias_parceiras_headless(parceria) if ENV['headless'] == 'true'
    
    @saude.valida_dados_farmacias_parceiras(parceria) if ENV['headless'] == nil
end

Entao("valido que cadastro de aviso digital seja gerado com sucesso") do

    @sinistro.valida_aviso_digital

end
