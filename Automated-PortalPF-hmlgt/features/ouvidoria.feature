
#language: pt 


@ouvidoria @all-exec
Funcionalidade: Realizar processo de ouvidoria 
    

@ouvidoria_logado
Esquema do Cenario: Gerar numero de protocolo de atendimento logado no ambiente
    Quando realizo 'Ouvidoria'
    E seleciono uma das opções para seguir com o "<processo>"
    Quando insiro numero de protocolo existente
    E realizo selecao de usuario em "<Eu sou>"
    Entao valido que solicitacao foi registrada com sucesso


Exemplos: 
|         processo        | Eu sou               |
|Outras Demandas Ouvidoria| Segurado             |  
# |Outras Demandas Ouvidoria| Prestador de Serviço |
# |Outras Demandas Ouvidoria| Corretor             |
# |Outras Demandas Ouvidoria| Não sou cliente      |
# |Reanálise - Seguros Saúde| Segurado             |
# |Reanálise - Seguros Saúde| Prestador de Serviço |
# |Reanálise - Seguros Saúde| Corretor             |
# |Reanálise - Seguros Saúde| Não sou cliente      |




@ouvidoria_nao_logado
Esquema do Cenario: Gerar numero de protocolo de atendimento nao logado
    Quando realizo 'Formulário de Ouvidoria'
    E seleciono uma das opções para seguir com o "<processo>"
    E insiro numero de protocolo existente
    E realizo selecao de usuario em "<Eu sou>"
    Entao valido que solicitacao foi registrada com sucesso


Exemplos: 
|         processo        | Eu sou               |
|Outras Demandas Ouvidoria| Segurado             |
# |Outras Demandas Ouvidoria| Prestador de Serviço |
# |Outras Demandas Ouvidoria| Corretor             |
# |Outras Demandas Ouvidoria| Não sou cliente      |
# |Reanálise - Seguros Saúde| Segurado             |
# |Reanálise - Seguros Saúde| Prestador de Serviço |
# |Reanálise - Seguros Saúde| Corretor             |
# |Reanálise - Seguros Saúde| Não sou cliente      |

