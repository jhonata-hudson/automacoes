#language: pt 
#encode: UTF-8

@re @all-exec
Funcionalidade: Validar fluxo produto Ramos elementares portal PF
    
 
@re1
Esquema do Cenario: Validar detalhes do Produto status ATIVO do Odonto
    Quando clico no 'produto Ramos Elementares'
    Entao realizo selecao do "<Plano>" 
    
Exemplos:
|Plano    |
|Em Vigor |


@re2 @historico-pagamento  @comum
Esquema do Cenario: Validar resultados de pesquisa historico de pagamento
    Quando clico no 'produto Ramos Elementares'
    E seleciono '<Drop List>'
    E seleciono '<Produto>', '<Status das parcelas>', '<Data inicial>', '<Data final>' 
    Entao valido dados de resposta

Exemplos: 
|         Drop List                |Produto                                                    |Status das parcelas| Data inicial| Data final |
|Financeiro/Histórico de pagamentos| Rc Profissional Saúde - Individual (019702019010378008281) |       Todos       |  19/09/2018 | 21/09/2018 | 



@re3  @historico-pagamento @comum
Esquema do Cenario: Consultar Historico de Pagamentos RE - sem historico
    Quando clico no 'produto Ramos Elemantares'
    E seleciono '<Drop List>'
    E seleciono '<Produto>', '<Status das parcelas>', '<Data inicial>', '<Data final>'
    Entao valido que mensagem 'Não foram encontradas parcelas com os parâmetros selecionados.' seja exibida  


Exemplos: 
|         Drop List                | Produto                                  | Status das parcelas | Data inicial| Data final |
|Financeiro/Histórico de pagamentos|  Unimed Pgbl Rf Corporate 100 (13047846) |       Todos         |  15/11/2018 | 14/11/2019 |



##teste
@re4 @2viaboleto @comum
Esquema do Cenario: Gerar 2 via de boleto
    Quando clico no 'produto Ramos Elementares'
    E seleciono '<Drop List>'
    E escolho qual '<Produto>' sera emitida segunda via de boleto
    Quando seleciono boleto bancario
    Entao valido '2ª via de boleto'

Exemplos: 
|         Drop List         | Produto                                                    |
|Financeiro/2ª via de boleto| Seguro Compreensivo Residencial (019702019010114001347)    |



@re5 @2viaboleto  @comum
Esquema do Cenario: Gerar 2a via de Boletos RE - sem boletos pendentes

    Quando clico no 'produto Ramos Elementares'
    E seleciono '<Drop List>'
    E escolho qual '<Produto>' sera emitida segunda via de boleto
    Quando seleciono boleto bancario
    Entao valido que mensagem 'Não foram encontradas parcelas com possibilidade de gerar 2ª via.' seja exibida  

Exemplos: 
|         Drop List         | Produto                                                  |
|Financeiro/2ª via de boleto| Seguro Compreensivo Residencial (019702019010114001347)  |


@re6 @2viaboleto @comum
Esquema do Cenario: Gerar 2a via de Boletos RE - SICCOB
    Quando clico no 'produto Ramos Elementares'
    E seleciono '<Drop List>'
    E escolho qual '<Produto>' sera emitida segunda via de boleto
    Quando seleciono boleto bancario
    Entao valido '2ª via de boleto'

Exemplos: 
|         Drop List         | Produto                                                    |
|Financeiro/2ª via de boleto| Seguro Compreensivo Residencial (019702019010114001347)    |



@re7 @2viaboleto @comum
Esquema do Cenario: Gerar 2a via de Boletos RE - ITAU
    Quando clico no 'produto Ramos Elementares'
    E seleciono '<Drop List>'
    E escolho qual '<Produto>' sera emitida segunda via de boleto
    Quando seleciono boleto bancario
    Entao valido '2ª via de boleto'

Exemplos: 
|         Drop List         | Produto                                                 |
|Financeiro/2ª via de boleto| Seguro Compreensivo Residencial (019702019010114001347  |



@re8
Esquema do Cenario: Gerar FAQ Vida
    Quando clico no 'produto Ramos Elementares'
    E seleciono '<Drop List>'
    Entao valido que tela de FAQ seja exibida

Exemplos:
|         Drop List                      | 
| Informações/Perguntas frequentes (FAQ) |



@re9
Esquema do Cenario: Realizar Notificacao de Sinistro Vida
    Quando clico no 'produto Ramos Elementares'
    E seleciono '<Drop List>'
    E preencho os campos de dados cadastrais, selecionando uma Apólice com "<Status>"
    Entao valido que cadastro de aviso digital seja gerado com sucesso

Exemplos:
|         Drop List               |  Status     |
|Sinistro/Notificação de Sinistro |  Em Vigor   |   
# |Sinistro/Notificação de Sinistro |  Terminado  |   
# |Sinistro/Notificação de Sinistro |  Terminado  |   
# |Sinistro/Notificação de Sinistro |  Terminado  |   
