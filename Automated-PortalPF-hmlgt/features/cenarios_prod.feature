
#language: pt 

@all-exec-prod
Funcionalidade: Validar produto Saude portal PF

@saude_prod
Esquema do Cenario: Gerar Relatório de Informe de Rendimento do Saúde 
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E seleciono '<Produto>', '<Segmento>', '<Ano Base>'
    Entao valido dados de informe de rendimento

Exemplos: 
|         Drop List                 | Produto                                       | Segmento  | Ano Base |
|Financeiro/Informes de rendimentos | UNIMED PLUS MÉDICO 2I (0 994 0999 21000100-5) | Saúde     |   2000   |


@saude5
Esquema do Cenario: Gerar IR Saúde - Somente Extrato de Reembolso
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E seleciono '<Segmento>', '<Ano Base>'
    Entao valido dados de informe de rendimento

Exemplos: 
|         Drop List                 | Segmento | Ano Base |
|Financeiro/Informes de rendimentos | Saúde    |   2000   |


@saude6
Esquema do Cenario: Gerar IR Saúde - Somente Demonstrativo de Valores Pagos
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E seleciono '<Segmento>', '<Ano Base>'
    Entao valido dados de informe de rendimento

Exemplos: 
|         Drop List                 | Segmento | Ano Base |
|Financeiro/Informes de rendimentos | Saúde    |   2012   |


@saude6ngtv 
Esquema do Cenario: Gerar IR Saúde - não possui
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E seleciono '<Segmento>', '<Ano Base>'
    Entao valido que mensagem 'Não existe documento a ser gerado para os dados selecionados.' seja exibida  

Exemplos: 
|         Drop List                 | Segmento | Ano Base |
|Financeiro/Informes de rendimentos | Saúde    |   2000   |


@odonto5
Esquema do Cenario: Gerar Relatório de Informe de Rendimento do Odonto
    Quando clico no 'produto Odonto'
    E seleciono '<Drop List>'
    E seleciono '<Segmento>', '<Ano Base>'
    Entao valido dados de informe de rendimento

Exemplos: 
|         Drop List                 | Segmento | Ano Base |
|Financeiro/Informes de rendimentos | Odonto   |   2018   |


@prev10
Esquema do Cenario: Gerar Relatório de Informe de Rendimento do previdencia
    Quando clico no 'produto Previdência'
    E seleciono '<Drop List>'
    E seleciono '<Segmento>', '<Ano Base>'
    Entao valido dados de informe de rendimento

Exemplos: 
|         Drop List                 | Segmento      | Ano Base |
|Financeiro/Informes de rendimentos | Previdência   |   2012   |


@pre10ngtv
Esquema do Cenario: Gerar IR Previdência - não possui
    Quando clico no 'produto Previdência'
    E seleciono '<Drop List>'
    E seleciono '<Segmento>', '<Ano Base>'
    Entao valido que mensagem 'Não existe documento a ser gerado para os dados selecionados.' seja exibida  

Exemplos: 
|         Drop List                 | Segmento      | Ano Base |
|Financeiro/Informes de rendimentos | Previdência   |   1945   |
