#language: pt 

@saude @all-exec
Funcionalidade: Validar produto Saude portal PF

 
@saude0 
Esquema do Cenario: Validar detalhes do Produto status ATIVO do Saúde
    Quando clico no 'produto Saúde'
    E realizo selecao do "<Plano>"
    Entao valido que status do produto seja 'Situação Ativo'

Exemplos:
|Plano |
|Ativo |

@saude1  
Esquema do Cenario: Validar Cartão digital do Saúde
    Quando clico no 'produto Saúde'
    E realizo selecao do "<Plano>"
    E capturo informacoes da certeirinha '<Produto>'
    Entao valido dados do Cartão digital "<Produto>" 

Exemplos:
|Plano | Produto |
|Ativo | Saude   |


@saude2  @historico-pagamento  @comum
Esquema do Cenario: Validar resultados de pesquisa historico de pagamento
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E seleciono '<Produto>', '<Status das parcelas>', '<Data inicial>', '<Data final>' 
    Entao valido dados de resposta

Exemplos: 
|         Drop List                |Produto                                                  |Status das parcelas| Data inicial| Data final |
|Financeiro/Histórico de pagamentos| Rede Especifico Especifico Apto (0 994 0032 91828700-0) |       Todos       |  01/09/2019 | 30/10/2019 | 




@historico-pagamento @saude2ngtv @comum
Esquema do Cenario: Consultar Histórico de Pagamentos Saúde - sem histórico
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E seleciono '<Produto>', '<Status das parcelas>', '<Data inicial>', '<Data final>' 
    Entao valido dados de resposta
    ##teste
    ##teste

Exemplos: 
|         Drop List                |Produto                                                  |Status das parcelas| Data inicial| Data final |
|Financeiro/Histórico de pagamentos| Rede Especifico Especifico Apto (0 994 0032 91828700-0) |       Todos       |  01/09/2019 | 30/10/2019 | 


@historico-pagamento @saude2ngtv @comum
Esquema do Cenario: Consultar Histórico de Pagamentos Saúde - sem histórico
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E seleciono '<Produto>', '<Status das parcelas>', '<Data inicial>', '<Data final>'
    Entao valido que mensagem 'Não foram encontradas parcelas com os parâmetros selecionados.' seja exibida  


Exemplos: 
|         Drop List                | Produto                                                          | Status das parcelas | Data inicial| Data final |
|Financeiro/Histórico de pagamentos| Unimed Seguro Saúde Empresarial Versatil (0 994 0065 52851600-0) |       Todos         |  15/11/2018 | 14/11/2019 | 




@saude3 @2viaboleto   @comum
Esquema do Cenario: Gerar 2 via de boleto
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E clico no ' botao Solicitar 2ª via de boleto'
    Entao valido tela de envio de boleto

Exemplos: 
|         Drop List         | Produto                                                 |
|Financeiro/2ª via de boleto| Rede Especifico Especifico Apto (0 994 0032 91828700-0) |




 @2viaboleto  @saude3ngtv @comum
Esquema do Cenario: Gerar 2a via de Boletos Saúde - sem boletos pendentes
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E clico no 'botao Confirmar'
    Entao valido que mensagem 'Não foram encontradas parcelas com possibilidade de gerar 2ª via.' seja exibida  



Exemplos: 
|         Drop List         | Produto                                                 |
|Financeiro/2ª via de boleto| Rede Especifico Especifico Apto (0 994 0032 91828700-0) |



@saude4
Esquema do Cenario: Gerar Relatório de Demonstrativo de Reembolso do Saúde
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E seleciono '<Segmento>', '<Ano Base>'
    Entao valido dados de informe de rendimento

Exemplos: 
|         Drop List                 | Segmento | Ano Base |
|Financeiro/Informes de rendimentos | Saúde    |   2000   |



@saude5
Esquema do Cenario: Gerar IR Saúde - Somente Extrato de Reembolso
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E seleciono '<Segmento>', '<Ano Base>'
    Entao valido dados de informe de rendimento

Exemplos: 
|         Drop List                 | Segmento | Ano Base |
|Financeiro/Informes de rendimentos | Saúde    |   2000   |


@saude6
Esquema do Cenario: Gerar IR Saúde - Somente Demonstrativo de Valores Pagos
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E seleciono '<Segmento>', '<Ano Base>'
    Entao valido dados de informe de rendimento

Exemplos: 
|         Drop List                 | Segmento | Ano Base |
|Financeiro/Informes de rendimentos | Saúde    |   2012   |



@saude6ngtv
Esquema do Cenario: Gerar IR Saúde - não possui
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E seleciono '<Segmento>', '<Ano Base>'
    Entao valido que mensagem 'Não existe documento a ser gerado para os dados selecionados.' seja exibida  

Exemplos: 
|         Drop List                 | Segmento | Ano Base |
|Financeiro/Informes de rendimentos | Saúde    |   2000   |



@saude7
Esquema do Cenario: Gerar Relatório de Utilização e Coparticipação do Saúde
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E seleciono '<Produto>', '<Intervalo>', '<Data_in>', '<Data_final>', '<Ano>', '<Semestre>'
    Entao valido dados de comparticipação

Exemplos: 
|         Drop List                     | Produto                                                | Intervalo   |Data_in     | Data_final |Ano   | Semestre         |
|Relatórios/Utilização e coparticipação | REDE ESPECIFICO ESPECIFICO APTO (0 994 0032 91828700-0)| Por período | 01/09/2019 | 25/09/2019 | 2018 | Julho a Dezembro |



@saude7ngtv
Esquema do Cenario: Gerar Relatório de Utilização e Coparticipação Saúde - não possui
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E seleciono '<Produto>', '<Intervalo>', '<Data_in>', '<Data_final>', '<Ano>', '<Semestre>'
    Entao valido que mensagem 'Não existe documento a ser gerado para os dados selecionados.' seja exibida  

Exemplos: 
|         Drop List                     | Produto                                                         | Intervalo   |Data_in     | Data_final |Ano   | Semestre         |
|Relatórios/Utilização e coparticipação | UNIMED SEGURO SAÚDE EMPRESARIAL VERSATIL (0 994 0065 52851600-0)| Por período | 01/10/2019 | 25/10/2019 | 2017 | Julho a Dezembro |



@saude8
Esquema do Cenario: Solicitar novo Reembolso
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E realizo 'Nova solicitação de reembolso'
    Quando seleciono '<Dados do procedimento>', '<Documentos necessarios>', '<Prestador de servico>', '<Dados bancários>'
    Entao valido que numero de protocolo seja exibido

Exemplos:
|         Drop List              |Dados do procedimento   |Documentos necessarios  |Prestador de servico    |Dados bancários         |
|Financeiro/Reembolsos e prévias | Reembolso_Especifico-1 | Reembolso_Especifico-1 | Reembolso_Especifico-1 | Reembolso_Especifico-1 |



@saude8ngtv
Esquema do Cenario: Consultar Reembolso de Dependente 
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E realizo 'Nova solicitação de reembolso'
    Quando seleciono '<Dados do procedimento>', '<Documentos necessarios>', '<Prestador de servico>', '<Dados bancários>'
    E valido que numero de protocolo seja exibido
    Entao valido que "<solicitacao>" funcione de acordo com idade do paciente

Exemplos:
|         Drop List              |Dados do procedimento   |Documentos necessarios  |Prestador de servico    |Dados bancários         | solicitacao     |
|Financeiro/Reembolsos e prévias | Menor_idade_Especifico |Menor_idade_Especifico  |Menor_idade_Especifico  |Menor_idade_Especifico  |  Menor de idade |
|Financeiro/Reembolsos e prévias | Maior_idade_Especifico  | Maior_idade_Especifico | Maior_idade_Especifico | Maior_idade_Especifico| Maior de idade  |



@saude9
Esquema do Cenario: Solicitar nova Prévia de Consulta Médica
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E realizo 'Prévia'
    E realizo 'Nova prévia de consulta médica'
    Quando escolho '<Paciente>', '<Data>'
    Entao valido que previa de consulta medico e efetuada com sucesso

Exemplos:
|    Drop List                    | Paciente                    | Data       |                 
|Financeiro/Reembolsos e prévias  | Guilherme Custodio Paulino  | 30/12/2019 |



@saude9ngtv
Esquema do Cenario: Consultar Prévia de Dependente outros procedimentos
    Quando clico no 'produto Saúde' 
    E seleciono '<Drop List>'
    E realizo 'Prévia'
    E realizo 'Nova prévia de outros procedimentos'
    Quando seleciono '<Dados do procedimento>', '<Documentos necessarios>'
    Entao valido que numero de protocolo seja exibido
    Quando realizo 'Reembolsos e Prévias' 
    E realizo 'Prévia'  
    Entao valido que "<solicitacao>" funcione de acordo com idade do paciente


Exemplos:
|    Drop List                     | Paciente                   | Data       | solicitacao    | Dados do procedimento     |Documentos necessarios     |             
|Financeiro/Reembolsos e prévias   | Kely Bianca Martins Lopes  | 30/12/2019 | Menor de idade | Previa_Especifico_Menor-1 | Previa_Especifico_Menor-1 | 
# |Financeiro/Reembolsos e prévias   |  Elis Martins Lopes        | 30/12/2019 | Maior de idade | Previa_Especifico_Maior-1 | Previa_Especifico_Maior-1 |



@saude10
Esquema do Cenario: Solicitar nova Prévia de Outros Procedimentos
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    E realizo 'Prévia'
    E realizo 'Nova prévia de outros procedimentos'
    Quando seleciono '<Dados do procedimento>', '<Documentos necessarios>'
    Entao valido que numero de protocolo seja exibido

Exemplos:
|    Drop List                    | Dados do procedimento |Documentos necessarios |            
|Financeiro/Reembolsos e prévias  | Previa_Especifico-1   | Previa_Especifico-1   |


@saude11
Esquema do Cenario: Buscar atendimento do Guia Médico do Saúde
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    Quando busca de atendimento é realizada por '<Tipo de estabelecimento>', '<Especialidade>', '<Prestador>', '<KM>'
    Entao valido resultados 


Exemplos:
|    Drop List           | Tipo de estabelecimento | Especialidade | Prestador | KM   |
|Informações/Guia médico |  CLÍNICAS               |  NEUROLOGIA   | Aleatorio | 30   |


@saude_login_invalido 
Cenario: CPF e Senhas inválidos
    Quando preencho campo de login e senha com dados invalidos 
    E valido que mensagem 'CPF não é Válido' seja exibida  
    Entao valido que mensagem 'Senha é Obrigatório' seja exibida  


@saude13
Esquema do Cenario: Imprimir Dados Cadastrais Saúde
    Quando clico no 'produto Saúde'
    E realizo selecao do "<Plano>"
    E realizo "Imprimir"
    Entao valido impressao
    Entao finalizo procedimento de impressao


Exemplos:
| Plano |
| Ativo |


@saude14
Esquema do Cenario: Solicitar Cancelamento do Plano Saúde
    Quando clico no 'produto Saúde'
    E realizo selecao do "<Plano>"
    Quando realizo "Solicitar cancelamento"
    E realizo procedimento de "<Movimentacao>", termos, formulario
    Entao valido solicitacao de cancelamento

Exemplos:
|Plano   | Movimentacao                                                                                    |
|Ativo   | O pedido de cancelamento será da totalidade do Grupo Familiar (Titular/Dependentes e Agregados).|


@saude15
Esquema do Cenario: Gerar FAQ Saúde
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    Entao valido que tela de FAQ seja exibida

Exemplos:
|         Drop List                      | 
| Informações/Perguntas frequentes (FAQ) |


@saude16
Esquema do Cenario: Baixar Descontos em Medicamentos Saúde
    Quando clico no 'produto Saúde'
    E seleciono '<Drop List>'
    Quando visualizo tela de "<Parcerias>" das Farmácias Unimed selecionando alguma delas
    Entao espero visualizar tela de impresao do PDF conforme "<Parcerias>"


Exemplos:
|         Drop List                      | Parcerias                               |
| Informações/Descontos em medicamentos  | RaiaDrogasil                            |
| Informações/Descontos em medicamentos  | Drogarias Pacheco & Drogaria São Paulo  |

