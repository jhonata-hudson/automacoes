#language: pt 


@prev @all-exec
Funcionalidade: Validar fluxo produto Previdencia portal PF


@prev1
Cenario: Validar Composição da Reserva do Previdência
    Quando clico no 'produto Previdência'
    Entao valido 'Composição da Reserva do Previdência'


@prev2
Cenario: Validar Rentabilidade de Fundos do Previdência
    Quando clico no 'produto Previdência'
    Entao valido 'Rentabilidade de Fundos do Previdência'


@prev3
Esquema do Cenario: Validar detalhes do Produto status ATIVO do Odonto
    Quando clico no 'produto Previdência'
    E visualizo detalhes de proposta "<Cliente>" 
    Entao valido que status do produto seja 'Situação Confirmada'

Exemplos:
| Cliente                           |
|Patrícia Junqueira Ferraz Baracat  |


@prev4
Esquema do Cenario: Realizar aporte da Proposta do Previdência
    Quando clico no 'produto Previdência'
    E clico no 'botao Aporte'
    E realizo cadastro de aporte com '<Valor do aporte>', '<Fundos>', '<Forma de cobrança>', '<Data de pagamento>'
    Entao valido que numero de protocolo seja exibido


Exemplos:   
|Valor do aporte |    Fundos       | Forma de cobrança | Data de pagamento  |
|   aleatorio    | RV15,RV20,RV25  | Débito em Conta   |   30/12/2019       |
|   aleatorio    | RV15,RV20,RV25  | Boleto            |   30/12/2019       |


@prev5
Esquema do Cenario: Realizar extrato da Proposta do Previdência
    Quando clico no 'produto Previdência'
    E clico no 'botao Extrato'
    E realizo selecao da "<Data inicio>", "<Data final>"
    Entao valido 'Emissão de extrato'

Exemplos: 
|Data inicio   | Data final  |
| 01/09/2019  | 01/10/2019  |


@prev5ngtv
Esquema do Cenario: Gerar Extrato Previdência - não possui
    Quando clico no 'produto Previdência'
    E clico no 'botao Extrato'
    E realizo selecao da "<Data inicio>", "<Data final>"
    Entao valido que mensagem 'Não foi possível processar sua requisição no momento.Tente novamentemais tarde.' seja exibida  

Exemplos: 
|Data inicio   | Data final  |
| 01/09/1945   | 01/10/1945  |




@prev6 @comum
Esquema do Cenario: Consultar Histórico de Pagamentos do Previdência 
    Quando clico no 'produto Previdência'
    E seleciono '<Drop List>'
    E seleciono '<Produto>', '<Status das parcelas>', '<Data inicial>', '<Data final>' 
    Entao valido dados de resposta

Exemplos: 
|         Drop List                |Produto                                        |Status das parcelas| Data inicial| Data final |
|Financeiro/Histórico de pagamentos| Unimed Pgbl Private Rf Conservador (13047510) |       Todos       |  15/11/2018 | 04/12/2019 | 


@prev6ngtv @comum
Esquema do Cenario: Consultar Histórico de Pagamentos RE - sem histórico
    Quando clico no 'produto Previdência'
    E seleciono '<Drop List>'
    E seleciono '<Produto>', '<Status das parcelas>', '<Data inicial>', '<Data final>'
    Entao valido que mensagem 'Não foram encontradas parcelas com os parâmetros selecionados.' seja exibida  


Exemplos: 
|         Drop List                | Produto                                  | Status das parcelas | Data inicial| Data final |
|Financeiro/Histórico de pagamentos|  Unimed Pgbl Rf Corporate 100 (13047846) |       Todos         |  15/11/2018 | 14/11/2019 | 
# |Financeiro/Histórico de pagamentos|  Unimed Pgbl Private Rf Conservador (13047510) |       Todos         |  15/11/2011 | 14/11/2019 | 
# |Financeiro/Histórico de pagamentos|  Unimed Pgbl Private Rf Conservador (13047510) |       Pendente      |  15/11/2011 | 14/11/2019 | 
# |Financeiro/Histórico de pagamentos|  Unimed Pgbl Private Rf Conservador (13047510) |       Pago          |  15/11/2011 | 14/11/2019 | 
# |Financeiro/Histórico de pagamentos|  Unimed Pgbl Private Rf Conservador (13047510) |       Cancelado     |  15/11/2011 | 14/11/2019 | 



@prev7 @2viaboleto @comum
Esquema do Cenario: Gerar 2 via de boleto
    Quando clico no 'produto Previdência'
    E seleciono '<Drop List>'
    E escolho qual '<Produto>' sera emitida segunda via de boleto
    Quando seleciono boleto bancario
    Entao valido '2ª via de boleto'

Exemplos: 
|         Drop List         | Produto                                       |
|Financeiro/2ª via de boleto| UNIMED PGBL PRIVATE RF CONSERVADOR (13047510) |



 @2viaboleto @comum
Esquema do Cenario: Gerar 2a via de Boletos Previdência - sem boletos pendentes
    Quando clico no 'produto Previdência'
    E seleciono '<Drop List>'
    E escolho qual '<Produto>' sera emitida segunda via de boleto
    Entao valido que mensagem 'Não foram encontradas parcelas com possibilidade de gerar 2ª via.' seja exibida  

Exemplos: 
|         Drop List         | Produto                                       |
|Financeiro/2ª via de boleto| UNIMED PGBL PRIVATE RF CONSERVADOR (13047510) |



@2viaboleto @prev8 @comum
Esquema do Cenario: Gerar 2a via de Boletos RE - SICCOB
    Quando clico no 'produto Previdência'
    E seleciono '<Drop List>'
    E escolho qual '<Produto>' sera emitida segunda via de boleto
    Quando seleciono boleto bancario
    Entao valido '2ª via de boleto'

Exemplos: 
|         Drop List         | Produto                                       |
|Financeiro/2ª via de boleto| UNIMED PGBL PRIVATE RF CONSERVADOR (13047510) |



@prev9 @2viaboleto  @comum
Esquema do Cenario:Gerar 2a via de Boletos RE - ITAÚ
    Quando clico no 'produto Previdência'
    E seleciono '<Drop List>'
    E escolho qual '<Produto>' sera emitida segunda via de boleto
    Quando seleciono boleto bancario
    Entao valido '2ª via de boleto'

Exemplos: 
|         Drop List         | Produto                                       |
|Financeiro/2ª via de boleto| UNIMED PGBL PRIVATE RF CONSERVADOR (13047510) |


@prev11
Esquema do Cenario: Consultar Regulamentos Previdência
    Quando clico no 'produto Previdência'
    E seleciono '<Drop List>'
    Quando realizo "<Previdencia>" 
    Entao valido tela de regulamentos "<Previdencia>"

Exemplos:
|         Drop List         |Previdencia |
| Informações/Regulamentos  | PGBL       |


@prev12
Esquema do Cenario:  Gerar FAQ Previdência
    Quando clico no 'produto Previdência'
    E seleciono '<Drop List>'
    Entao valido que tela de FAQ seja exibida


Exemplos:
|         Drop List                      | 
| Informações/Perguntas frequentes (FAQ) |


