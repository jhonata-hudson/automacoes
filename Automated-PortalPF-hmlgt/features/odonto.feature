
#language: pt 


@odonto @all-exec
Funcionalidade: Validar produto Odonto portal PF
    
 
@odonto1 
Esquema do Cenario: Validar detalhes do Produto status ATIVO do Odonto
    Quando clico no 'produto Odonto'
    E realizo selecao do "<Plano>" 
    Entao valido que status do produto seja 'Situação Ativo'

Exemplos:
|Plano   |
|Ativo   |


@odonto2
Esquema do Cenario: Validar Cartão digital do Odonto
    Quando clico no 'produto Odonto'
    E realizo selecao do "<Plano>"
    E capturo informacoes da certeirinha '<Produto>'
    Entao valido dados do Cartão digital "<Produto>" 

Exemplos:
|Plano  |Produto |
|Ativo  | Odonto |


@odonto3 @historico-pagamento @comum @comum
Esquema do Cenario: Validar resultados de pesquisa historico de pagamento
    Quando clico no 'produto Odonto'
    E seleciono '<Drop List>'
    E seleciono '<Produto>', '<Status das parcelas>', '<Data inicial>', '<Data final>' 
    Entao valido dados de resposta

Exemplos: 
|         Drop List                | Produto                                                       |Status das parcelas| Data inicial| Data final |
|Financeiro/Histórico de pagamentos| Sou Essencial Pleno (Sem Orto) - Cop (9 000 0001 18704900-3)  |       Todos       |  01/04/2019 | 30/10/2019 | 


@odonto4 @2viaboleto @comum
Esquema do Cenario: Gerar 2 via de boleto
    Quando clico no 'produto Odonto'
    E seleciono '<Drop List>'
    E escolho qual '<Produto>' sera emitida segunda via de boleto
    E clico no 'botao Solicitar 2ª via de boleto'
    Entao valido tela de envio de boleto

Exemplos: 
|         Drop List         | Produto                                                       |
|Financeiro/2ª via de boleto| SOU ESSENCIAL PLENO (SEM ORTO) - COP (9 000 0001 18704900-3)  |


@odonto6
Esquema do Cenario: Gerar Relatório de Utilização e Coparticipação do Odonto
    Quando clico no 'produto Odonto'
    E seleciono '<Drop List>'
    E seleciono '<Produto>', '<Intervalo>', '<Data_in>', '<Data_final>', '<Ano>', '<Semestre>'
    Entao valido dados de comparticipação

Exemplos: 
|         Drop List                     | Produto                                                      | Intervalo   |Data_in     | Data_final  |Ano   | Semestre        |
|Relatórios/Utilização e coparticipação | SOU ESSENCIAL PLENO (SEM ORTO) - COP (9 000 0001 18704900-3) | Por período | 01/09/2019 | 25/09/2019  | 2018 | Julho a Dezembro|
# |Relatórios/Utilização e coparticipação | SOU ESSENCIAL PLENO (SEM ORTO) - COP (9 000 0001 18704900-3) | Por semestre| 01/09/2019 | 25/09/2019  | 2018 | Julho a Dezembro|



@odonto7
Esquema do Cenario: Solicitar Cancelamento do Plano Odonto
    Quando clico no 'produto Odonto'
    E realizo selecao do "<Plano>"
    Quando realizo "Solicitar cancelamento"
    E realizo procedimento de "<Movimentacao>", termos, formulario
    Entao valido solicitacao de cancelamento

Exemplos:
|Plano   | Movimentacao                                                                                    |
|Ativo   | O pedido de cancelamento será da totalidade do Grupo Familiar (Titular/Dependentes e Agregados).|


@odonto8
Esquema do Cenario: Imprimir Dados Cadastrais Odonto
    Quando clico no 'produto Odonto'
    E realizo selecao do "<Plano>"
    E realizo "Imprimir"
    Entao valido impressao
    Entao finalizo procedimento de impressao


Exemplos:
| Plano |
| Ativo |


@odonto9
Esquema do Cenario: Acessar Clube de Vantagens Odonto
    Quando clico no 'produto Odonto'
    E seleciono '<Drop List>'
    Quando visualizo tela de "<Parcerias>" das Farmácias Unimed selecionando alguma delas
    Entao espero visualizar tela de impresao do PDF conforme "<Parcerias>"


Exemplos:
|         Drop List                      | Parcerias                               |
| Informações/Descontos em medicamentos  | RaiaDrogasil                            |
| Informações/Descontos em medicamentos  | Drogarias Pacheco & Drogaria São Paulo  |