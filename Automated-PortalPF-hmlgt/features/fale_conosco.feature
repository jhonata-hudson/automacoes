#language: pt 

@conosco @all-exec
Funcionalidade: Fale conosco

@conosco_logado
Esquema do Cenario: Gerar numero de protocolo de atendimento com area logada
    Quando realizo 'Formulário Fale Conosco'
    E realizo selecao de usuario em "<Eu sou>"
    E clico no 'botao Concluir'
    Entao valido numero de protocolo armazenando o mesmo

Exemplos:
|Eu sou                |
| Segurado             |
# | Prestador de Serviço |
# | Corretor             |
# | Estipulante-RH       |
# | Empresa de Cobrança  |
# | Não sou cliente      |
# | Médico               |


@conosco_nao_logado
Esquema do Cenario: Gerar numero de protocolo de atendimento nao logado
    Quando realizo 'Formulário Fale Conosco'
    E realizo selecao de usuario em "<Eu sou>"
    E clico no 'botao Concluir'
    Entao valido numero de protocolo armazenando o mesmo

Exemplos:
|Eu sou                |
| Segurado             |
# | Prestador de Serviço |
# | Corretor             |
# | Estipulante-RH       |
# | Empresa de Cobrança  |
# | Não sou cliente      |
# | Médico               |





