#language: pt 

@vida @all-exec
Funcionalidade: Validar produto Vida portal PF
    

@vida1
Esquema do Cenario: Validar detalhes do Produto status ATIVO do Vida
    Quando clico no 'produto Vida'
    E realizo selecao do "<Plano>"
    Entao valido que status do produto seja 'Situação Emitida'

Exemplos:
|Plano   |
|Emitida | 


@vida2 @historico-pagamento @comum
Esquema do Cenario: Validar resultados de pesquisa historico de pagamento
    Quando clico no 'produto Vida'
    E seleciono '<Drop List>'
    E seleciono '<Produto>', '<Status das parcelas>', '<Data inicial>', '<Data final>' 
    Entao valido dados de resposta

Exemplos: 
|         Drop List                |Produto                          |Status das parcelas| Data inicial| Data final |
|Financeiro/Histórico de pagamentos|Garantia Funeral (1009300612945) |       Todos       |  30/06/2019 | 30/10/2019 | 


 @historico-pagamento @vida3ngtv @comum
Esquema do Cenario: Consultar Histórico de Pagamentos Vida - sem histórico
    Quando clico no 'produto Vida'
    E seleciono '<Drop List>'
    E seleciono '<Produto>', '<Status das parcelas>', '<Data inicial>', '<Data final>' 
    Entao valido que historico nao seja apresentado


Exemplos: 
|         Drop List                |Produto                          |Status das parcelas| Data inicial| Data final |
|Financeiro/Histórico de pagamentos|Garantia Funeral (1009300612945) |       Todos       |  30/06/2019 | 30/10/2019 | 


@vida4 @2viaboleto @comum
Esquema do Cenario: Gerar 2 via de boleto
    Quando clico no 'produto Vida'
    E seleciono '<Drop List>'
    E escolho qual '<Produto>' sera emitida segunda via de boleto
    E seleciono boleto bancario
    Entao validao geracao do boleto em pdf

Exemplos: 
|         Drop List         | Produto                                                 |
|Financeiro/2ª via de boleto| Vida Em Grupo e Acidentes Pessoais (1009300624927/134)  |



@vida4ngtv @2viaboleto @comum
Esquema do Cenario: Gerar 2 via de boleto sem boletos pendentes
    Quando clico no 'produto Vida'
    E seleciono '<Drop List>'
    E escolho qual '<Produto>' sera emitida segunda via de boleto
    Entao valido que mensagem 'Não foram encontradas parcelas com possibilidade de gerar 2ª via.' seja exibida  

Exemplos: 
|         Drop List         | Produto                                  |
|Financeiro/2ª via de boleto| Garantia Funeral (1009300612945/1367047) |


@vida6
Esquema do Cenario: Realizar Notificação de Sinistro Vida
    Quando clico no 'produto Vida'
    E seleciono '<Drop List>'
    Quando seleciono opcao de "<Sinistro>"
    E realizo o procedimento de cadastro de aviso com "<Cadastrante>"  
    Entao valido que notificacao de sinistro gere numero de atendimento

Exemplos:
|         Drop List               | Sinistro          | Cadastrante |
|Sinistro/Notificação de Sinistro | Cadastro de Aviso |  Segurado   |


@vida7
Esquema do Cenario: Gerar FAQ Vida
    Quando clico no 'produto Vida'
    E seleciono '<Drop List>'
    Entao valido que tela de FAQ seja exibida

Exemplos:
|         Drop List                      | 
| Informações/Perguntas frequentes (FAQ) |

